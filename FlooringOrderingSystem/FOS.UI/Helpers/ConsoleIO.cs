﻿using FOS.BLL.Factories;
using FOS.BLL.Managers;
using FOS.Models;
using FOS.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.UI.Helpers
{
    public class ConsoleIO
    {
        public const string SeperatorBar = "-------------------------------------------------------------";

        public static void DisplayOrderDetails(Order order, DateTime date)
        {
           

            DateTime orderDate = date;
            string _date = orderDate.ToString("MM/dd/yyyy");

            Console.WriteLine();
            Console.WriteLine($"Order Number: { order.OrderNumber}  | Order Date: { _date} ");

            string commaHolder = AddComma(order.CustomerName);
            Console.WriteLine($"Customer Name: { commaHolder} | State: { order.CustomerState }");
            Console.WriteLine(SeperatorBar);
            Console.WriteLine($"Product Type: {order.Product.TypeOfProduct}");
            Console.WriteLine($"Area Ft Sq: {order.Area}");
            Console.WriteLine();
            Console.WriteLine($"Material Cost: {order.MaterialCost:C}");
            Console.WriteLine($"   Labor Cost: {order.LaborCost:C}");
            Console.WriteLine($"          Tax: {order.Tax:C}");
            Console.WriteLine($"       ___________________");
            Console.WriteLine($"        Total: {order.Total:C}");
            Console.WriteLine();

        }

        public static string GetTwoLetterState(string prompt, bool nullIsOk)
        {
            Console.Write($"{ prompt}");
            string state = Console.ReadLine().ToUpper();

            if (nullIsOk == true)
            {
                if (state == "")
                {
                    return null;
                }
            }
            while (true)
            {
                while (state.Length < 2 || state.Length > 2 )
                {
                    Console.Write("Please enter the two letter abreviation for your state:  ");
                    state = Console.ReadLine().ToUpper();
                }
                return state;

            }
           
        }

        public static decimal GetRequiredSquareFootage(string prompt)
        {
            Console.Write(prompt);
            string area = Console.ReadLine();
            int sqft;


            while (!int.TryParse(area, out sqft) || (sqft < 100))
            {
                Console.Write("Please enter a valid area, larger than 100 ft sq:  ");
                area = Console.ReadLine();
            }

            return sqft;
        }

        public static string GetRequiredStringFromUser(string prompt, bool nullIsOk)
        {
            while (true)
            {
                Console.Write(prompt);
                string input = Console.ReadLine();

                if (nullIsOk == false && string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter valid text.");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
                else if(nullIsOk== true && string.IsNullOrEmpty(input))
                {
                    input = null;
                    return input;
                }
                else
                {
                    input = CheckForCommas(input);
                    return input;
                }
            }
        }

        public static DateTime GetValidDateFromUser()
        {
            DateTime orderDate;
            while (true)
            {
                Console.Write("Enter order date:  ");
                string date = Console.ReadLine();

                if (DateTime.TryParse(date, out orderDate))
                {
                    return orderDate;
                }

                else
                {
                    Console.WriteLine("Invalid date. ");
                }
            }
        }

        public static DateTime GetFutureDateFromUser()
        {
            DateTime date;

            while (true)
            {
                date = GetValidDateFromUser();

                if (date > DateTime.Today)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("All orders must be placed tomorrow or later.");
                }
            }
            return date;
        }

        public static ProductType GetRequiredFloorTypeFromUser()
        {
            while (true)
            {
                Console.Write("Enter desired flooring type - (C)arpet, (W)ood, (L)inolium, or (T)ile: ");

                string flooringType = Console.ReadLine().ToUpper();

                switch (flooringType)
                {
                    case "C":
                        return ProductType.Carpet;
                    case "W":
                        return ProductType.Wood;
                    case "L":
                        return ProductType.Laminate;
                    case "T":
                        return ProductType.Tile;
                    default:
                        Console.WriteLine("Please enter a valid choice.");
                        continue;
                }
            }
        }

        public static string CheckForCommas(string input)
        {
            string output;

            if (input.Contains(','))
            {
                output = input.Replace(",", "|");
            }
            else
                output = input;

            return output;
        }

        public static string AddComma(string input)
        {
            string output;

            if (input.Contains('|'))
            {
                output = input.Replace("|", ",");
            }
            else
                output = input;

            return output;
        }

        public static string GetYesNoAnswerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt + " (Y/N)? ");
                string input = Console.ReadLine().ToUpper();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter Y/N.");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
                else
                {
                    if (input != "Y" && input != "N")
                    {
                        Console.WriteLine("You must enter Y/N.");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        continue;
                    }
                    return input;
                }
            }
        }

        public static int GetOrderIndexFromUser(string prompt, int count)
        {
            int output;

            while (true)
            {
                Console.Write(prompt);
                string input = Console.ReadLine();

                if (!int.TryParse(input, out output))
                {
                    Console.WriteLine("You must enter valid order number.");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
                else
                {
                    if (output < 1 || output > count)
                    {
                        Console.WriteLine("Please choose an order by number between {0} and {1}", 1, count);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        continue;
                    }

                    return output;
                }
            }
        }
    }
}
