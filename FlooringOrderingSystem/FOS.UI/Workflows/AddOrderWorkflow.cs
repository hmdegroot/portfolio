﻿using FOS.BLL.Factories;
using FOS.BLL.Managers;
using FOS.Models;
using FOS.Models.Responses;
using FOS.UI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.UI.Workflows
{
    public class AddOrderWorkflow
    {
        public void Execute()
        {
            Console.Clear();
            Console.WriteLine("New Order");
            Console.WriteLine(ConsoleIO.SeperatorBar);
            Console.WriteLine();

            Order newOrder = new Order();

            newOrder.OrderDate = ConsoleIO.GetFutureDateFromUser();
            newOrder.CustomerName = ConsoleIO.GetRequiredStringFromUser("Name: ", false);
            newOrder.ProductType = ConsoleIO.GetRequiredFloorTypeFromUser();
            newOrder.CustomerState = ConsoleIO.GetTwoLetterState("Enter delivery state:  ", false);
            newOrder.Area = ConsoleIO.GetRequiredSquareFootage("Enter area, minimum of 100 sq ft: ");

            OrderManager orderManager = OrderManagerFactory.Create();

            var taxResponse = orderManager.CreateTaxInfoResponse(newOrder);

            if (taxResponse.Success == false)
            {
                Console.WriteLine(taxResponse.Message);
                Console.WriteLine("Your order was not created. Press any key to continue. ");
                Console.ReadKey();
                Menu.Start();
            }

            orderManager.CreateProductInfoResponse(newOrder);

            Console.Clear();

            ConsoleIO.DisplayOrderDetails(newOrder, newOrder.OrderDate);
            Console.WriteLine();
            string yesOrNo = ConsoleIO.GetYesNoAnswerFromUser("Would you like to add this order?  ");

            if (yesOrNo == "Y")
            {
                OrderAddResponse response = orderManager.AddOrder(newOrder, newOrder.OrderDate);
                Console.Clear();
                Console.WriteLine("Your order has been added.");
            }
            else
            {
                Console.WriteLine("New Order Cancelled.");
                Console.Write("Press any key to continue.   ");
            }
            Console.WriteLine();
            ConsoleIO.DisplayOrderDetails(newOrder, newOrder.OrderDate);
            Console.WriteLine();
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

    }
}
