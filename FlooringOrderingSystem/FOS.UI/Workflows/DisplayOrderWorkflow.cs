﻿using FOS.BLL;
using FOS.BLL.Factories;
using FOS.BLL.Managers;
using FOS.Models.Responses;
using FOS.UI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FOS.UI.Workflows
{
    public class DisplayOrderWorkflow
    {
        public void Execute()
        {
            Console.Clear();
            DisplayListOfOrders();
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        public static void DisplayListOfOrders()
        {
            Console.Clear();
            OrderManager orderManager = OrderManagerFactory.Create();

            while (true)
            {

                Console.WriteLine("Lookup an order");
                Console.WriteLine(ConsoleIO.SeperatorBar);
                DateTime orderDate = ConsoleIO.GetValidDateFromUser();

                Console.Clear();

                OrdersLookupResponse response = orderManager.DisplayOrders(orderDate);

                if (response.Success)
                {
                    foreach (var order in response.Orders)
                    {
                        orderManager.CreateTaxInfoResponse(order);
                        orderManager.CreateProductInfoResponse(order);
                        ConsoleIO.DisplayOrderDetails(order, orderDate);
                    }
                }
                else
                {
                    Console.WriteLine("No orders found. ");
                    
                }
                break;
            }
        }
    }
}
