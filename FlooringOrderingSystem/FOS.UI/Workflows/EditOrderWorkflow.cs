﻿using FOS.BLL.Factories;
using FOS.BLL.Managers;
using FOS.Models;
using FOS.Models.Responses;
using FOS.UI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.UI.Workflows
{
    public class EditOrderWorkflow
    {
        public void Execute()
        {
            Console.Clear();
            OrderManager orderManager = OrderManagerFactory.Create();

            Order originalOrder = new Order();
            Order tempOrder = new Order();

            while (true)
            {

                Console.WriteLine("Lookup an order");
                Console.WriteLine(ConsoleIO.SeperatorBar);
                DateTime orderDate = ConsoleIO.GetValidDateFromUser();

                OrdersLookupResponse response = orderManager.DisplayOrders(orderDate);

                if (response.Success)
                {
                    foreach (var order in response.Orders)
                    {
                        orderManager.CreateTaxInfoResponse(order);
                        orderManager.CreateProductInfoResponse(order);
                        ConsoleIO.DisplayOrderDetails(order, orderDate);
                    }
                }
                else
                {
                    Console.WriteLine("No orders found on that date");
                    Console.Write("Press any key to continue");
                    Console.ReadKey();
                    break;
                }

                int index = ConsoleIO.GetOrderIndexFromUser("What order number would you like to edit?  ", response.Orders.Count());

                Console.Clear();
                Console.WriteLine();

                tempOrder = response.Orders.Where(o => o.OrderNumber == index).FirstOrDefault();
                originalOrder = response.Orders.Where(o => o.OrderNumber == index).FirstOrDefault();

                Console.WriteLine("To change your order, enter corrected information. Otherwise press Enter to continue. ");
                Console.WriteLine();
                tempOrder.OrderDate = orderDate;
                tempOrder.CustomerName = ConsoleIO.GetRequiredStringFromUser($"Name ({originalOrder.CustomerName}):  ", true) ?? originalOrder.CustomerName;
                tempOrder.Product.TypeOfProduct = GetEditedProductTypeFromUser($"Product Type Ordered ({ originalOrder.Product.TypeOfProduct})  ", originalOrder);
                tempOrder.CustomerState = ConsoleIO.GetTwoLetterState($"State ({tempOrder.CustomerState}):  ", true) ?? originalOrder.CustomerState;
                tempOrder.Area = GetEditedSquareFootageFromUser($"Square Feet ({originalOrder.Area}):  ", originalOrder);

                Console.Clear();

                ConsoleIO.DisplayOrderDetails(tempOrder, tempOrder.OrderDate);

                string yesOrNo = ConsoleIO.GetYesNoAnswerFromUser("Are you sure you want to change your order?");

                if (yesOrNo == "Y")
                {
                    OrderEditResponse editResponse = orderManager.EditOrder(tempOrder);
                    Console.WriteLine();
                    Console.WriteLine("Your order has been changed.");
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Your order was not changed.");
                }

                Console.WriteLine("Press any key to continue. ");
                Console.ReadLine();
            }
        }
        
        public ProductType GetEditedProductTypeFromUser(string prompt, Order original)
        {
            while (true)
            {
                Console.WriteLine(prompt);
                Console.Write("Enter desired flooring type - (C)arpet, (W)ood, (L)inolium, or (T)ile: ");

                string flooringType = Console.ReadLine().ToUpper();

                if (string.IsNullOrEmpty(flooringType))
                {
                    return original.Product.TypeOfProduct;
                }

                switch (flooringType)
                {
                    case "C":
                        return ProductType.Carpet;
                    case "W":
                        return ProductType.Wood;
                    case "L":
                        return ProductType.Laminate;
                    case "T":
                        return ProductType.Tile;
                    default:
                        Console.WriteLine("Please enter a valid choice.");
                        continue;
                }
            }
        }

        public decimal GetEditedSquareFootageFromUser(string prompt, Order original)
        {
            Console.Write(prompt);
            string area = Console.ReadLine();
            int sqft;

            if (string.IsNullOrEmpty(area))
            {
                return original.Area;

            }
            while (!int.TryParse(area, out sqft) || (sqft < 100))
            {
                Console.Write("Please enter a valid area, larger than 100 ft sq:  ");
                area = Console.ReadLine();
            }

            return sqft;
        }
    }
}
