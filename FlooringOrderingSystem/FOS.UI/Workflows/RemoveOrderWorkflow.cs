﻿using FOS.BLL.Factories;
using FOS.BLL.Managers;
using FOS.Models;
using FOS.Models.Responses;
using FOS.UI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.UI.Workflows
{
    public class RemoveOrderWorkflow
    {
        public void Execute()
        {
            Console.Clear();
            OrderManager orderManager = OrderManagerFactory.Create();

            Order orderToRemove = new Order();

            while (true)
            {

                Console.WriteLine("Lookup an order");
                Console.WriteLine(ConsoleIO.SeperatorBar);
                DateTime orderDate = ConsoleIO.GetValidDateFromUser();

                OrdersLookupResponse response = orderManager.DisplayOrders(orderDate);

                if (response.Success)
                {
                    foreach (var order in response.Orders)
                    {
                        orderManager.CreateTaxInfoResponse(order);
                        orderManager.CreateProductInfoResponse(order);
                        ConsoleIO.DisplayOrderDetails(order, orderDate);
                    }
                }
                else
                {
                    Console.WriteLine("No orders found. Press any key to continue. ");
                    Console.ReadKey();
                    break;
                }

                //pick an order from a list
                int index = ConsoleIO.GetOrderIndexFromUser("What order number would you like to remove?  ", response.Orders.Count());
                index--;

                orderToRemove = response.Orders[index];

                orderToRemove.OrderDate = orderDate;

                Console.WriteLine();

                Console.Clear();
                Console.WriteLine("++++++++++ORDER TO BE DELETED+++++++++++");
                Console.WriteLine();
                ConsoleIO.DisplayOrderDetails(orderToRemove, orderDate);
                string yesOrNo = ConsoleIO.GetYesNoAnswerFromUser("Are you sure you would you like to remove this order?  ");

                if (yesOrNo == "Y")
                {
                    OrderRemoveResponse removeResponse = orderManager.RemoveOrder(orderToRemove);
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine("Your order has been removed.");
                }
                else
                {
                    Console.WriteLine("Your order is still active.");
                }

                Console.WriteLine();
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                break;
            }
        }
    }
}
