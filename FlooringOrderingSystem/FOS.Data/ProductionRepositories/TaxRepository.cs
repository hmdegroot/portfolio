﻿using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Models;
using System.IO;
using FOS.Models.Settings;

namespace FOS.Data.ProductionRepositories
{
    public class TaxRepository : ITaxRepository
    {
        private string _directory;

        public TaxRepository(string directory)
        {
            _directory = directory;
        }

        public List<Taxes> LoadTaxes()
        {
            List<Taxes> taxes = new List<Taxes>();

            using (StreamReader sr = new StreamReader(_directory))
            {
                sr.ReadLine();
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    Taxes listTaxes = new Taxes();

                    string[] columns = line.Split(',');

                    listTaxes.StateAbbreviation = columns[0];
                    listTaxes.StateName = columns[1];
                    listTaxes.TaxRate = decimal.Parse(columns[2]);
                    taxes.Add(listTaxes);
                }
            }
            return taxes;
        }
    }
}
