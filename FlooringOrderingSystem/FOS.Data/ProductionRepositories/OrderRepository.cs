﻿using FOS.Models;
using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Models.Settings;


namespace FOS.Data.ProductionRepositories
{
    public class OrderRepository : IOrderRepository
    {
        private string _directory;
        private string _fullFileName;
        private List<Order> _orders;

        public OrderRepository(string directory)
        {
            _directory = directory;
        }

        private void FindOrMakeFileDirectory(bool canBeEmpty)
        {
            if (!File.Exists(_fullFileName) && canBeEmpty == true)
            {
                TextWriter tw = new StreamWriter(_fullFileName);
                tw.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaberCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
                tw.Close();
            }
            else if (!File.Exists(_fullFileName) && canBeEmpty == false)
            {
                _fullFileName = null;
            }
        }

        public List<Order> DisplayOrders(DateTime date)
        {
            if (!File.Exists(_fullFileName))
            {
                _fullFileName = ConvertDateToFileName(date);
                FindOrMakeFileDirectory(false);
            }
            if (_orders == null)
            {
                _orders = LoadOrders();
            }
            return _orders;
        }

        public void CreateNewOrder(Order order)
        {
            _fullFileName = ConvertDateToFileName(order.OrderDate);
            FindOrMakeFileDirectory(true);
            var orders = LoadOrders();

            orders.Add(order);
            CreateOrderFile(orders);
        }

        public void RemoveOrder(Order order)
        {
            if (_orders == null)
            {
                Console.WriteLine("No orders on that date.");
            }
            if (_orders != null)
            {
                _orders.RemoveAt(order.OrderNumber - 1);
                CreateOrderFile(_orders);
            }
        }

        public void EditOrder(Order order)
        {
            if (_orders == null)
            {
                Console.WriteLine("No orders on that date.");
            }
            else
            {
                int index = order.OrderNumber;
                index--;
                _orders[index] = order;
                CreateOrderFile(_orders);
            }
        }

        public List<Order> LoadOrders()
        {
            List<Order> orders = new List<Order>();

            using (StreamReader sr = new StreamReader(_fullFileName))
            {
                sr.ReadLine();
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    Order newOrder = new Order();

                    string[] columns = line.Split(',');

                    newOrder.OrderNumber = int.Parse(columns[0]);
                    newOrder.CustomerName = columns[1];
                    newOrder.CustomerState = columns[2];
                    newOrder.TaxInfo.TaxRate = decimal.Parse(columns[3]);
                    newOrder.Product.TypeOfProduct = (ProductType)Enum.Parse(typeof(ProductType), columns[4]);
                    newOrder.Area = decimal.Parse(columns[5]);
                    newOrder.Product.CostPerSquareFoot = decimal.Parse(columns[6]);
                    newOrder.Product.LaborCostPerSquareFoot = decimal.Parse(columns[7]);
                    orders.Add(newOrder);
                }

            }
            _orders = orders;
            return orders;
        }

        public void CreateOrderFile(List<Order> orders)
        {

            if (File.Exists(_fullFileName))
                File.Delete(_fullFileName);

            using (StreamWriter sr = new StreamWriter(_fullFileName))
            {
                sr.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaberCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
                int count = 1;

                foreach (var order in orders)
                {
                    order.OrderNumber = count;
                    sr.WriteLine(WriteLineForOrder(order, orders));
                    count++;
                }
            }
        }

        private string WriteLineForOrder(Order order, List<Order> orders)
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", order.OrderNumber, order.CustomerName, order.CustomerState, order.TaxInfo.TaxRate, ConvertFromTypeEnum(order), order.Area, order.Product.CostPerSquareFoot, order.Product.LaborCostPerSquareFoot, order.MaterialCost, order.LaborCost, order.Tax, order.Total);
        }

        public string ConvertDateToFileName(DateTime date)
        {
            string dateToString = date.ToString("MMddyyyy");
            string datedFileName = $"Orders_{dateToString}.txt";
            _fullFileName = $"{_directory}{ datedFileName}";
            return _fullFileName;
        }

        private string ConvertFromTypeEnum(Order order)
        {
            if (order.ProductType == ProductType.Carpet)
            {
                return "Carpet";
            }
            else if (order.ProductType == ProductType.Laminate)
            {
                return "Laminate";
            }
            else if (order.ProductType == ProductType.Tile)
            {
                return "Tile";
            }
            else
            {
                return "Wood";
            }
        }


    }
}
