﻿using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Models;
using System.IO;
using FOS.Models.Settings;

namespace FOS.Data.ProductionRepositories
{
    public class ProductRepository : IProductRepository
    {
        private string _directory;

        public ProductRepository(string directory)
        {
            _directory = directory;
        }

        public List<Product> LoadProducts()
        {
            List<Product> products = new List<Product>();

            using (StreamReader sr = new StreamReader(_directory))
            {
                sr.ReadLine();
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    Product newProduct = new Product();

                    string[] columns = line.Split(',');

                    newProduct.TypeOfProduct = (ProductType)Enum.Parse(typeof(ProductType), columns[0]); 
                    newProduct.CostPerSquareFoot = decimal.Parse(columns[1]);
                    newProduct.LaborCostPerSquareFoot = decimal.Parse(columns[2]);

                    products.Add(newProduct);
                }
            }
            return products;
        }
    }
}
