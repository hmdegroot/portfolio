﻿using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Models;

namespace FOS.Data.TestRepositories
{
    public class TaxTestRepository : ITaxRepository
    {
        public List<Taxes> LoadTaxes()
        {
            return new List<Taxes>()
                {
               new Taxes()
            {
                StateAbbreviation = "OH",
                StateName = "Ohio",
                TaxRate = 6.32m
            },
            new Taxes()
            {
                StateAbbreviation = "NE",
                StateName = "Nebraska",
                TaxRate = 4.32m
            },
            new Taxes()
            {
                StateAbbreviation = "CO",
                StateName = "Colorado",
                TaxRate = 7.31m
            }
                };
        }
    }
}





