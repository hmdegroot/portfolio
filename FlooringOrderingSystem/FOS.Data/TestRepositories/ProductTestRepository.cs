﻿using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Models;

namespace FOS.Data.TestRepositories
{
    public class ProductTestRepository : IProductRepository
    {
        public static List<Product> _products = new List<Product>()
        {
            new Product
            {
                TypeOfProduct = ProductType.Tile,
                CostPerSquareFoot = 10.00M,
                LaborCostPerSquareFoot = 16.00M
            },
            new Product
            {
                TypeOfProduct = ProductType.Wood,
                CostPerSquareFoot = 18.00M,
                LaborCostPerSquareFoot = 14.00M
            },
            new Product
            {
                TypeOfProduct = ProductType.Laminate,
                CostPerSquareFoot = 5.00M,
                LaborCostPerSquareFoot = 7.00M
            },
            new Product
            {
                TypeOfProduct = ProductType.Carpet,
                CostPerSquareFoot = 9.00M,
                LaborCostPerSquareFoot = 8.00M
            }
        };

        public List<Product> LoadProducts()
        {
            return _products;
        }
    }
}
