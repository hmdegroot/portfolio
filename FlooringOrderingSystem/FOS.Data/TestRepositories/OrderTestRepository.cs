﻿using FOS.Models;
using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.Data.TestRepositories
{
    public class OrderTestRepository : IOrderRepository
    {
        private static Dictionary<DateTime, List<Order>> Data { get; set; }

        public OrderTestRepository()
        {
            Data = new Dictionary<DateTime, List<Order>>();

            Data.Add(new DateTime(2016, 12, 25), new List<Order>()
            {
                new Order()
                {
                OrderNumber = 1,
                CustomerName = "Black",
                OrderDate = new DateTime (2016,12,25),
                CustomerState = "OH",
                Area = 203,
                ProductType = ProductType.Tile
                },
                new Order()
                {
                OrderNumber = 2,
                CustomerName = "Oliver Dean",
                OrderDate = new DateTime (2016,12,25),
                CustomerState = "NE",
                Area = 500,
                ProductType = ProductType.Wood
                },
                new Order
                {
                OrderNumber = 3,
                OrderDate = new DateTime (2016,12,25),
                CustomerName = "Vernon",
                CustomerState = "CO",
                Area = 700,
                ProductType = ProductType.Carpet
                },
                new Order
                {
                OrderNumber = 4,
                CustomerName = "Jen",
                OrderDate = new DateTime (2016,12,25),
                CustomerState = "NE",
                Area = 1000,
                ProductType = ProductType.Laminate
                }
              }
            );
        }

        public void EditOrder(Order order)
        {
            if (Data.ContainsKey(order.OrderDate))
            {
                for (int i = 0; i < Data[order.OrderDate].Count; i++)
                {
                    if (Data[order.OrderDate][i].OrderNumber == order.OrderNumber)
                    {
                        Data[order.OrderDate][i] = order;
                        break;
                    }
                }
            }
        }
        
        public void RemoveOrder(Order order)
        {
            if (Data.ContainsKey(order.OrderDate))
            {
                Data[order.OrderDate].Remove(order);
            }
            else
            {
                Console.WriteLine("Order not deleted.");
            }
        }

        public void CreateNewOrder(Order order)
        {
            order.OrderNumber = GenerateOrderNumber(order);

            if (Data.ContainsKey(order.OrderDate))
            {

                Data[order.OrderDate].Add(order);
            }
            else
            {
                Data.Add(order.OrderDate, new List<Order>() { order });
            }
        }

        public int GenerateOrderNumber(Order order)
        {
            int orderNumber;
            order.OrderNumber = Data[order.OrderDate].Count();
            orderNumber = order.OrderNumber++;

            return orderNumber;
        }

        public List<Order> DisplayOrders(DateTime date)
        {
            return Data[date];
        }
    }
}
