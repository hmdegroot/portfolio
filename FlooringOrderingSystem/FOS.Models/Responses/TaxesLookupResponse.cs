﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.Models.Responses
{
    public class TaxesLookupResponse : Response
    {
        public List<Taxes> TaxList { get; set; }
    }
}
