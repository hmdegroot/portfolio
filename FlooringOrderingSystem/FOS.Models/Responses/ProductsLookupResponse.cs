﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.Models.Responses
{
    public class ProductsLookupResponse : Response
    {
        public List<Product> Products { get; set; }
    }
}
