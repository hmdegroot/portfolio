﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.Models
{
    public class Order
    {
        public int OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerState { get; set; }
        public Taxes TaxInfo { get; set; }
        public ProductType ProductType { get; set; }
        public Product Product { get; set; }
        public decimal Area { get; set; }
        public decimal MaterialCost
        {
            get { return Area * Product.CostPerSquareFoot; } 
        }
        public decimal LaborCost
        {
            get { return Area * Product.LaborCostPerSquareFoot; } 
        }
        public decimal Tax
        {
            get { return (MaterialCost + LaborCost) * (TaxInfo.TaxRate/ 100); } 
        }
        public decimal Total
        {
            get { return MaterialCost + LaborCost + Tax; } 
        }
        public Order()
        {
            this.Product= new Product();
            this.TaxInfo = new Taxes();
        }
    }
}
