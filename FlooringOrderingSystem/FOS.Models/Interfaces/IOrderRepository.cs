﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.Models.Interfaces
{
    public interface IOrderRepository
    {
        void RemoveOrder(Order order); //delete
        void CreateNewOrder(Order order);//create
        void EditOrder(Order order); //update
        List<Order> DisplayOrders(DateTime date);
    }
}