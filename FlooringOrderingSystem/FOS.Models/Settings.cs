﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.Models.Settings
{
    public class Settings
    {
        private static string _directory;

        public static string GetDirectory()
        {
            // has it been loaded yet?
            if (string.IsNullOrEmpty(_directory))
            {
                // nope, load it
                _directory = ConfigurationManager.AppSettings["Directory"].ToString();
            }

            return _directory;
        }

        public static string GetTaxPath()
        {
            return GetDirectory() + "Taxes.txt";
        }

        public static string GetProductsPath()
        {
            return GetDirectory() + "Products.txt";
        }
    }
}


