﻿using FOS.BLL.Managers;
using FOS.Models;
using FOS.Models.Interfaces;
using FOS.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Data.Mocks;

namespace FOS.BLL.Managers
{
    public class OrderManager
    {
        private IOrderRepository _orderRepository;
        private ITaxRepository _taxRepository;
        private IProductRepository _productRepository;
        private TaxInfoMockRepo taxInfoMockRepo;
        private ProductInfoMockRepo productInfoMockRepo;

        public OrderManager(IOrderRepository orderRepository,
            ITaxRepository taxRepository,
            IProductRepository productRepository)

        {
            _orderRepository = orderRepository;
            _taxRepository = taxRepository;
            _productRepository = productRepository;
        }

 

        public Response CreateTaxInfoResponse(Order newOrder)
        {
            Response response = new Response();
            // get tax rate
            var taxInfo = _taxRepository.LoadTaxes().FirstOrDefault(t => t.StateAbbreviation == newOrder.CustomerState);

            if (taxInfo == null)
            {
                response.Success = false;
                response.Message = "Invalid tax state";
            }
            else
            {
                response.Success = true;
                newOrder.TaxInfo = taxInfo;
            }

            return response;
        }

        public Response CreateProductInfoResponse(Order order)
        {
            Response response = new Response();
            var productInfo = _productRepository.LoadProducts().FirstOrDefault(t => t.TypeOfProduct == order.ProductType);

            if (productInfo == null)
            {
                response.Success = false;
                response.Message = "Invalid product type.";

            }
            else
            {
                order.Product = productInfo;
                response.Success = true;
            }
            return response;
        }

        public OrderAddResponse AddOrder(Order newOrder, DateTime orderDate)
        {
            OrderAddResponse response = new OrderAddResponse();

            try
            {
                _orderRepository.CreateNewOrder(newOrder);

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public OrderEditResponse EditOrder(Order editedOrder)
        {
            OrderEditResponse response = new OrderEditResponse();

            try
            {
                _orderRepository.EditOrder(editedOrder);

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public OrdersLookupResponse DisplayOrders(DateTime date)
        {
            OrdersLookupResponse response = new OrdersLookupResponse();

            try
            {
                response.Orders = _orderRepository.DisplayOrders(date);

                if (response.Orders.Count == 0)
                {
                    response.Success = false;
                    response.Message = "No orders were found.";
                }
                else
                {
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public OrderRemoveResponse RemoveOrder(Order order)
        {
            OrderRemoveResponse response = new OrderRemoveResponse();

            _orderRepository.RemoveOrder(order);

            try
            {
               if (response.Order == null)
                {
                    response.Success = false;
                    response.Message = "No orders were found.";
                }
                else
                {
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public OrderManager(TaxInfoMockRepo taxInfoMockRepo)
        {
            this.taxInfoMockRepo = taxInfoMockRepo;
        }

        public OrderManager(ProductInfoMockRepo productInfoMockRepo)
        {
            this.productInfoMockRepo = productInfoMockRepo;
        }
    }
}
