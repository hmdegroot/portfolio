﻿using FOS.Models;
using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Models.Responses;

namespace FOS.BLL.Managers
{
    public class ProductManager
    {
        private IProductRepository _productRepository;

        public ProductManager(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public ProductsLookupResponse LookupProducts()
        {
            ProductsLookupResponse response = new ProductsLookupResponse();

            response.Products = _productRepository.LoadProducts();

            if(response.Products == null)
            {
                response.Success = false;
                response.Message = "Not a valid product";
            }
            else
            {
                response.Success = true;
            }
            return response;
        }
    }
}
