﻿using FOS.BLL.Managers;
using FOS.Data.TestRepositories;
using FOS.Models.Interfaces;
using FOS.Models.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.BLL.Managers
{
    public class TaxManager
    {
        private ITaxRepository _taxRepository;

        public TaxManager(ITaxRepository taxRepository)
        {
            _taxRepository = taxRepository;
        }

        public TaxesLookupResponse LookupTaxes()
        {
            TaxesLookupResponse response = new TaxesLookupResponse();

            response.TaxList = _taxRepository.LoadTaxes();

            if(response.TaxList == null)
            {
                response.Success = false;
                response.Message = "Taxes no found";
            }
            else
            {
                response.Success = true ;
            }
            return response;
        }
        
    }
}
