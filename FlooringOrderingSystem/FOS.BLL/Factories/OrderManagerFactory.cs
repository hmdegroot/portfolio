﻿using FOS.BLL.Managers;
using FOS.Data;
using FOS.Data.ProductionRepositories;
using FOS.Data.TestRepositories;
using FOS.Models.Settings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//todo: update tax and product constructors
namespace FOS.BLL.Factories
{
    public static class OrderManagerFactory
    {
        public static OrderManager Create()
        {
            string mode = ConfigurationManager.AppSettings["Mode"].ToString();

            switch(mode)
            {
                case "Test":
                    return new OrderManager(new OrderTestRepository(), new TaxTestRepository(), new ProductTestRepository());
                case "Prod":
                    return new OrderManager(new OrderRepository(Settings.GetDirectory()), new TaxRepository(Settings.GetTaxPath()), new ProductRepository(Settings.GetProductsPath()));
                default:
                    throw new Exception("Mode value in app config is not valid");
            }
        }
    }
}
