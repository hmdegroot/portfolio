﻿using FOS.BLL.Managers;
using FOS.Data.TestRepositories;
using FOS.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Data.ProductionRepositories;
using FOS.Models.Settings;

namespace FOS.BLL.Factories
{
    public class TaxManagerFactory
    {
        public static TaxManager Create()
        {
            string mode = ConfigurationManager.AppSettings["Mode"].ToString();

            switch (mode)
            {
                case "Test":
                    return new TaxManager(new TaxTestRepository());
                case "Prod":
                    return new TaxManager(new TaxRepository(Settings.GetTaxPath()));
                default:
                    throw new NotImplementedException("Mode value in the app config is not valid");
            }
        }
    }
}
