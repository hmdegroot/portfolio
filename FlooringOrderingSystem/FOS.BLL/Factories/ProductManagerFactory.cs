﻿using FOS.BLL.Managers;
using FOS.Data.ProductionRepositories;
using FOS.Data.TestRepositories;
using FOS.Models.Settings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOS.BLL.Factories
{
    public class ProductManagerFactory
    {
        public static ProductManager Create()
        {
            string mode = ConfigurationManager.AppSettings["Mode"].ToString();

            switch (mode)
            {
                case "Test":
                    return new ProductManager(new ProductTestRepository());
                case "Prod":
                    return new ProductManager(new ProductRepository(Settings.GetProductsPath()));
                default:
                    throw new Exception("Mode value in app config is not valid.");
            }
        }
    }
}
