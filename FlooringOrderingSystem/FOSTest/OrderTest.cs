﻿using FOS.BLL;
using FOS.BLL.Factories;
using FOS.BLL.Managers;
using FOS.Models;
using FOS.Models.Responses;

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS.Data.Mocks;

namespace FOSTest
{
    //todo: test test test, review eric's test and implement
    [TestFixture]
    public class OrderTest
    {
        [Test]
        public void CanLoadOrder()
        {
            DateTime date = new DateTime(2016, 12, 25);
            OrderManager manager = OrderManagerFactory.Create();

            OrdersLookupResponse response = manager.DisplayOrders(date);

            Assert.IsNotNull(response.Orders);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public void OrderCalculationsTest()
        {
            Order order = new Order();

            order.Area = 100;
            order.Product.CostPerSquareFoot = 1;
            order.Product.LaborCostPerSquareFoot = 1;
            order.TaxInfo.TaxRate = 10;


            Assert.AreEqual(100, order.MaterialCost);
            Assert.AreEqual(100, order.LaborCost);
            Assert.AreEqual(20, order.Tax);
            Assert.AreEqual(220, order.Total);
        }

        [Test]
        public void OrderManagerRejectsInvalidState()
        {
            OrderManager om = new OrderManager(new TaxInfoMockRepo());
            Order orderToAdd = new Order();
            orderToAdd.CustomerState = "NE";
            DateTime date = new DateTime(2016, 12, 25);

            Response result = om.AddOrder(orderToAdd, date);

            Assert.IsFalse(result.Success);
        }

        [Test]
        public void ProductManagerRejectInvalidState()
        {
            OrderManager om = new OrderManager(new ProductInfoMockRepo());
            Order orderToAdd = new Order();
           // orderToAdd.Product = "Rug";
        }
    }
}
