﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FOS.BLL;
using FOS.Data;
using FOS.Models;
using System.IO;
using FOS.Data.ProductionRepositories;
using FOS.BLL.Managers;
using FOS.BLL.Factories;
using FOS.Models.Settings;

namespace FOSTest
{
    [TestFixture]
    public class RepoTests
    {
        private const string _filePath = @"C:\Users\hmdeg\Documents\heather-degroot-individual-work\FlooringOrderingSystem\FOS.Data\dataFiles\Production\Orders_06012017.txt";
        private const string _taxFilePath = @"C:\Users\hmdeg\Documents\heather-degroot-individual-work\FlooringOrderingSystem\FOS.Data\dataFiles\Production\Taxes.txt";
        private const string _productsFilePath = @"C:\Users\hmdeg\Documents\heather-degroot-individual-work\FlooringOrderingSystem\FOS.Data\dataFiles\Production\Products.txt";

        private const string _originaOrdersFilePath = @"C:\Users\hmdeg\Documents\heather-degroot-individual-work\FlooringOrderingSystem\FOS.Data\dataFiles\OriginalData\Orders_06012017.txt";
        private const string _originalTaxFilePath = @"C:\Users\hmdeg\Documents\heather-degroot-individual-work\FlooringOrderingSystem\FOS.Data\dataFiles\OriginalData\Taxes.txt";
        private const string _originalProductsFilePath = @"C:\Users\hmdeg\Documents\heather-degroot-individual-work\FlooringOrderingSystem\FOS.Data\dataFiles\OriginalData\Products.txt";

        [SetUp]
        public void Setup()
        {
            if (File.Exists(_filePath))
            {
                File.Delete(_filePath);
            }

            File.Copy(_originaOrdersFilePath, _filePath);
        }

        [Test]
        public void CanReadDataFromFile()
        {
            DateTime date = new DateTime(2017, 06, 01);
            OrderRepository repo = new OrderRepository(Settings.GetDirectory());
            List<Order> orders = repo.DisplayOrders(date);

            Assert.AreEqual(1, orders.Count());
            Assert.AreEqual("Wise", orders[0].CustomerName);
        }

        [Test]
        public void CadAddOrderToFile()
        {

            OrderRepository repo = new OrderRepository(Settings.GetDirectory());
            Order order = new Order();
            order.CustomerName = "JJ";
            order.CustomerState = "IN";
            order.ProductType = ProductType.Wood;
            order.OrderDate = new DateTime(2017, 06, 01);
            order.Area = 200;

            repo.CreateNewOrder(order);

            OrderRepository testRepo = new OrderRepository(_filePath);
            List<Order> orders = repo.DisplayOrders(order.OrderDate);
            

            Assert.AreEqual(2, orders.Count());

            Assert.AreEqual("JJ", orders[1].CustomerName);
        }

        [Test]
        public void CanRemoveOrderFromFile()
        {
            DateTime date = new DateTime(2017, 06, 01);
            OrderRepository repo = new OrderRepository(Settings.GetDirectory());
            List<Order> orders = repo.DisplayOrders(date);
            Order order = orders[0];
            order.OrderDate = date;
            
            
            repo.RemoveOrder(order);

           Assert.AreEqual(0, orders.Count());
        }


        [Test]
        public void CanEditDataInFile()
        {
            OrderRepository repo = new OrderRepository(Settings.GetDirectory());
            DateTime date = new DateTime(2017, 06, 01);
            repo.ConvertDateToFileName(date);
            List<Order> orders = repo.LoadOrders();
            
            
            Order editedOrder = new Order();
            editedOrder.CustomerName = "Malcom";
            editedOrder.CustomerState = "IN";
            editedOrder.OrderNumber = 1;
            editedOrder.OrderDate = date;
            repo.EditOrder(editedOrder);

            orders = repo.LoadOrders();

            Assert.AreEqual(1, orders.Count());
            Assert.AreEqual("Malcom", orders[0].CustomerName);
            Assert.AreEqual("IN", orders[0].CustomerState);
        }

        [Test]
        public void CanReadTaxFile()
        {
            TaxRepository repo = new TaxRepository(Settings.GetTaxPath());

            List<Taxes> taxes = repo.LoadTaxes();

            Assert.IsTrue(taxes.Count() > 0);
        }

        [Test]
        public void CanReadProductFile()
        {
            ProductRepository repo = new ProductRepository(Settings.GetProductsPath());

            List<Product> products = repo.LoadProducts();

            Assert.IsTrue(products.Count() > 0);
        }
    }
}

