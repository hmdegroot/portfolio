USE DvdLibrary
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdSampleData')
		DROP PROCEDURE DvdSampleData
GO

CREATE PROCEDURE DvdSampleData AS
BEGIN
	DELETE FROM Dvds;
	DELETE FROM Ratings;
	DELETE FROM AspNetUsers WHERE id = '00000000-0000-0000-0000-000000000000';
	
	DBCC CHECKIDENT ('Dvds', RESEED, 1)

	SET IDENTITY_INSERT Ratings ON

	INSERT INTO Ratings (RatingId, Rating)
	VALUES (1, 'G'), (2, 'PG'), (3, 'PG-13'), (4, 'R'), (5, 'NR')

	SET IDENTITY_INSERT Ratings OFF

	SET IDENTITY_INSERT Dvds ON

	INSERT INTO Dvds(DvdId, RatingId, Title, ReleaseYear, Director, Notes)
	VALUES(1, 4, 'TopGun', 1985, 'Tom Seleck', 'Tom Cruise is a fighter pilot that gets Russian and saves the world'), 
	(2, 1, 'Aladin', 1991, 'Jafar', 'Aladin finds a Genie'),
	(3, 2, 'The Santa Claus', 1995, 'Tim Allen', 'Santa falls off a ladder so Tim Allen must be Santa and not lose custody of his kid along the way')

	SET IDENTITY_INSERT Dvds OFF
	

END