USE DvdLibrary
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'RatingsSelectAll')
		DROP PROCEDURE RatingsSelectAll
GO

CREATE PROCEDURE RatingsSelectAll AS
BEGIN
	SELECT RatingId, Rating
	FROM Ratings
END

GO



IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdUpdate')
		DROP PROCEDURE DvdUpdate
GO

CREATE PROCEDURE DvdUpdate (
	@DvdId int output,
	@RatingId int,
	@Title varchar(200),
	@ReleaseYear smallint,
	@Director nvarchar(50),
	@Notes varchar(500),
	@Rating varchar(10)
	)AS
	
BEGIN
	UPDATE Dvds SET
		RatingId = @RatingId, 
		Title = @Title,
		ReleaseYear = @ReleaseYear,
		Director = @Director,
		Notes = @Notes,
		Rating = @Rating
	WHERE DvdId = @DvdId
	
END

GO


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdInsert')
		DROP PROCEDURE DvdInsert
GO

CREATE PROCEDURE DvdInsert (
	@DvdId int output,
	@RatingId int,
	@Title varchar(200),
	@ReleaseYear smallint,
	@Director nvarchar(50),
	@Notes varchar(500)
	)AS
	
BEGIN
	INSERT INTO Dvds(RatingId, Title, ReleaseYear, Director, Notes)
	VALUES(@RatingId, @Title, @ReleaseYear, @Director, @Notes)

	SET @DvdId = SCOPE_IDENTITY();
END

GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdDelete')
		DROP PROCEDURE DvdDelete
GO

CREATE PROCEDURE DvdDelete (
	@DvdId int
)AS
BEGIN
	BEGIN TRANSACTION

	DELETE FROM Dvds WHERE DvdId = @DvdId

	COMMIT TRANSACTION
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdSelect')
		DROP PROCEDURE DvdSelect
GO

CREATE PROCEDURE DvdSelect (
	@DvdId int
)AS
BEGIN
	SELECT DvdId, Title, d.RatingId, ReleaseYear, Director, Notes, r.Rating 
	FROM Dvds d
	INNER JOIN Ratings r ON r.RatingId = d.RatingId
	WHERE DvdId = @DvdId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdsSelectAll')
		DROP PROCEDURE DvdsSelectAll
GO

CREATE PROCEDURE DvdsSelectAll AS
BEGIN
	SELECT DvdId, Title, ReleaseYear, Director, d.RatingId, r.Rating, Notes
	FROM Dvds d
	INNER JOIN Ratings r ON r.RatingId = d.RatingId
	ORDER BY Title DESC
END

GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdDirectorSearch')
		DROP PROCEDURE DvdDirectorSearch
GO

CREATE PROCEDURE DvdDirectorSearch (
	@Director varchar(500)
)AS
BEGIN
	SELECT DvdId, Title, d.RatingId, ReleaseYear, Director, Notes, r.Rating 
	FROM Dvds d
	INNER JOIN Ratings r ON r.RatingId = d.RatingId
	WHERE Director LIKE '%'+ @Director+'%'
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdTitleSearch')
		DROP PROCEDURE DvdTitleSearch
GO

CREATE PROCEDURE DvdTitleSearch (
	@Title varchar(500)
)AS
BEGIN
	SELECT DvdId, Title, d.RatingId, ReleaseYear, Director, Notes, r.Rating 
	FROM Dvds d
	INNER JOIN Ratings r ON r.RatingId = d.RatingId
	WHERE Title LIKE '%'+@Title+'%'
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdRatingSearch')
		DROP PROCEDURE DvdRatingSearch
GO

CREATE PROCEDURE DvdRatingSearch (
	@RatingId int
)AS
BEGIN
	SELECT DvdId, Title, d.RatingId, ReleaseYear, Director, Notes, r.Rating 
	FROM Dvds d
	INNER JOIN Ratings r ON r.RatingId = d.RatingId
	WHERE d.RatingId = @RatingId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
	WHERE ROUTINE_NAME= 'DvdYearSearch')
		DROP PROCEDURE DvdYearSearch
GO

CREATE PROCEDURE DvdYearSearch (
	@Year int
)AS
BEGIN
	SELECT DvdId, Title, d.RatingId, ReleaseYear, Director, Notes, r.Rating 
	FROM Dvds d
	INNER JOIN Ratings r ON r.RatingId = d.RatingId
	WHERE ReleaseYear = @Year
END
GO

