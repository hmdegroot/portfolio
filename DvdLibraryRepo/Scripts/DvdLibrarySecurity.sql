use DvdLibrary
GO

CREATE LOGIN DvdLibraryApp WITH PASSWORD='testing123'
GO

CREATE USER DvdLibraryApp FOR LOGIN DvdLibraryApp

CREATE ROLE db_executor

GRANT EXECUTE TO db_executor

ALTER ROLE db_executor ADD MEMBER DvdLibraryApp

GRANT SELECT ON Dvds TO DvdLibraryApp
GRANT INSERT ON Dvds TO DvdLibraryApp
GRANT UPDATE ON Dvds TO DvdLibraryApp
GRANT DELETE ON Dvds TO DvdLibraryApp
GRANT CREATE TABLE TO DvdLibraryApp
GO