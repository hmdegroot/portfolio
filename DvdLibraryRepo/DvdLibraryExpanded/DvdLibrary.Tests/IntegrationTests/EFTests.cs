﻿using Dvd.Data.EF;
using DvdLibrary.Models.Tables;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//todo: fix delete test after doing how to add

namespace DvdLibrary.Tests.IntegrationTests
{
    [TestFixture]
    public class EFTests
    {
        [SetUp]
        public void Init()
        {
            using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "DvdSampleData";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Connection = cn;
                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        [Test]
        public void EFCanGetRatings()
        {

            using (var context = new EFRatingRepository())
            {
                var ratings = context.GetAll();

                Assert.AreEqual(5, ratings.Count());
                Assert.AreEqual("G", ratings[0].Rating);
                Assert.AreEqual(1, ratings[0].RatingId);
            }
        }

        [Test]
        public void EFCanGetSingleDvd()
        {
            EFDvdRepository repo = new EFDvdRepository();
            var dvd = repo.GetById(1);

            Assert.IsNotNull(dvd);

            Assert.AreEqual(1, dvd.DvdId);
            Assert.AreEqual("TopGun", dvd.Title);
            Assert.AreEqual("Tom Seleck", dvd.Director);
            Assert.AreEqual(1985, dvd.ReleaseYear);
            Assert.AreEqual("Tom Cruise is a fighter pilot that gets Russian and saves the world", dvd.Notes);
            Assert.AreEqual(4, dvd.RatingId);
        }


        [Test]
        public void EFCanGetDvds()
        {
            var context = new EFDvdRepository();
            var dvds = context.GetAllDvds().ToList();
            Assert.AreEqual(3, dvds.Count());

            Assert.AreEqual(1, dvds[0].DvdId);

        }

        [Test]
        public void CanAddDvdEF()
        {
            Dvds dvdToAdd = new Dvds();

            var repo = new EFDvdRepository();

            dvdToAdd.Title = "Iron Eagle";
            dvdToAdd.ReleaseYear = 1984;
            dvdToAdd.RatingId = 4;
            dvdToAdd.Notes = "When you are in high school, you can fly fighter jets.";
            dvdToAdd.Director = "unknown";

            repo.Insert(dvdToAdd);

            Assert.AreEqual(4, dvdToAdd.DvdId);
        }
        [Test]
        public void CanDeleteDvdEF()
        {
            EFDvdRepository repo = new EFDvdRepository();

            Dvds dvdToAdd = new Dvds();
            
            dvdToAdd.Title = "Iron Eagle";
            dvdToAdd.ReleaseYear = 1984;
            dvdToAdd.RatingId = 4;
            dvdToAdd.Notes = "When you are in high school, you can fly fighter jets.";
            dvdToAdd.Director = "unknown";
            
            repo.Insert(dvdToAdd);

            var newDvd = repo.GetById(4);
            Assert.IsNotNull(newDvd);

            repo.Delete(4);
            newDvd = repo.GetById(4);

            Assert.IsNull(newDvd);
        }

        [Test]
        public void EFCanUpdateDvd()
        {
            Dvds dvdToAdd = new Dvds();

            var context = new EFDvdRepository();

            dvdToAdd.Title = "Iron Eagle";
            dvdToAdd.ReleaseYear = 1984;
            dvdToAdd.RatingId = 4;
            dvdToAdd.Notes = "When you are in high school, you can fly fighter jets.";
            dvdToAdd.Director = "unknown";

            context.Insert(dvdToAdd);

            dvdToAdd.Title = "Iron Beagle";
            dvdToAdd.ReleaseYear = 2016;
            dvdToAdd.RatingId = 2;
            dvdToAdd.Notes = "When you are in high school, you can fly fighter beagles now.";
            dvdToAdd.Director = "BeagleBeable";

            context.Update(dvdToAdd);

            var updatedDvd = context.GetById(4);

            Assert.AreEqual("Iron Beagle", updatedDvd.Title);
            Assert.AreEqual(2016, updatedDvd.ReleaseYear);
            Assert.AreEqual(2, updatedDvd.RatingId);
            Assert.AreEqual("When you are in high school, you can fly fighter beagles now.", updatedDvd.Notes);
            Assert.AreEqual("BeagleBeable", updatedDvd.Director);
        }

        [Test]
        public void EFCanSearchForTitle()
        {
            var context = new EFDvdRepository();

            var search = context.GetByTitle("TopGun");

            Assert.AreEqual(1, search.Count());
        }

        [Test]
        public void EFCanSearchForDirector()
        {
            var context = new EFDvdRepository();

            var search = context.GetByDirector("Jafar");

            Assert.AreEqual(1, search.Count());
        }

        [Test]
        public void EFCanSearchForRating()
        {
            var context = new EFDvdRepository();

            var search = context.GetByRating(2);

            Assert.AreEqual(1, search.Count());
        }

        [Test]
        public void EFCanSearchByYear()
        {
            var context = new EFDvdRepository();

            var search = context.GetByYear(1995);

            Assert.AreEqual(1, search.Count());
        }


    }
}
