﻿$(document).ready(function () {

    loadDvds();

    $('#create-dvd').click(function (event) {
        $('#errorMessages').empty();
        $('#dvd-add').show();
        $('#dvd-display').hide();
    });

    $('#cancel-add').click(function (event) {
        if (!confirm("Are you sure you do not want to add this Dvd?")) {
            return false;
        }
        else {
            $('#errorMessages').empty();
            loadDvds();
        }
    });

    $('#cancel-edit').click(function (event) {
        if (!confirm("Are you sure you do not want to edit this Dvd?")) {
            return false;
        }
        else {
            loadDvds();
            $('#editErrorMessages').empty();
        }
    });

    $('#confirm-edit').click(function (event) {
        $('#editErrorMessages').empty();
        var errorMessages = [];
        var year = $('#edit-year').val();
        var title = $('#edit-title').val();
        var reg = /^[0-9]+$/;
        var id = $('#edit-dvd-id').val();
        var director = $('#edit-director').val();
        var notes = $('#edit-notes').val();


        if (title == null || title == "") {
            errorMessage = "Title is required!";
            errorMessages.push(errorMessage);
        }
        if (year == null || year == "") {
            errorMessage = "Year is required!";
            errorMessages.push(errorMessage);
        }
        else if ((year.length) < 4 || (year.length) > 4) {
            errorMessage = "Year should be 4 digits";
            errorMessages.push(errorMessage);
        }
        else if (!reg.test(year)) {
            errorMessage = "Year should be numbers only";
            errorMessages.push(errorMessage);
        }

        if (errorMessages.length > 0) {
            $.each(errorMessages, function (index, message) {
                $('#editErrorMessages').append($('<li>').attr({ class: 'list-group-item list-group-item-danger' }).text(message));
            });

            return false;
        }

        var rating = $('#edit-rating').val();
        var ratingId = 0;

        if (rating == 'G') {
            ratingId = 1;
        }
        if (rating == 'PG') {
            ratingId = 2;
        }
        if (rating == 'PG-13') {
            ratingId = 3;
        }
        if (rating == 'R') {
            ratingId = 4;
        }
        if (rating == 'NR') {
            ratingId = 5;
        }



        $.ajax({
            type: 'PUT',
            url: 'http://localhost:52662/dvd/' + id,
            data: JSON.stringify({
                dvdId: id,
                title: $('#edit-title').val(),
                releaseYear: year,
                director: director,
                ratingId: ratingId,
                notes: notes,
                rating: rating
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json',

            success: function () {
                $('#edit-dvd-error').empty();
                $('#edit-title').empty();
                $('#edit-year').empty();
                $('#edit-director').empty();
                $('#edit-rating').empty();
                $('#edit-notes').empty();
                $('#edit-dvd-id').empty();
                loadDvds();
            },
            error: function () {
                alert: ('Somethings wrong with editing a movie.');
            }
        })
    });

    $('#confirm-add').click(function (event) {
        $('#errorMessages').empty();

        var errorMessages = [];
        var year = $('#add-year').val();
        var title = $('#add-title').val();
        var reg = /^[0-9]+$/;



        if (title == null || title == "") {
            errorMessage = "Title is required!";
            errorMessages.push(errorMessage);
        }
        if (year == null || year == "") {
            errorMessage = "Year is required!";
            errorMessages.push(errorMessage);
        }
        else if ((year.length) < 4 || (year.length) > 4) {
            errorMessage = "Year should be 4 digits";
            errorMessages.push(errorMessage);
        }
        else if (!reg.test(year)) {
            errorMessage = "Year should be numbers only";
            errorMessages.push(errorMessage);
        }

        if (errorMessages.length > 0) {
            $.each(errorMessages, function (index, message) {
                $('#errorMessages').append($('<li>').attr({ class: 'list-group-item list-group-item-danger' }).text(message));
            });
            return false;
        }

        var rating = $('#add-rating').val();
        var ratingId = 0;

        if (rating == 'G') {
            ratingId = 1;
        }
        if (rating == 'PG') {
            ratingId = 2;
        }
        if (rating == 'PG-13') {
            ratingId = 3;
        }
        if (rating == 'R') {
            ratingId = 4;
        }
        if (rating == 'NR') {
            ratingId = 5;
        }

        $('#dvd-add').hide();

        $.ajax({
            type: 'POST',
            url: 'http://localhost:52662/dvd',
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseYear: $('#add-year').val(),
                director: $('#add-director').val(),
                ratingId: ratingId,
                notes: $('#add-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json',

            success: function () {
                $('#add-dvd-error').empty();
                $('#add-title').empty();
                $('#add-year').empty();
                $('#add-director').empty();
                $('#add-rating').empty();
                $('#add-notes').empty();

                loadDvds();
            },
            error: function () {
                alert: ('Somethings wrong with adding a movie.');
            }
        })
    })

    $('#confirm-return').click(function (event) {
        loadDvds();
    });

    $('#search-button').click(function (event) {

        $("#searchErrorMessages").empty();
        var selected = $('#drop-box option:selected').text();
        var searchText = $('#search-box').val();
        var errorMessages = [];
        var movieTable = $('#movie-table');

        if (selected == null || selected == "" || selected == "Search Category") {
            errorMessage = "Please make a search category selection.";
            errorMessages.push(errorMessage);
        }
        if (searchText == null || searchText == "") {
            errorMessage = "Blank searches are not allowed.";
            errorMessages.push(errorMessage);
        }
        if (errorMessages.length > 0) {
            $.each(errorMessages, function (index, message) {
                $('#searchErrorMessages').append($('<li>').attr({ class: 'list-group-item list-group-item-danger' }).text(message));
            });
            return false;
        }

        if (selected == "Title") {
            clearDvds();
            $.ajax({
                type: 'GET',
                url: 'http://localhost:52662/dvds/title/' + searchText,
                success: function (dvdArray) {
                    $.each(dvdArray, function (index, dvd) {
                        var title = dvd.title;
                        var year = dvd.releaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var dvdId = dvd.dvdId;
                        var notesDvd = dvd.notes;

                        var row = '<tr>';
                        row += '<td><a onclick="showDvdDetails(' + dvdId + ')">' + title + '</a></td>';

                        row += '<td>' + year + '</td>';
                        row += '<td>' + director + '</td>';
                        row += '<td>' + rating + '</td>';
                        row += '<td class="text-center"><a onclick="showEditDvd(' + dvdId + ')">Edit</a>' + ' | ' + '<a onclick="deleteDvd(' + dvdId + ')">Delete</a></td>';
                        row += '</tr>';

                        movieTable.append(row);
                    });
                },
                error: function () {
                    $('#errorMessages')
                    .append($('<li>')
                        .attr({ class: 'list-group-item-danger' })
                        .text('Error calling web service. Please try again later.'));
                }
            })
        };

        if (selected == "Director") {
            clearDvds();
            $.ajax({
                type: 'GET',
                url: 'http://localhost:52662/dvds/director/' + searchText,
                success: function (dvdArray) {
                    $.each(dvdArray, function (index, dvd) {
                        var title = dvd.title;
                        var year = dvd.releaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var dvdId = dvd.dvdId;
                        var notesDvd = dvd.notes;

                        var row = '<tr>';
                        row += '<td><a onclick="showDvdDetails(' + dvdId + ')">' + title + '</a></td>';

                        row += '<td>' + year + '</td>';
                        row += '<td>' + director + '</td>';
                        row += '<td>' + rating + '</td>';
                        row += '<td class="text-center"><a onclick="showEditDvd(' + dvdId + ')">Edit</a>' + ' | ' + '<a onclick="deleteDvd(' + dvdId + ')">Delete</a></td>';
                        row += '</tr>';

                        movieTable.append(row);
                    });
                },
                error: function () {
                    $('#errorMessages')
                    .append($('<li>')
                        .attr({ class: 'list-group-item-danger' })
                        .text('Error calling web service. Please try again later.'));
                }
            })
        };

        if (selected == "Rating") {
            clearDvds();
            $.ajax({
                type: 'GET',
                url: 'http://localhost:52662/dvds/rating/' + searchText,
                success: function (dvdArray) {
                    $.each(dvdArray, function (index, dvd) {
                        var title = dvd.title;
                        var year = dvd.releaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var dvdId = dvd.dvdId;
                        var notesDvd = dvd.notes;

                        var row = '<tr>';
                        row += '<td><a onclick="showDvdDetails(' + dvdId + ')">' + title + '</a></td>';

                        row += '<td>' + year + '</td>';
                        row += '<td>' + director + '</td>';
                        row += '<td>' + rating + '</td>';
                        row += '<td class="text-center"><a onclick="showEditDvd(' + dvdId + ')">Edit</a>' + ' | ' + '<a onclick="deleteDvd(' + dvdId + ')">Delete</a></td>';
                        row += '</tr>';

                        movieTable.append(row);
                    });
                },
                error: function () {
                    $('#errorMessages')
                    .append($('<li>')
                        .attr({ class: 'list-group-item-danger' })
                        .text('Error calling web service. Please try again later.'));
                }
            })
        };
        if (selected == "Release Year") {
            clearDvds();
            $.ajax({
                type: 'GET',
                url: 'http://localhost:52662/dvds/year/' + searchText,
                success: function (dvdArray) {
                    $.each(dvdArray, function (index, dvd) {
                        var title = dvd.title;
                        var year = dvd.releaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var dvdId = dvd.dvdId;
                        var notesDvd = dvd.notes;

                        var row = '<tr>';
                        row += '<td><a onclick="showDvdDetails(' + dvdId + ')">' + title + '</a></td>';

                        row += '<td>' + year + '</td>';
                        row += '<td>' + director + '</td>';
                        row += '<td>' + rating + '</td>';
                        row += '<td class="text-center"><a onclick="showEditDvd(' + dvdId + ')">Edit</a>' + ' | ' + '<a onclick="deleteDvd(' + dvdId + ')">Delete</a></td>';
                        row += '</tr>';

                        movieTable.append(row);
                    });
                },
                error: function () {
                    $('#errorMessages')
                    .append($('<li>')
                        .attr({ class: 'list-group-item-danger' })
                        .text('Error calling web service. Please try again later.'));
                }
            })
        };
    })
});





function loadDvds() {
    var movieTable = $('#movie-table');
    clearDvds();
    $('#dvd-add').hide();
    $('#dvd-edit').hide();
    $('#dvd-display').show();
    $('#film-display').hide();


    $.ajax({
        type: 'GET',
        url: 'http://localhost:52662/dvds',
        success: function (dvdArray) {
            $.each(dvdArray, function (index, dvd) {
                var title = dvd.title;
                var year = dvd.releaseYear;
                var director = dvd.director;
                var rating = dvd.rating;
                var dvdId = dvd.dvdId;
                var notesDvd = dvd.notes;

                var row = '<tr>';
                row += '<td><a onclick="showDvdDetails(' + dvdId + ')">' + title + '</a></td>';

                row += '<td>' + year + '</td>';
                row += '<td>' + director + '</td>';
                row += '<td>' + rating + '</td>';
                row += '<td class="text-center"><a onclick="showEditDvd(' + dvdId + ')">Edit</a>' + ' | ' + '<a onclick="deleteDvd(' + dvdId + ')">Delete</a></td>';
                row += '</tr>';

                movieTable.append(row);
            });
        },
        error: function () {
            $('#errorMessages')
			.append($('<li>')
				.attr({ class: 'list-group-item-danger' })
				.text('Error calling web service. Please try again later.'));
        }
    })
}

function clearDvds() {
    $('#movie-table').empty();
};

function deleteDvd(dvdId) {
    if (!confirm("Are you sure you want to delete this DVD?")) {
        return false;
    }
    else {
        $.ajax({
            type: 'DELETE',
            url: 'http://localhost:52662/dvd/' + dvdId,
            success: function () {
                loadDvds();
            }
        })
    }
};

function showEditDvd(dvdId) {

    $('#dvd-edit').show();
    $('#dvd-display').hide();

    $.ajax({
        type: 'GET',
        url: 'http://localhost:52662/dvd/' + dvdId,
        success: function (data, status) {
            $('#edit-title').val(data.title);
            $('#edit-year').val(data.releaseYear);
            $('#edit-director').val(data.director);
            $('#edit-rating').val(data.rating);
            $('#edit-notes').val(data.notes);
            $('#edit-dvd-id').val(data.dvdId);
        },
        error: function () {
            alert: ('something is wrong. seek assistance.');
        }
    })
};

function showDvdDetails(dvdId) {
    $('#film-display').show();
    $('#dvd-display').hide();

    $.ajax({
        type: 'GET',
        url: 'http://localhost:52662/dvd/' + dvdId,
        success: function (data, status) {
            clearSingleDvd();
            var dvdName = '<h3>Title: ' + data.title + '</h3>';
            $('#title-display').append(dvdName);

            var dvdYear = '<h4>' + data.releaseYear + '</h4>';
            $('#display-year').append(dvdYear);

            var director = '<h4>' + data.director + '</h4>';
            $('#display-director').append(director);

            var rating = '<h4>' + data.rating + '</h4>';
            $('#display-rating').append(rating);

            var thisNote = '<h4>' + data.notes + '</h4>';
            $('#display-notes').append(thisNote);
        },
        error: function () {
            alert: ('servers down');
        }
    })
};

function clearSingleDvd() {
    $('#title-display').empty();
    $('#display-year').empty();
    $('#display-director').empty();
    $('#display-rating').empty();
    $('#display-notes').empty();
}



