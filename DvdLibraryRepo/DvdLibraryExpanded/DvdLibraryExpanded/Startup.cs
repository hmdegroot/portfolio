﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DvdLibraryExpanded.Startup))]
namespace DvdLibraryExpanded
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
