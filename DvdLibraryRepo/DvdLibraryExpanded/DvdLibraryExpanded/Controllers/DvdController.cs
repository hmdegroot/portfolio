﻿using Dvd.Data.ADO;
using Dvd.Data.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using DvdLibrary.Models.Tables;
using Dvd.Data.Factories;
using Dvd.Data;

namespace DvdLibraryExpanded.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DvdController : ApiController
    {

        [Route("dvds")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetAllDvds()
        {
            var repo = DvdRepositoryFactory.GetRepository();
            return Ok(repo.GetAllDvds());
        }

        [Route("dvd/{id}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Get(int id)
        {
            var repo = DvdRepositoryFactory.GetRepository();
            DvdListing dvd = new DvdListing();
            dvd = repo.GetById(id);
            
            if(dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvds/title/{title}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetTitle(string title)
        {
            var repo = DvdRepositoryFactory.GetRepository();
            var dvds = new List<Dvds>();
            dvds = repo.GetByTitle(title);

            if (dvds == null)
            {
                return NotFound();
            }

            return Ok(dvds);
        }

        [Route("dvds/year/{releaseYear}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetYear(int releaseYear)
        {
            var repo = DvdRepositoryFactory.GetRepository();
            var dvd = new List<Dvds>();
            dvd = repo.GetByYear(releaseYear);

            if (dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvds/director/{directorName}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetDirector(string directorName)
        {
            var repo = DvdRepositoryFactory.GetRepository();
            var dvd = new List<Dvds>();
            dvd = repo.GetByDirector(directorName);

            if (dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }

        [Route("dvds/rating/{rating}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetRating(string rating)
        {
            int ratingId = Settings.GetRatingId(rating);

            var repo = DvdRepositoryFactory.GetRepository();
            var dvd = new List<Dvds>();
            dvd = repo.GetByRating(ratingId);

            foreach(var d in dvd)
            {
                d.Rating = Settings.GetRatingName(ratingId);
            }


            if (dvd == null)
            {
                return NotFound();
            }

            return Ok(dvd);
        }



        [Route("dvd")]
        [AcceptVerbs("POST")]
        public IHttpActionResult Add(Dvds dvd)
        {
            var repo = DvdRepositoryFactory.GetRepository();
            repo.Insert(dvd);

            return Created($"dvd/{dvd.DvdId}", dvd);
        }

        [Route("dvd/{id}")]
        [AcceptVerbs("PUT")]
        public void Update( Dvds dvd) 
        {
            var repo = DvdRepositoryFactory.GetRepository();
            repo.Update(dvd);
        }

        [Route("dvd/{id}")]
        [AcceptVerbs("DELETE")]
        public void Delete(int id)
        {
            var repo = DvdRepositoryFactory.GetRepository();
            repo.Delete(id);
        }
    }
}
