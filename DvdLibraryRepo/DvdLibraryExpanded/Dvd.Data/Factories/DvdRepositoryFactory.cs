﻿using Dvd.Data.ADO;
using Dvd.Data.EF;
using Dvd.Data.Interfaces;
using Dvd.Data.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dvd.Data.Factories
{
    public class DvdRepositoryFactory
    {
        public static IDvdRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "ADO":
                    return new DvdRepositoryADO();
                case "Mock":
                    return new MockDvdRepository();
                case "EntityFramework":
                    return new EFDvdRepository();
                default:
                    throw new Exception("Could not find repo.");
            }
        }
    }
}
