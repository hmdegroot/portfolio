﻿using Dvd.Data.ADO;
using Dvd.Data.EF;
using Dvd.Data.Interfaces;
using Dvd.Data.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Dvd.Data.Factories
{
    public class RatingsRepositoryFactory
    {
        public static IRatingRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "ADO":
                    return new RatingRepositoryADO();
                case "Mocks":
                    return new MockRatingRepository();
                case "EntityFramework":
                    return new EFRatingRepository();
                default:
                    throw new Exception("Could not load ratings repo.");
            }
        }
    }
}
