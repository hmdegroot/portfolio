﻿using Dvd.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.Models.Tables;
using System.Collections;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
//to do: fix ef can load dvds

namespace Dvd.Data.EF
{
    public class EFDvdRepository : IDvdRepository
    {
        private EFDvdContext DvdContext { get; set; }

        public EFDvdRepository()
        {
            DvdContext = new EFDvdContext();
        }

        public void Delete(int dvdId)
        {

            Dvds dvd = DvdContext.Dvds.FirstOrDefault(i => i.DvdId == dvdId);
            DvdContext.Dvds.Remove(dvd);
            DvdContext.SaveChanges();

        }

        public DvdListing GetById(int dvdId)
        {
            return DvdContext.DvdListings.SqlQuery("dbo.DvdSelect @p0", dvdId).FirstOrDefault();

        }

        public void Insert(Dvds dvd)
        {
            DvdContext.Dvds.Add(dvd);
            DvdContext.SaveChanges();
        }
    
        public IEnumerable<Dvds> GetAllDvds()
        {
            return (from d in DvdContext.Dvds select d);
        }

        public void Update(Dvds dvd)
        {
            var dvdToUpdate = DvdContext.Dvds.Where(d => d.DvdId == dvd.DvdId).FirstOrDefault();
            dvdToUpdate.Title = dvd.Title;
            dvdToUpdate.Director = dvd.Director;
            dvdToUpdate.RatingId = dvd.RatingId;
            dvdToUpdate.ReleaseYear = dvd.ReleaseYear;
            dvdToUpdate.Notes = dvd.Notes;
            dvdToUpdate.Rating = Settings.GetRatingName(dvd.DvdId);

            DvdContext.SaveChanges();
        }

        public List<Dvds> GetByTitle(string title)
        {
            List<Dvds> listing = DvdContext.Dvds.Where(d => d.Title.Contains(title)).ToList();
            return listing;
        }

        IEnumerable<DvdListing> IDvdRepository.GetAllDvds()
        {
            var dvds = GetAllDvds();
            List<DvdListing> dvdList = new List<DvdListing>();


            foreach (var dvd in dvds)
            {
                DvdListing singleDvd = new DvdListing();

                singleDvd.Title = dvd.Title;
                singleDvd.ReleaseYear = dvd.ReleaseYear;
                singleDvd.DvdId = dvd.DvdId;
                singleDvd.Director = dvd.Director;
                singleDvd.RatingId = dvd.RatingId;
                singleDvd.Rating = Settings.GetRatingName(dvd.RatingId);

                dvdList.Add(singleDvd);

            }
            return dvdList;
        }

        public List<Dvds> GetByDirector(string director)
        {
            var dvds = DvdContext.Dvds.Where(d => d.Director.Contains(director)).ToList();
            List<Dvds> dvdList = new List<Dvds>();

            foreach (var dvd in dvds)
            {
                Dvds singleDvd = new Dvds();

                singleDvd.Title = dvd.Title;
                singleDvd.ReleaseYear = dvd.ReleaseYear;
                singleDvd.DvdId = dvd.DvdId;
                singleDvd.Director = dvd.Director;
                singleDvd.RatingId = dvd.RatingId;
                singleDvd.Rating = dvd.Rating;

                dvdList.Add(singleDvd);

            }
            return dvdList;

        }

        public List<Dvds> GetByRating(int ratingId)
        {
            var dvdRating = Settings.GetRatingName(ratingId);

            List<Dvds> listing = DvdContext.Dvds.Where(d => d.RatingId == ratingId).ToList();
            return listing;
        }

        public List<Dvds> GetByYear(int year)
        {
            List<Dvds> listing = DvdContext.Dvds.Where(d => d.ReleaseYear == year).ToList();
            return listing;
        }








        // msdn.microsoft.com/en-us/library/jj573936(v=vs.113).aspx
    }
}
