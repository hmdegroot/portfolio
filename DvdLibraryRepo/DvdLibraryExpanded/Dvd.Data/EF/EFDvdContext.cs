﻿using DvdLibrary.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dvd.Data.EF
{
    public class EFDvdContext : DbContext
    {
        public EFDvdContext() : base(Settings.GetConnectionString())
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<DvdListing> DvdListings { get; set; }
        public DbSet<Ratings> Rating { get; set; }
        public DbSet<Dvds> Dvds { get; set; }

    }
}

