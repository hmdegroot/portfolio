﻿using Dvd.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.Models.Tables;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Dvd.Data.EF
{
    public class EFRatingRepository : DbContext,IRatingRepository
    {
        private EFDvdContext DvdContext { get; set; }

        public EFRatingRepository()
        {
            DvdContext = new EFDvdContext();
        }
       
        

        public List<Ratings> GetAll()
        {
            return (from r in DvdContext.Rating select r).ToList();
        }


    }
}
