﻿using Dvd.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.Models.Tables;

namespace Dvd.Data.Mocks
{
    public class MockDvdRepository : IDvdRepository
    {

        private static List<Dvds> _dvdList = new List<Dvds>()
        {
            new Dvds
            {
                DvdId= 1,
                RatingId = 4,
                Rating = "R",
                Title = "TopGun",
                ReleaseYear = 1985,
                Director = "Tom Seleck",
                Notes = "Tom Cruise is a fighter pilot that gets Russian and saves the world"
            },
            new Dvds
            {
                DvdId = 2,
                RatingId = 1,
                Rating = "G",
                Title = "Aladin",
                ReleaseYear = 1991,
                Director = "Jafar",
                Notes = "Aladin finds a Genie"
            },
            new Dvds
            {
                DvdId = 3,
                RatingId = 2,
                Rating = "PG",
                Title = "The Santa Claus",
                ReleaseYear = 1995,
                Director = "Tim Allen",
                Notes = "Santa falls off a ladder so Tim Allen must be Santa and not lose custody of his kid along the way"

            }
        };



        public void Delete(int dvdId)
        {
            _dvdList.RemoveAll(c => c.DvdId == dvdId);
        }

        public IEnumerable<DvdListing> GetAllDvds()
        {

            List<DvdListing> dvdList = new List<DvdListing>();


            foreach (var dvd in _dvdList)
            {
                DvdListing singleDvd = new DvdListing();

                singleDvd.Title = dvd.Title;
                singleDvd.ReleaseYear = dvd.ReleaseYear;
                singleDvd.DvdId = dvd.DvdId;
                singleDvd.Director = dvd.Director;
                singleDvd.RatingId = dvd.RatingId;
                singleDvd.Rating = Settings.GetRatingName(dvd.RatingId);

                dvdList.Add(singleDvd);

            }
            return dvdList;
        }

        public List<Dvds> GetByDirector(string director)
        {

            var dvds = _dvdList.Where(d => d.Director.Contains(director)).ToList();
            List<Dvds> dvdList = new List<Dvds>();

            foreach (var dvd in dvds)
            {
                Dvds singleDvd = new Dvds();

                singleDvd.Title = dvd.Title;
                singleDvd.ReleaseYear = dvd.ReleaseYear;
                singleDvd.DvdId = dvd.DvdId;
                singleDvd.Director = dvd.Director;
                singleDvd.RatingId = dvd.RatingId;
                singleDvd.Rating = dvd.Rating;

                dvdList.Add(singleDvd);

            }
            return dvdList;
        }

        public DvdListing GetById(int dvdId)
        {
            var dvd = new DvdListing();

            var dvdFetch = _dvdList.Where(d => d.DvdId == dvdId).FirstOrDefault();

            dvd.DvdId = dvdFetch.DvdId;
            dvd.Title = dvdFetch.Title;
            dvd.Director = dvdFetch.Director;
            dvd.ReleaseYear = dvdFetch.ReleaseYear;
            dvd.RatingId = dvdFetch.RatingId;
            dvd.Notes = dvdFetch.Notes;
            dvd.Rating = dvdFetch.Rating;

            return dvd;
        }

        public List<Dvds> GetByRating(int ratingId)
        {
            var dvdRating = Settings.GetRatingName(ratingId);

            List<Dvds> listing = _dvdList.Where(d => d.RatingId == ratingId).ToList();
            return listing;
        }

        public List<Dvds> GetByTitle(string title)
        {
            List<Dvds> listing = _dvdList.Where(d => d.Title.Contains(title)).ToList();
            return listing;
        }

        public List<Dvds> GetByYear(int year)
        {
            List<Dvds> listing = _dvdList.Where(d => d.ReleaseYear == year).ToList();
            return listing;
        }

        public void Insert(Dvds dvd)
        {
            var newDvd = new Dvds();

            newDvd.DvdId = dvd.DvdId;
            newDvd.Title = dvd.Title;
            newDvd.Director = dvd.Director;
            newDvd.Notes = dvd.Notes;
            newDvd.RatingId = dvd.RatingId;
            newDvd.ReleaseYear = dvd.ReleaseYear;

            if (_dvdList.Any())
            {
                newDvd.DvdId = _dvdList.Max(d => d.DvdId) + 1;
            }
            else
            {
                newDvd.DvdId = 0;
            }
            _dvdList.Add(newDvd);
        }



        public void Update(Dvds dvd)
        {
            _dvdList.RemoveAll(d => d.DvdId == dvd.DvdId);
            var updatedDvd = new Dvds();

            updatedDvd.Title = dvd.Title;
            updatedDvd.Director = dvd.Director;
            updatedDvd.Notes = dvd.Notes;
            updatedDvd.RatingId = dvd.RatingId;
            updatedDvd.ReleaseYear = dvd.ReleaseYear;
            updatedDvd.Rating = dvd.Rating;

            _dvdList.Add(updatedDvd);
        }
    }
}
