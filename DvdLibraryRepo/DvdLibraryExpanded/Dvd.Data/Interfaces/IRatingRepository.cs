﻿using DvdLibrary.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dvd.Data.Interfaces
{
    public interface IRatingRepository
    {
        List<Ratings> GetAll();
    }
}
