﻿using DvdLibrary.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dvd.Data.Interfaces
{
    public interface IDvdRepository
    {
        DvdListing GetById(int dvdId);
        void Insert(Dvds dvd);
        void Update(Dvds dvd);
        void Delete(int dvdId);
        IEnumerable<DvdListing> GetAllDvds();
        List<Dvds> GetByTitle(string title);
        List<Dvds> GetByDirector(string director);
        List<Dvds> GetByRating(int ratingId);
        List<Dvds> GetByYear(int year);
    }
}
