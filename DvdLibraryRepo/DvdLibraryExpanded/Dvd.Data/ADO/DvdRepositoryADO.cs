﻿using Dvd.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.Models.Tables;
using System.Data.SqlClient;
using System.Data;

namespace Dvd.Data.ADO
{
    public class DvdRepositoryADO : IDvdRepository
    {
        public void Delete(int dvdId)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdDelete", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DvdId", dvdId);

                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public IEnumerable<DvdListing> GetAllDvds()
        {  
            List<DvdListing> dvds = new List<DvdListing>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdsSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using(SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DvdListing row = new DvdListing();
                       
                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString(); 
                        row.RatingId= (int)dr["RatingId"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Director"] != DBNull.Value)
                            row.Director = dr["Director"].ToString();

                        if (dr["ReleaseYear"] != DBNull.Value)
                            row.ReleaseYear = (int)dr["ReleaseYear"];

                        if (dr["Notes"] != DBNull.Value)
                            row.Notes = dr["Notes"].ToString();

                        dvds.Add(row);
                    }
                } 
            }
            return dvds;
        }

        public List<Dvds> GetByDirector(string director)
        {
            List<Dvds> dvds = new List<Dvds>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdDirectorSearch", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Director", director);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Dvds row = new Dvds();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.RatingId = (int)dr["RatingId"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Director"] != DBNull.Value)
                            row.Director = dr["Director"].ToString();

                        if (dr["ReleaseYear"] != DBNull.Value)
                            row.ReleaseYear = (int)dr["ReleaseYear"];

                        if (dr["Notes"] != DBNull.Value)
                            row.Notes = dr["Notes"].ToString();

                        dvds.Add(row);
                    }
                }
            }
            return dvds;
        }

        public DvdListing GetById(int dvdId)
        {
            DvdListing dvd = null;

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdSelect", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DvdId", dvdId);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dvd = new DvdListing();
                        dvd.DvdId = (int)dr["DvdId"];
                        dvd.Title = dr["Title"].ToString();
                        dvd.RatingId = (int)dr["RatingId"];
                        dvd.Rating = dr["Rating"].ToString();

                        if (dr["Director"] != DBNull.Value)
                            dvd.Director = dr["Director"].ToString();

                        if (dr["Notes"] != DBNull.Value)
                            dvd.Notes = dr["Notes"].ToString();

                        if (dr["ReleaseYear"] != DBNull.Value)
                            dvd.ReleaseYear = (int)dr["ReleaseYear"];


                    }
                }
            }

            return dvd;
        }

        public List<Dvds> GetByRating(int ratingId)
        {
            List<Dvds> dvds = new List<Dvds>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdRatingSearch", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@RatingId", ratingId);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Dvds row = new Dvds();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.RatingId = (int)dr["RatingId"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Director"] != DBNull.Value)
                            row.Director = dr["Director"].ToString();

                        if (dr["ReleaseYear"] != DBNull.Value)
                            row.ReleaseYear = (int)dr["ReleaseYear"];

                        if (dr["Notes"] != DBNull.Value)
                            row.Notes = dr["Notes"].ToString();

                        dvds.Add(row);
                    }
                }
            }
            return dvds;
        }

        public List<Dvds> GetByTitle(string title)
        {
            List<Dvds> dvds = new List<Dvds>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdTitleSearch", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Title", title);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Dvds row = new Dvds();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.RatingId = (int)dr["RatingId"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Director"] != DBNull.Value)
                            row.Director = dr["Director"].ToString();

                        if (dr["ReleaseYear"] != DBNull.Value)
                            row.ReleaseYear = (int)dr["ReleaseYear"];

                        if (dr["Notes"] != DBNull.Value)
                            row.Notes = dr["Notes"].ToString();

                        dvds.Add(row);
                    }
                }
            }
            return dvds;
        }

        public List<Dvds> GetByYear(int year)
        {
            List<Dvds> dvds = new List<Dvds>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdYearSearch", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Year", year);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Dvds row = new Dvds();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.RatingId = (int)dr["RatingId"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Director"] != DBNull.Value)
                            row.Director = dr["Director"].ToString();

                        if (dr["ReleaseYear"] != DBNull.Value)
                            row.ReleaseYear = (int)dr["ReleaseYear"];

                        if (dr["Notes"] != DBNull.Value)
                            row.Notes = dr["Notes"].ToString();

                        dvds.Add(row);
                    }
                }
            }
            return dvds;
        }

        public void Insert(Dvds dvd)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdInsert", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter("@DvdId", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("@Title", dvd.Title);
                cmd.Parameters.AddWithValue("@RatingId", dvd.RatingId);
                cmd.Parameters.AddWithValue("@Director", dvd.Director);
                cmd.Parameters.AddWithValue("@ReleaseYear", dvd.ReleaseYear);
                cmd.Parameters.AddWithValue("@Notes", dvd.Notes);

                cn.Open();

                cmd.ExecuteNonQuery();

                dvd.DvdId = (int)param.Value;

            }
        }
 
        public void Update(Dvds dvd)
        {

            var dvdRating = Settings.GetRatingId(dvd.Rating);
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdUpdate", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DvdId", dvd.DvdId);
                cmd.Parameters.AddWithValue("@Title", dvd.Title);
                cmd.Parameters.AddWithValue("@RatingId", dvdRating);
                cmd.Parameters.AddWithValue("@Director", dvd.Director);
                cmd.Parameters.AddWithValue("@ReleaseYear", dvd.ReleaseYear);
                cmd.Parameters.AddWithValue("@Notes", dvd.Notes);
                cmd.Parameters.AddWithValue("@Rating", dvd.Rating);

                cn.Open();

                cmd.ExecuteNonQuery();

            }
        }
    }
}
