﻿using Dvd.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.Models.Tables;
using System.Data.SqlClient;
using System.Data;

namespace Dvd.Data.ADO
{
    public class RatingRepositoryADO : IRatingRepository
    {
        public List<Ratings> GetAll()
        {
            List<Ratings> ratings = new List<Ratings>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("RatingsSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using(SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Ratings currentRow = new Ratings();
                        currentRow.RatingId = (int)dr["RatingId"];
                        currentRow.Rating = dr["Rating"].ToString();

                        ratings.Add(currentRow);
                    }

                }
            }
            return ratings;
        }
    }
}
