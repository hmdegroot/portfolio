﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dvd.Data
{
    public class Settings
    {
        private static string _connectionString;
        private static string _repositoryType;


        public static string GetConnectionString()
        {
            if (string.IsNullOrEmpty(_connectionString))
                _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            return _connectionString;
        }

        public static string GetRepositoryType()
        {
            if (string.IsNullOrEmpty(_repositoryType))
            {
                _repositoryType = ConfigurationManager.AppSettings["RepositoryType"].ToString();
            }
            return _repositoryType;
        }

        public static int GetRatingId(string rating)
        {
            var ratingId = 0;
            if(rating == "G" || rating == "g")
            {
                ratingId = 1;
            }
            if (rating == "PG" || rating == "pg")
            {
                ratingId = 2;
            }
            if (rating == "PG-13" || rating == "pg-13" || rating == "pg13")
            {
                ratingId = 3;
            }
            if (rating == "R" || rating == "r")
            {
                ratingId = 4;
            }
            if (rating == "NR" || rating == "nr" || rating == "not rated" || rating == "notrated")
            {
                ratingId = 5;
            }
            
            return ratingId;
        }

        public static string GetRatingName(int ratingId)
        {
            string ratingName = "";

            if (ratingId ==1)
            {
                ratingName = "G";
            }
            if (ratingId == 2)
            {
                ratingName = "PG";
            }
            if (ratingId == 3)
            {
                ratingName = "PG-13";
            }
            if (ratingId == 4)
            {
                ratingName = "R";
            }
            if (ratingId == 5)
            {
                ratingName = "NR";
            }

            return ratingName;
        }
    }
}
