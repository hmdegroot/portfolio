﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.Models.Tables
{
    public class Ratings
    {
        [Key]
        public int RatingId { get; set; }
        public string Rating { get; set; }
    }
}
