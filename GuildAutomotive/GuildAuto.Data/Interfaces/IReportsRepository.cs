﻿using GuildAuto.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Interfaces
{
    public interface IReportsRepository
    {
        IEnumerable<SalesReport> SalesSearch(SalesSearchParameters parameters);
        IEnumerable<SalesReport> GetAllSales();
        IEnumerable<InventoryReport> GetNewInventory();
        IEnumerable<InventoryReport> GetUsedInvetory();
    }
}
