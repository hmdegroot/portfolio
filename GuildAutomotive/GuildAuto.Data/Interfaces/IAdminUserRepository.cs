﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Interfaces
{
    public interface IAdminUserRepository
    {
        List<AdminUser> GetAllUserDetails();
        void AddUser (AdminUser user);
        void EditUser(AdminUser user);
    }
}
