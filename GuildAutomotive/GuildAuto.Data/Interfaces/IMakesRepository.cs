﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Interfaces
{
    public interface IMakesRepository
    {
        List<Makes> GetAllMakes();
        List<MakeDetails> GetMakeDetails();
        void Insert(Makes make);
    }
}
