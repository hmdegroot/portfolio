﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Interfaces
{
    public interface ICarModelsRepository
    {
        List<CarModels> GetAllCarModels();
        List<CarModels> GetModel(int makeId);
        List<CarModelDetails> GetModelDetails();
        void Insert(CarModels carModel);
    }
}
