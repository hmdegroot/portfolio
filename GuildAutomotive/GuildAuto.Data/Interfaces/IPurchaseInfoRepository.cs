﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Interfaces
{
    public interface IPurchaseInfoRepository
    {
        IEnumerable<PurchaseInfo> GetAllPurchaseInfo();
        void InsertPurchase(PurchaseInfo purchaseInfo);
        void UpdateSold(SoldVehicle soldVehicle);
    }
}
