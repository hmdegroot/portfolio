﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Interfaces
{
    public interface IListingsRepository
    {
        IEnumerable<FeaturedVehicles> GetFeaturedVehicles();
        IEnumerable<ListingShortItem> GetNewVehicles();
        IEnumerable<ListingShortItem> GetUsedVehicles();
        ListingItem GetVehicleById(int vehicleId);
        IEnumerable<ListingShortItem> GetAllVehicles();
        void Insert(Vehicle vehicle);
        void Update(Vehicle vehicle);
        IEnumerable<ListingShortItem> UsedSearch(ListingSearchParameters parameters);
        IEnumerable<ListingShortItem> NewSearch(ListingSearchParameters parameters);
        IEnumerable<ListingShortItem> AllSearch(ListingSearchParameters parameters);

    }
}
