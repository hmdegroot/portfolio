﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class MakesRepositoryDapper : IMakesRepository
    {
        public List<Makes> GetAllMakes()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<Makes>("GetAllMakes", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public List<MakeDetails> GetMakeDetails()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<MakeDetails>("GetMakeDetails", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public void Insert(Makes make)
        {
            using(var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@MakeId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@Make", make.Make);
                parameters.Add("@DateAdded", make.DateAdded);
                parameters.Add("@Id", make.Id);

                cn.Execute("MakeInsert", parameters, commandType: CommandType.StoredProcedure);

                make.MakeId = parameters.Get<int>("@MakeId");
            }
        }
    }
}
