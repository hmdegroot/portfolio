﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class InteriorColorRepositoryDapper : IInteriorColorRepository
    {
        public List<InteriorColors> GetAllInteriorColors()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<InteriorColors>("InteriorColorsSelectAll", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
