﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class SpecialsRepositoryDapper : ISpecialsRepository
    {
        public void Delete(int specialId)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SpecialId", specialId);

                cn.Execute("SpecialDelete", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public List<Specials> GetAllSpecials()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<Specials>("SpecialsSelectAll", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public void Insert(Specials special)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@SpecialId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@SpecialDescription", special.SpecialDescription);
                parameters.Add("@Title", special.Title);

                cn.Execute("SpecialInsert", parameters, commandType: CommandType.StoredProcedure);

                special.SpecialId = parameters.Get<int>("@SpecialId");

            }
        }
    }
}
