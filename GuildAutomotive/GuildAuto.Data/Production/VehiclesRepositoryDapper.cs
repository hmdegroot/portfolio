﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class VehiclesRepositoryDapper : IVehiclesRepository
    {
        public void Delete(int id)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@VehicleId", id);

                cn.Execute("VehicleDelete", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Vehicle> GetAllVehicles()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<Vehicle>("VehiclesSelectAll", commandType: CommandType.StoredProcedure);
            }
        }
    }
}
