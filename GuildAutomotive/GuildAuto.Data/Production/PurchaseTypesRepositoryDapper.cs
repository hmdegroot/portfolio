﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class PurchaseTypesRepositoryDapper : IPurchaseTypesRepository
    {
        public List<PurchaseTypes> GetAllPurchaseTypes()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<PurchaseTypes>("PurchaseTypesSelectAll", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
