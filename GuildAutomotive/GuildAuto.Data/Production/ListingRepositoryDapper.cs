﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class ListingRepositoryDapper : IListingsRepository
    {
        public IEnumerable<ListingShortItem> GetAllVehicles()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<ListingShortItem>("VehiclesAllUnsold", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<FeaturedVehicles> GetFeaturedVehicles()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<FeaturedVehicles>("VehiclesSelectFeatured", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<ListingShortItem> GetNewVehicles()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<ListingShortItem>("VehicleNewList", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<ListingShortItem> GetUsedVehicles()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<ListingShortItem>("VehicleUsedList", commandType: CommandType.StoredProcedure);
            }
        }

        public ListingItem GetVehicleById(int vehicleId)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                ListingItem car = null;
                
                SqlCommand cmd = new SqlCommand("VehicleListing", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@VehicleId", vehicleId);

                cn.Open();

                using(SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        car = new ListingItem();
                        car.Id = dr["Id"].ToString();
                        car.BodyStyle = dr["BodyStyle"].ToString();
                        car.BodyStyleId = (int)dr["BodyStyleId"];
                        car.CarDescription = dr["CarDescription"].ToString();
                        car.CarModel = dr["CarModel"].ToString();
                        car.CarModelId = (int)dr["CarModelId"];
                        car.Condition = dr["Condition"].ToString();
                        car.ConditionId = (int)dr["ConditionId"];
                        car.CreationDate = (DateTime)dr["AddDate"];
                        car.ExteriorColor = dr["ExteriorColor"].ToString();
                        car.ExteriorColorId = (int)dr["ExteriorColorId"];
                        car.Featured = (bool)dr["Featured"];
                        car.InteriorColor = dr["InteriorColor"].ToString();
                        car.InteriorColorId = (int)dr["InteriorColorId"];
                        car.ListingPrice = (decimal)dr["ListingPrice"];
                        car.Make = dr["Make"].ToString();
                        car.MakeId = (int)dr["MakeId"];
                        car.Mileage = (int)dr["Mileage"];
                        car.ModelYear = (int)dr["ModelYear"];
                        car.MSRP = (decimal)dr["MSRP"];
                        car.Sold = (bool)dr["Sold"];
                        car.Transmission = dr["Transmission"].ToString();
                        car.TransmissionId = (int)dr["TransmissionId"];
                        car.VIN = dr["VIN"].ToString();

                        if (dr["ImageFileName"] != DBNull.Value)
                            car.ImageFileName = dr["ImageFileName"].ToString();
                    }
                }
                return car;
            }
            
        }

        public void Insert(Vehicle vehicle)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@VehicleId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@CarModelId", vehicle.CarModelId);
                parameters.Add("@ExteriorColorId", vehicle.ExteriorColorId);
                parameters.Add("@InteriorColorId", vehicle.InteriorColorId);
                parameters.Add("@TransmissionId", vehicle.TransmissionId);
                parameters.Add("@ConditionId", vehicle.ConditionId);
                parameters.Add("@ModelYear", vehicle.ModelYear);
                parameters.Add("@BodyStyleId", vehicle.BodyStyleId);
                parameters.Add("@ListingPrice", vehicle.ListingPrice);
                parameters.Add("@MSRP", vehicle.MSRP);
                parameters.Add("@VIN", vehicle.VIN);
                parameters.Add("@Mileage", vehicle.Mileage);
                parameters.Add("@CarDescription", vehicle.CarDescription);
                parameters.Add("@ImageFileName", vehicle.ImageFileName);
                parameters.Add("@Featured", vehicle.Featured);
                parameters.Add("@AddDate", vehicle.AddDate);
                parameters.Add("@Sold", vehicle.Sold);
                parameters.Add("@Id", vehicle.Id);

                cn.Execute("VehicleInsert", parameters, commandType: CommandType.StoredProcedure);

                vehicle.VehicleId = parameters.Get<int>("@VehicleId");
            }
        }

        public IEnumerable<ListingShortItem> UsedSearch(ListingSearchParameters parameters)
        {
            List<ListingShortItem> listings = new List<ListingShortItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = "SELECT TOP 20 v.VehicleId, CarModel, v.CarModelId, Mileage, Make, ModelYear, ListingPrice, MSRP, ImageFileName, BodyStyle, InteriorColor, ExteriorColor, Transmission, VIN FROM Vehicles v INNER JOIN CarModels c ON c.CarModelId = v.CarModelId INNER JOIN Makes m ON m.MakeId = c.MakeId INNER JOIN BodyStyles b ON b.BodyStyleId = v.BodyStyleId INNER JOIN Transmissions t ON t.TransmissionId = v.TransmissionId INNER JOIN InteriorColors i ON i.InteriorColorId = v.InteriorColorId INNER JOIN ExteriorColors e ON e.ExteriorColorId = v.ExteriorColorId WHERE v.ConditionId = 2 AND v.Sold = 0 AND 1=1";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (parameters.MinPrice.HasValue)
                {
                    query += $"AND ListingPrice >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice.Value);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += $"AND ListingPrice <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice.Value);
                }
                if(parameters.MinYear.HasValue)
                {
                    query += $"AND ModelYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear.Value);
                }
                if (parameters.MaxYear.HasValue)
                {
                    query += $"AND ModelYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear.Value);
                }
                if (!string.IsNullOrEmpty(parameters.SearchBox))
                {
                    query += $"AND (Make LIKE @SearchBox OR CarModel LIKE @SearchBox)";
                    cmd.Parameters.AddWithValue("@SearchBox", parameters.SearchBox + '%');
                }

                query += "ORDER BY MSRP DESC";

                cmd.CommandText = query;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ListingShortItem row = new ListingShortItem();

                        row.VehicleId = (int)dr["VehicleId"];
                        row.CarModel = dr["CarModel"].ToString();
                        row.CarModelId = (int)dr["CarModelId"];
                        row.Make = dr["Make"].ToString();
                        row.ModelYear = (int)dr["ModelYear"];
                        row.ListingPrice = (decimal)dr["ListingPrice"];
                        row.Mileage = (int)dr["Mileage"];
                        row.MSRP = (decimal)dr["MSRP"];
                        row.ImageFileName = dr["ImageFileName"].ToString();
                        row.BodyStyle = dr["BodyStyle"].ToString();
                        row.InteriorColor = dr["InteriorColor"].ToString();
                        row.ExteriorColor = dr["ExteriorColor"].ToString();
                        row.Transmission = dr["Transmission"].ToString();
                        row.VIN = dr["Vin"].ToString();

                        listings.Add(row);
                    }
                }
            }
            return listings;
        }

        public void Update(Vehicle vehicle)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@VehicleId", vehicle.VehicleId);
                parameters.Add("@CarModelId", vehicle.CarModelId);
                parameters.Add("@ExteriorColorId", vehicle.ExteriorColorId);
                parameters.Add("@InteriorColorId", vehicle.InteriorColorId);
                parameters.Add("@TransmissionId", vehicle.TransmissionId);
                parameters.Add("@ConditionId", vehicle.ConditionId);
                parameters.Add("@ModelYear", vehicle.ModelYear);
                parameters.Add("@BodyStyleId", vehicle.BodyStyleId);
                parameters.Add("@ListingPrice", vehicle.ListingPrice);
                parameters.Add("@MSRP", vehicle.MSRP);
                parameters.Add("@VIN", vehicle.VIN);
                parameters.Add("@Mileage", vehicle.Mileage);
                parameters.Add("@CarDescription", vehicle.CarDescription);
                parameters.Add("@ImageFileName", vehicle.ImageFileName);
                parameters.Add("@Featured", vehicle.Featured);

                cn.Execute("VehicleUpdate", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<ListingShortItem> NewSearch(ListingSearchParameters parameters)
        {
            List<ListingShortItem> listings = new List<ListingShortItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = "SELECT TOP 20 v.VehicleId, CarModel, v.CarModelId, Mileage, Make, ModelYear, ListingPrice, MSRP, ImageFileName, BodyStyle, InteriorColor, ExteriorColor, Transmission, VIN FROM Vehicles v INNER JOIN CarModels c ON c.CarModelId = v.CarModelId INNER JOIN Makes m ON m.MakeId = c.MakeId INNER JOIN BodyStyles b ON b.BodyStyleId = v.BodyStyleId INNER JOIN Transmissions t ON t.TransmissionId = v.TransmissionId INNER JOIN InteriorColors i ON i.InteriorColorId = v.InteriorColorId INNER JOIN ExteriorColors e ON e.ExteriorColorId = v.ExteriorColorId WHERE v.ConditionId = 1 AND v.Sold = 0 AND 1=1";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (parameters.MinPrice.HasValue)
                {
                    query += $"AND ListingPrice >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice.Value);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += $"AND ListingPrice <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice.Value);
                }
                if (parameters.MinYear.HasValue)
                {
                    query += $"AND ModelYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear.Value);
                }
                if (parameters.MaxYear.HasValue)
                {
                    query += $"AND ModelYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear.Value);
                }
                if (!string.IsNullOrEmpty(parameters.SearchBox))
                {
                    query += $"AND (Make LIKE @SearchBox OR CarModel LIKE @SearchBox)";
                    cmd.Parameters.AddWithValue("@SearchBox", parameters.SearchBox + '%');
                }

                query += "ORDER BY MSRP DESC";

                cmd.CommandText = query;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ListingShortItem row = new ListingShortItem();

                        row.VehicleId = (int)dr["VehicleId"];
                        row.CarModel = dr["CarModel"].ToString();
                        row.CarModelId = (int)dr["CarModelId"];
                        row.Make = dr["Make"].ToString();
                        row.ModelYear = (int)dr["ModelYear"];
                        row.ListingPrice = (decimal)dr["ListingPrice"];
                        row.Mileage = (int)dr["Mileage"];
                        row.MSRP = (decimal)dr["MSRP"];
                        row.ImageFileName = dr["ImageFileName"].ToString();
                        row.BodyStyle = dr["BodyStyle"].ToString();
                        row.InteriorColor = dr["InteriorColor"].ToString();
                        row.ExteriorColor = dr["ExteriorColor"].ToString();
                        row.Transmission = dr["Transmission"].ToString();
                        row.VIN = dr["Vin"].ToString();

                        listings.Add(row);
                    }
                }
            }
            return listings;
        }

        public IEnumerable<ListingShortItem> AllSearch(ListingSearchParameters parameters)
        {
            List<ListingShortItem> listings = new List<ListingShortItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = "SELECT TOP 20 v.VehicleId, CarModel, v.CarModelId, Mileage, Make, ModelYear, ListingPrice, MSRP, ImageFileName, BodyStyle, InteriorColor, ExteriorColor, Transmission, VIN FROM Vehicles v INNER JOIN CarModels c ON c.CarModelId = v.CarModelId INNER JOIN Makes m ON m.MakeId = c.MakeId INNER JOIN BodyStyles b ON b.BodyStyleId = v.BodyStyleId INNER JOIN Transmissions t ON t.TransmissionId = v.TransmissionId INNER JOIN InteriorColors i ON i.InteriorColorId = v.InteriorColorId INNER JOIN ExteriorColors e ON e.ExteriorColorId = v.ExteriorColorId WHERE v.Sold = 0 AND 1=1";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (parameters.MinPrice.HasValue)
                {
                    query += $"AND ListingPrice >= @MinPrice ";
                    cmd.Parameters.AddWithValue("@MinPrice", parameters.MinPrice.Value);
                }

                if (parameters.MaxPrice.HasValue)
                {
                    query += $"AND ListingPrice <= @MaxPrice ";
                    cmd.Parameters.AddWithValue("@MaxPrice", parameters.MaxPrice.Value);
                }
                if (parameters.MinYear.HasValue)
                {
                    query += $"AND ModelYear >= @MinYear ";
                    cmd.Parameters.AddWithValue("@MinYear", parameters.MinYear.Value);
                }
                if (parameters.MaxYear.HasValue)
                {
                    query += $"AND ModelYear <= @MaxYear ";
                    cmd.Parameters.AddWithValue("@MaxYear", parameters.MaxYear.Value);
                }
                if (!string.IsNullOrEmpty(parameters.SearchBox))
                {
                    query += $"AND (Make LIKE @SearchBox OR CarModel LIKE @SearchBox)";
                    cmd.Parameters.AddWithValue("@SearchBox", parameters.SearchBox + '%');
                }

                query += "ORDER BY MSRP DESC";

                cmd.CommandText = query;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ListingShortItem row = new ListingShortItem();

                        row.VehicleId = (int)dr["VehicleId"];
                        row.CarModel = dr["CarModel"].ToString();
                        row.CarModelId = (int)dr["CarModelId"];
                        row.Make = dr["Make"].ToString();
                        row.ModelYear = (int)dr["ModelYear"];
                        row.ListingPrice = (decimal)dr["ListingPrice"];
                        row.Mileage = (int)dr["Mileage"];
                        row.MSRP = (decimal)dr["MSRP"];
                        row.ImageFileName = dr["ImageFileName"].ToString();
                        row.BodyStyle = dr["BodyStyle"].ToString();
                        row.InteriorColor = dr["InteriorColor"].ToString();
                        row.ExteriorColor = dr["ExteriorColor"].ToString();
                        row.Transmission = dr["Transmission"].ToString();
                        row.VIN = dr["Vin"].ToString();

                        listings.Add(row);
                    }
                }
            }
            return listings;
        }
    }
}
