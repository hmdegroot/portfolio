﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;

namespace GuildAuto.Data.Production
{
    public class CarModelsRepositoryDapper : ICarModelsRepository
    {
        public List<CarModels> GetAllCarModels()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<CarModels>("GetAllCarModels", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public List<CarModels> GetModel(int makeId)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@MakeId", makeId);

                return cn.Query<CarModels>("GetCarModelForMake", parameters, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public List<CarModelDetails> GetModelDetails()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<CarModelDetails>("GetModelDetails", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public void Insert(CarModels carModel)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@CarModelId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@MakeId", carModel.MakeId);
                parameters.Add("@CarModel", carModel.CarModel);
                parameters.Add("@DateAdded", carModel.DateAdded);
                parameters.Add("@Id", carModel.Id);

                cn.Execute("ModelInsert", parameters, commandType: CommandType.StoredProcedure);

                carModel.CarModelId = parameters.Get<int>("@CarModelId");
            }
        }
    }
}
