﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace GuildAuto.Data.Production
{
    public class ContactRepositoryDapper : IContactsRepository
    {
        public void Insert(Contacts contact)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ContactId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@ContactName", contact.ContactName);
                parameters.Add("@Email", contact.Email);
                parameters.Add("@Phone", contact.Phone);
                parameters.Add("@ContactMessage", contact.ContactMessage);

                cn.Execute("ContactInsert", parameters, commandType: CommandType.StoredProcedure);

                contact.ContactId = parameters.Get<int>("@ContactId");
            }
        }
    }
}
