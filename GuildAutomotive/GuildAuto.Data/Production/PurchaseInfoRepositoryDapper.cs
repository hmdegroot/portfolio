﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using GuildAuto.Models.Queries;

namespace GuildAuto.Data.Production
{
    public class PurchaseInfoRepositoryDapper : IPurchaseInfoRepository
    {
        public IEnumerable<PurchaseInfo> GetAllPurchaseInfo()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<PurchaseInfo>("PurchaseInfoSelectAll", commandType: CommandType.StoredProcedure);
            }
        }

        public void InsertPurchase(PurchaseInfo purchaseInfo)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@PurchaseInfoId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@BuyerName", purchaseInfo.BuyerName);
                parameters.Add("@Phone", purchaseInfo.Phone);
                parameters.Add("@Email", purchaseInfo.Email);
                parameters.Add("@Street1", purchaseInfo.Street1);
                parameters.Add("@Street2", purchaseInfo.Street2);
                parameters.Add("@StateId", purchaseInfo.StateId);
                parameters.Add("@ZipCode", purchaseInfo.ZipCode);
                parameters.Add("@SalePrice", purchaseInfo.SalePrice);
                parameters.Add("@PurchaseTypeId", purchaseInfo.PurchaseTypeId);
                parameters.Add("@PurchaseDate", purchaseInfo.PurchaseDate);
                parameters.Add("@VehicleId", purchaseInfo.VehicleId);
                parameters.Add("@Id", purchaseInfo.Id);

                cn.Execute("PurchaseInsert", parameters, commandType: CommandType.StoredProcedure);

                purchaseInfo.PurchaseInfoId = parameters.Get<int>("@PurchaseInfoId");
            }
        }

        public void UpdateSold(SoldVehicle soldVehicle)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@VehicleId", soldVehicle.VehicleId);
                parameters.Add("@Sold", soldVehicle.Sold);
                parameters.Add("@ImageFileName", soldVehicle.ImageFileName);
                parameters.Add("@Featured", soldVehicle.Featured);

                cn.Execute("SoldUpdate", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
