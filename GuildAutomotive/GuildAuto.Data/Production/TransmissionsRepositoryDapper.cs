﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class TransmissionsRepositoryDapper : ITransmissionsRepository
    {
        public List<Transmissions> GetAllTransmissions()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<Transmissions>("TransmissionsSelectAll", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
