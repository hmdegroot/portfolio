﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using GuildAuto.Models.Queries;

namespace GuildAuto.Data.Production
{
    public class AdminUserRepositoryDapper : IAdminUserRepository
    {
        public void AddUser(AdminUser user)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@Id", dbType: DbType.String, direction: ParameterDirection.Output);
                parameters.Add("@FirstName", user.FirstName);
                parameters.Add("@LastName", user.LastName);
                parameters.Add("@RoleId", user.Role);

                cn.Execute("UserInsert", parameters, commandType: CommandType.StoredProcedure);

                user.Id = parameters.Get<string>("@Id");
            }
        }
       
        public void EditUser(AdminUser user)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@Id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@FirstName", user.FirstName);
                parameters.Add("@LastName", user.LastName);
                parameters.Add("@RoleId", user.Role);

                cn.Execute("UserInsert", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public List<AdminUser> GetAllUserDetails()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<AdminUser>("GetAllAdminUsers", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
