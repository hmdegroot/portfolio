﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace GuildAuto.Data.Production
{
    public class RolesRepositoryDapper : IRolesRepository
    {
        public List<Roles> GetAllRoles()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<Roles>("RolesSelectAll", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
