﻿using Dapper;
using GuildAuto.Data.Interfaces;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Production
{
    public class ConditionsRepositoryDapper : IConditionsRepository
    {
        public List<Conditions> GetAllConditions()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<Conditions>("ConditionsSelectAll", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
