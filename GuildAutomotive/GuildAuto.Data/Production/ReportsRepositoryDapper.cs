﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;
using System.Data.SqlClient;
using System.Data;
using Dapper;

namespace GuildAuto.Data.Production
{
    public class ReportsRepositoryDapper : IReportsRepository
    {
        public IEnumerable<SalesReport> GetAllSales()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<SalesReport>("SalesReport", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<InventoryReport> GetNewInventory()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<InventoryReport>("NewInventoryReport", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<InventoryReport> GetUsedInvetory()
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                return cn.Query<InventoryReport>("UsedInventoryReport", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<SalesReport> SalesSearch(SalesSearchParameters parameters)
        {
            List<SalesReport> report = new List<SalesReport>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = "Select FirstName + ' ' + LastName AS [User], SUM(p.SalePrice) AS TotalSales, COUNT(PurchaseInfoId) AS NumberOfSales FROM AspNetUsers a INNER JOIN PurchaseInfo p ON a.Id = p.Id ";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                
                if (parameters.FromDate.HasValue)
                {
                    query += $"WHERE p.PurchaseDate > @FromDate ";
                    cmd.Parameters.AddWithValue("@FromDate", parameters.FromDate);

                    if (parameters.ToDate.HasValue)
                    {
                        query += $"AND p.PurchaseDate <= @ToDate ";
                        cmd.Parameters.AddWithValue("@ToDate", parameters.ToDate);
                    }

                    if (!string.IsNullOrEmpty(parameters.Id))
                    {
                        query += $"AND a.Id = @Id ";
                        cmd.Parameters.AddWithValue("@Id", parameters.Id);
                    }
                }
                
                else if (parameters.ToDate.HasValue)
                {
                    query += $"WHERE p.PurchaseDate <= @ToDate ";
                    cmd.Parameters.AddWithValue("@ToDate", parameters.ToDate);

                    if (!string.IsNullOrEmpty(parameters.Id))
                    {
                        query += $"AND a.Id = @Id ";
                        cmd.Parameters.AddWithValue("@Id", parameters.Id);
                    }
                }

                else if (!string.IsNullOrEmpty(parameters.Id))
                {
                    query += $"WHERE a.Id = @Id ";
                    cmd.Parameters.AddWithValue("@Id", parameters.Id);
                }

                query += "GROUP BY FirstName, LastName ORDER BY TotalSales DESC";

                cmd.CommandText = query;
                cn.Open();

                using(SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SalesReport row = new SalesReport();

                        row.User = dr["User"].ToString();
                        row.TotalSales = (decimal)dr["TotalSales"];
                        row.NumberOfSales = (int)dr["NumberOfSales"];

                        report.Add(row);
                    }
                }
                return report;
            }
        }
    }
}
