﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class StatesMockRepo : IStatesRepository
    {
        private static List<States> _states;

        static StatesMockRepo()
        {
            _states = new List<States>()
            {
            
            new States
            {
                StateId = "CA",
                StateName = "California"
            },
            new States
            {
                StateName = "Colorado",
                StateId = "CO"
            },
            new States
            {
                StateId = "IA",
                StateName = "Iowa"
            },
            new States
            {
                StateName = "Nebraska",
                StateId = "NE"
            }
        };
        }
        public List<States> GetAllStates()
        {
            return _states;
        }
    }
}
