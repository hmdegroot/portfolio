﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class ConditionsMockRepo : IConditionsRepository
    {
        private static List<Conditions> _conditions;
        
            static ConditionsMockRepo()
        {

             _conditions = new List<Conditions>() { 
            new Conditions
            {
                ConditionId = 1,
                Condition = "New"
            },
            new Conditions
            {
                ConditionId = 2,
                Condition = "Used"
            }
        };
    }
    public List<Conditions> GetAllConditions()
        {
            return _conditions;
        }
    }
}
