﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class InteriorColorsMockRepo : IInteriorColorRepository
    {
        private static List<InteriorColors> _colors;

        static InteriorColorsMockRepo()
        {
            _colors = new List<InteriorColors>()
            {
            new InteriorColors
            {
                InteriorColorId = 1,
                InteriorColor = "Black"
            },
            new InteriorColors
            {
                InteriorColorId = 2,
                InteriorColor ="Tan"
            },
            new InteriorColors
            {
                InteriorColorId = 3,
                InteriorColor = "Grey"
            }
        };
        }

        public List<InteriorColors> GetAllInteriorColors()
        {
            return _colors;
        }
    }
}
