﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class PurchaseTypesMockRepo : IPurchaseTypesRepository
    {
        private static List<PurchaseTypes> _purchaseTypes;
            
        static PurchaseTypesMockRepo()
        {
            _purchaseTypes = new List<PurchaseTypes>() 
            {
            new PurchaseTypes
            {
                PurchaseType = "Cash",
                PurchaseTypeId = 2
            },
            new PurchaseTypes
            {
                PurchaseTypeId = 1,
                PurchaseType = "Bank Finance"
            },
            new PurchaseTypes
            {
                PurchaseType = "Dealer Finance",
                PurchaseTypeId = 3
            }
        };
        }

        public List<PurchaseTypes> GetAllPurchaseTypes()
        {
            return _purchaseTypes;
        }
    }
}
