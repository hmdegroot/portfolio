﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class AdminUserMockRepo : IAdminUserRepository
    {
        private static List<AdminUser> _users;

        static AdminUserMockRepo()
        {
            _users = new List<AdminUser>()
            {
                new AdminUser
                {
                    Id = "1",
                    FirstName = "Sam",
                    LastName = "Hill",
                    Role = "admin",
                    Email = "Sam@hill.com"
                },
                new AdminUser
                 {
                    Id = "2",
                    FirstName = "Jack",
                    LastName = "Black",
                    Role = "sales",
                    Email = "Jack@Black.com"
                  }
            };
        }
        public void AddUser(AdminUser user)
        {
            user.Id = _users.Max(u => u.Id) + 1;
            _users.Add(user);
        }

        public void EditUser(AdminUser user)
        {
            var exstingUser = _users.FirstOrDefault(c => c.Id == user.Id);
            exstingUser.Id = user.Id;
            exstingUser.FirstName = user.FirstName;
            exstingUser.Role = user.Role;
        }

        public List<AdminUser> GetAllUserDetails()
        {
            return _users;
        }

    }
}
