﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class MakesMockRepo : IMakesRepository
    {
        private static List<Makes> _makes;

        static MakesMockRepo()
        {
            _makes = new List<Makes>()
            {
            new Makes
            {
                Make = "Toyota",
                MakeId = 1,
                DateAdded = new DateTime(2016, 10, 1),
                Id = "00000000-0000-0000-0000-000000000000"
            },
            new Makes
            {
                Make = "Subaru",
                MakeId = 2,
                DateAdded = new DateTime(2013, 7, 26),
                Id = "00000000-0000-0000-0000-000000000000"
            },
            new Makes
            {
                Make = "Volvo",
                MakeId = 3,
                DateAdded = new DateTime(2016, 1, 1),
                Id = "00000000-0000-0000-0000-000000000000"
            },
            new Makes
            {
                Make = "Honda",
                MakeId = 4,
                DateAdded = new DateTime(2016, 10, 31),
                Id = "00000000-0000-0000-0000-000000000000"
            }
        };
        }

        public List<Makes> GetAllMakes()
        {
            return _makes;
        }

        public List<MakeDetails> GetMakeDetails()
        {
            var makes = GetAllMakes();
            var makeDetails = new List<MakeDetails>();

            foreach (var make in makes)
            {
                var details = new MakeDetails();
                details.Id = make.Id;
                details.DateAdded = make.DateAdded;
                details.Email = "";
                details.Make = make.Make;
                details.MakeId = make.MakeId;
                makeDetails.Add(details);
            }
            return makeDetails;
        }

        public void Insert(Makes make)
        {
            make.MakeId = _makes.Max(m => m.MakeId) + 1;
            _makes.Add(make);
        }
    }
}
