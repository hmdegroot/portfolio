﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;


namespace GuildAuto.Data.Mocks
{
    public class PurchaseInfoMockRepo : IPurchaseInfoRepository
    {
        private static List<PurchaseInfo> _purchaseInfo;

        static PurchaseInfoMockRepo()
        {
            _purchaseInfo = new List<PurchaseInfo>()
            {
                new PurchaseInfo
                {
                    BuyerName = "Carlos",
                    City = "Denver",
                    StateId ="CO",
                    Phone = "303-751-1111",
                    Email = "Carlos@carlos.com",
                    Id = "carlosId",
                    PurchaseDate = new DateTime(2017, 1, 20),
                    PurchaseInfoId = 1,
                    PurchaseTypeId = 1,
                    SalePrice = 5000m,
                    Street1 = "2111 Champa St",
                    ZipCode = 80205,
                    VehicleId = 1
                }
            };
        }
        
        public IEnumerable<PurchaseInfo> GetAllPurchaseInfo()
        {
            return _purchaseInfo;
        }

        public void InsertPurchase(PurchaseInfo purchaseInfo)
        {
            purchaseInfo.PurchaseInfoId = _purchaseInfo.Max(m => m.PurchaseInfoId + 1);

            _purchaseInfo.Add(purchaseInfo);
        }

        public void UpdateSold(SoldVehicle soldVehicle)
        {
            var car = new VehiclesMockRepo().GetVehicleById(soldVehicle.VehicleId);
            car.Sold = true;

            var repo = new ListingMockRepo();
            repo.Update(car); 
        }
    }
}
