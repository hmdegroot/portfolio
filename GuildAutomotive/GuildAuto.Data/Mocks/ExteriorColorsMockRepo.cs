﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class ExteriorColorsMockRepo : IExteriorColorsRepository
    {
        private static List<ExteriorColors> _exteriorColors;

        static ExteriorColorsMockRepo()
        {
            _exteriorColors = new List<ExteriorColors>()
            {
            new ExteriorColors
            {
                ExteriorColorId = 1,
                ExteriorColor = "Black"
            },
            new ExteriorColors
            {
                ExteriorColorId = 2,
                ExteriorColor = "Silver"
            },
            new ExteriorColors
            {
                ExteriorColorId = 3,
                ExteriorColor = "Red"
            },
            new ExteriorColors
            {
                ExteriorColorId = 4,
                ExteriorColor = "Blue"
            },
            new ExteriorColors
            {
                ExteriorColorId = 5,
                ExteriorColor = "Green"
            }
        };
        }

        public List<ExteriorColors> GetAllExteriorColors()
        {
            return _exteriorColors;
        }
    }
}
