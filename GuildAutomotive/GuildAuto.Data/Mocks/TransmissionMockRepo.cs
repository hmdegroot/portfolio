﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class TransmissionMockRepo : ITransmissionsRepository
    {
        private static List<Transmissions> _transmissions;

        static TransmissionMockRepo()
        {
            _transmissions = new List<Transmissions>()
            {
            new Transmissions
            {
                Transmission = "Automatic",
                TransmissionId = 1
            },
            new Transmissions
            {
                TransmissionId = 2,
                Transmission = "Manual"
            }
        };
        }
        public List<Transmissions> GetAllTransmissions()
        {
            return _transmissions;
        }
    }
}
