﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class BodyStylesMockRepo : IBodyStylesRepository
    {
        private static List<BodyStyles> _bodyStyles;

        static BodyStylesMockRepo()
        {
            _bodyStyles = new List<BodyStyles>()
            {
                new BodyStyles
                {
                    BodyStyleId = 1,
                    BodyStyle = "SUV"
                 },
                new BodyStyles
                {
                    BodyStyleId = 2,
                    BodyStyle = "Truck"
                },
                new BodyStyles
                {
                    BodyStyleId = 3,
                    BodyStyle = "Car"
                },
                new BodyStyles
                {
                    BodyStyleId = 4,
                    BodyStyle = "Van"
                }
            };
        }


        public List<BodyStyles> GetAllBodyStyles()
        {
            return _bodyStyles;
        }
    }
}
