﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class CarModelMockRepo : ICarModelsRepository
    {
        private static List<CarModels> _carModels; 
        
            static CarModelMockRepo() {

            _carModels = new List<CarModels>() { 
            new CarModels
            {
                CarModelId = 1,
                MakeId = 1, 
                CarModel = "4Runner",
                DateAdded = new DateTime(2017, 01, 20 ),
                Id = "00000000-0000-0000-0000-000000000000"
            },
            new CarModels
            {
                CarModelId = 2, 
                MakeId = 2,
                CarModel = "Forester",
                DateAdded = new DateTime(2016, 10, 31 ),
                Id = "00000000-0000-0000-0000-000000000000"
            },
            new CarModels
            {
                CarModelId = 3,
                MakeId = 3,
                CarModel = "S60",
                DateAdded = new DateTime(2013, 07, 26 ),
                Id = "00000000-0000-0000-0000-000000000000"
            },
              new CarModels
            {
                CarModelId = 4,
                MakeId = 4,
                CarModel = "Civic",
                DateAdded = new DateTime(2016, 6, 6 ),
                Id = "00000000-0000-0000-0000-000000000000"
            },
              new CarModels
            {
                CarModelId = 5,
                MakeId = 2,
                CarModel = "Impreza",
                DateAdded = new DateTime(2017, 1, 1 ),
                Id = "00000000-0000-0000-0000-000000000000"
            }
        };
    }
    public List<CarModels> GetAllCarModels()
        {
            return _carModels;
        }

        public List<CarModels> GetModel(int makeId)
        {
            var cars =_carModels.Where(c => c.MakeId == makeId).ToList();
            
            return cars;
        }

        public List<CarModelDetails> GetModelDetails()
        {
            var models = GetAllCarModels();
            var detailsList = new List<CarModelDetails>();
            var makes = new MakesMockRepo().GetAllMakes();

            foreach(var model in models)
            {
                var modelDetail = new CarModelDetails();

                modelDetail.Id = model.Id;
                modelDetail.CarModel = model.CarModel;
                modelDetail.CarModelId = model.CarModelId;
                modelDetail.DateAdded = model.DateAdded;
                modelDetail.Email = "";
                modelDetail.MakeId = model.MakeId;

                foreach(var make in makes)
                {
                    if(make.MakeId == model.MakeId)
                    {
                        modelDetail.Make = make.Make;
                    }
                }

                detailsList.Add(modelDetail);
            }

            return detailsList;
        }

        public void Insert(CarModels carModel)
        {
            carModel.CarModelId = _carModels.Max(c => c.CarModelId) + 1;
            _carModels.Add(carModel);
        }
    }
}
