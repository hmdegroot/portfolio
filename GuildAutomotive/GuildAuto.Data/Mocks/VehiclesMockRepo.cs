﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class VehiclesMockRepo : IVehiclesRepository
    {
        private static List<Vehicle> _cars;

        static VehiclesMockRepo()
        {
            _cars = new List<Vehicle>()
        {
            new Vehicle
            {
                VehicleId = 1,
                CarModelId = 1,
                ExteriorColorId = 3,
                InteriorColorId = 2,
                TransmissionId = 1,
                ConditionId = 2,
                ModelYear = 2010,
                BodyStyleId = 1,
                ListingPrice = 35000m,
                MSRP = 36250m,
                VIN = "JH4TB2H26CC000000",
                Mileage = 45000,
                CarDescription = "Like new condition, ready to go off road",
                ImageFileName = "20104runner.jpg",
                Featured = true,
                AddDate = new DateTime(2016, 11,30),
                Sold = false,
                Id = "00000000-0000-0000-0000-000000000000"
            },
            new Vehicle
            {
                VehicleId = 2,
                CarModelId = 2,
                ExteriorColorId = 2,
                InteriorColorId = 3,
                TransmissionId = 2,
                ConditionId = 1,
                ModelYear = 2017,
                BodyStyleId = 1,
                ListingPrice = 22400m,
                MSRP = 22595m,
                VIN = "PPPP22223333344DAP",
                Mileage = 12,
                CarDescription = "Fully loaded, BOSE sound system, all leather interior. Brand new ready to haul whatever your adventures require all weather. One of the safest vehicles on the road today. The Forester will not dissapoint.",
                ImageFileName = "subaruForester2017.jpg",
                Featured = true,
                AddDate = new DateTime(2016, 12, 10),
                Sold = false,
                Id = "00000000-0000-0000-0000-000000000000"
            },
               new Vehicle
            {
                VehicleId = 3,
                CarModelId = 3,
                ExteriorColorId = 1,
                InteriorColorId = 3,
                TransmissionId = 2,
                ConditionId = 1,
                ModelYear = 2017,
                BodyStyleId = 4,
                ListingPrice = 32550m,
                MSRP = 33950m,
                VIN = "ASDD1234PPPP5546AA",
                Mileage = 53,
                CarDescription = "Go Fast Now.",
                ImageFileName = "volvos60.jpg",
                Featured = true,
                AddDate = new DateTime(2017, 03, 03),
                Sold = false,
                Id = "00000000-0000-0000-0000-000000000000"
            },
                  new Vehicle
            {
                VehicleId = 4,
                CarModelId = 4,
                ExteriorColorId = 4,
                InteriorColorId = 1,
                TransmissionId = 1,
                ConditionId = 2,
                ModelYear = 2016,
                BodyStyleId = 4,
                ListingPrice = 18600m,
                MSRP = 18640m,
                VIN = "ASDD1234PPPP5WWWWE",
                Mileage = 13974,
                CarDescription = "1 year old = brand new with a discount!",
                ImageFileName = "2016hondacivic.jpg",
                Featured = true,
                AddDate = new DateTime(2017, 01, 01),
                Sold = false,
                Id = "00000000-0000-0000-0000-000000000000"
            }
        };
        }

        public void Delete(int id)
        {
            _cars.RemoveAll(c => c.VehicleId == id);
        }

        public IEnumerable<Vehicle> GetAllVehicles()
        {
            return _cars;
        }

        public void Insert(Vehicle vehicle)
        {
            vehicle.VehicleId = _cars.Max(c => c.VehicleId) + 1;
            _cars.Add(vehicle);
        }

        public Vehicle GetVehicleById(int id)
        {
            var car = new Vehicle();
            car = _cars.FirstOrDefault(v => v.VehicleId == id);
            return car;
        }
    }
}
