﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class SpecialsMockRepo : ISpecialsRepository
    {
        private static List<Specials> _specials;

        static SpecialsMockRepo()
        {
            _specials = new List<Specials>()
            {
            new Specials
            {
                SpecialDescription = "No interest financing for 3 years on new inventory. Does not include 2016 vehicles. Does not include scratch and dent inventory. Must pre-qaulify with extraordinary credit. $2000 down payment required, trade in value not considered. Cosigners need not apply.",
                SpecialId = 1,
                Title = "Financing Special"
            },
            new Specials
            {
                SpecialDescription = "Discount %5 off new inventory for all active military. Must present valid military ID. Not valid with other offers.",
                SpecialId = 2,
                Title = "Military Discount"
            },
            new Specials
            {
                SpecialDescription = "Trade in = Blue book + %10 on all vehicles 2000 and newer!! Must be in working condition and does not apply to push, pull or drag in vehicles. Must be purchasing a vehicle from Guild Cars.",
                SpecialId = 3,
                Title = "Trade In Bonanza!"
            },
            new Specials
            {
                Title = "Test Drive Our New Inventory",
                SpecialId = 4,
                SpecialDescription = "Test drive a new car and get a $25 gift card to Applebees."
            }
            };
        }

        public void Delete(int specialId)
        {
            _specials.RemoveAll(s => s.SpecialId == specialId);
        }

        public List<Specials> GetAllSpecials()
        {
            return _specials;
        }

        public void Insert(Specials special)
        {
            special.SpecialId = _specials.Max(s => s.SpecialId) + 1;
            _specials.Add(special);
        }
    }
}
