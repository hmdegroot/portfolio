﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;

namespace GuildAuto.Data.Mocks
{
    public class ReportsMockRepo : IReportsRepository
    {
        public IEnumerable<SalesReport> GetAllSales()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<InventoryReport> GetNewInventory()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<InventoryReport> GetUsedInvetory()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SalesReport> SalesSearch(SalesSearchParameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}
