﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{

    public class ListingMockRepo : IListingsRepository
    {
        public IEnumerable<ListingShortItem> AllSearch(ListingSearchParameters parameters)
        {
            var listings = GetAllVehicles();

            if (parameters.MaxPrice > 0 && parameters.MaxPrice != null)
            {
                listings = listings.Where(c => c.ListingPrice < parameters.MaxPrice);
            }
            if (parameters.MinPrice > 0 && parameters.MinPrice != null)
            {
                listings = listings.Where(c => c.ListingPrice > parameters.MinPrice);
            }
            if (parameters.MinYear > 0 && parameters.MinYear != null)
            {
                listings = listings.Where(c => c.ModelYear > parameters.MinYear);
            }
            if (parameters.MaxYear > 0 && parameters.MaxYear != null)
            {
                listings = listings.Where(c => c.ListingPrice < parameters.MaxYear);
            }

            return listings;
        }

        public IEnumerable<ListingShortItem> GetAllVehicles()
        {
            var carList = new List<ListingShortItem>();
            var vehicles = new VehiclesMockRepo().GetAllVehicles();
            var bodyStyles = new BodyStylesMockRepo().GetAllBodyStyles();
            var conditions = new ConditionsMockRepo().GetAllConditions();
            var exteriorColors = new ExteriorColorsMockRepo().GetAllExteriorColors();
            var interiorColors = new InteriorColorsMockRepo().GetAllInteriorColors();
            var makes = new MakesMockRepo().GetAllMakes();
            var models = new CarModelMockRepo().GetAllCarModels();
            var transmissions = new TransmissionMockRepo().GetAllTransmissions();

            foreach (var car in vehicles)
            {
                var listItem = new ListingShortItem();

                listItem.BodyStyleId = car.BodyStyleId;
                listItem.BodyStyle = bodyStyles.FirstOrDefault(s => s.BodyStyleId == listItem.BodyStyleId).BodyStyle;
                listItem.CarModelId = car.CarModelId;
                listItem.CarModel = models.FirstOrDefault(m => m.CarModelId == listItem.CarModelId).CarModel;
                listItem.MakeId = models.FirstOrDefault(m => m.CarModelId == listItem.CarModelId).MakeId;
                listItem.Make = makes.FirstOrDefault(m => m.MakeId == listItem.MakeId).Make;
                listItem.ExteriorColorId = car.ExteriorColorId;
                listItem.ExteriorColor = exteriorColors.FirstOrDefault(c => c.ExteriorColorId == listItem.ExteriorColorId).ExteriorColor;
                listItem.InteriorColorId = car.InteriorColorId;
                listItem.InteriorColor = interiorColors.FirstOrDefault(c => c.InteriorColorId == listItem.InteriorColorId).InteriorColor;
                listItem.ImageFileName = car.ImageFileName;
                listItem.ListingPrice = car.ListingPrice;
                listItem.Mileage = car.Mileage;
                listItem.ModelYear = car.ModelYear;
                listItem.MSRP = car.MSRP;
                listItem.TransmissionId = car.TransmissionId;
                listItem.Transmission = transmissions.FirstOrDefault(t => t.TransmissionId == listItem.TransmissionId).Transmission;
                listItem.VehicleId = car.VehicleId;
                listItem.VIN = car.VIN;

                carList.Add(listItem);
            }

            return carList;
        }

        public IEnumerable<FeaturedVehicles> GetFeaturedVehicles()
        {
            var cars = new VehiclesMockRepo().GetAllVehicles();
            cars = cars.Where(c => c.Featured == true);

            var makes = new MakesMockRepo().GetAllMakes();
            var models = new CarModelMockRepo().GetAllCarModels();


            var featured = new List<FeaturedVehicles>();

            foreach (var car in cars)
            {
                var featuredToAdd = new FeaturedVehicles();

                featuredToAdd.CarModelId = car.CarModelId;

                foreach (var model in models)
                {
                    if (car.CarModelId == model.CarModelId)
                    {
                        featuredToAdd.CarModel = model.CarModel;
                        featuredToAdd.MakeId = model.MakeId;
                    }
                }

                foreach (var make in makes)
                {
                    if (featuredToAdd.MakeId == make.MakeId)
                    {
                        featuredToAdd.Make = make.Make;
                    }
                }

                featuredToAdd.ImageFileName = car.ImageFileName;
                featuredToAdd.ListingPrice = car.ListingPrice;
                featuredToAdd.ModelYear = car.ModelYear;
                featuredToAdd.VehicleId = car.VehicleId;
                featured.Add(featuredToAdd);
            }
            return featured;
        }

        public IEnumerable<ListingShortItem> GetNewVehicles()
        {
            var vehicles = new VehiclesMockRepo().GetAllVehicles().Where(c => c.ConditionId == 1);
            var carList = new List<ListingShortItem>();
            var bodyStyles = new BodyStylesMockRepo().GetAllBodyStyles();
            var conditions = new ConditionsMockRepo().GetAllConditions();
            var exteriorColors = new ExteriorColorsMockRepo().GetAllExteriorColors();
            var interiorColors = new InteriorColorsMockRepo().GetAllInteriorColors();
            var makes = new MakesMockRepo().GetAllMakes();
            var models = new CarModelMockRepo().GetAllCarModels();
            var transmissions = new TransmissionMockRepo().GetAllTransmissions();

            foreach (var car in vehicles)
            {
                var listItem = new ListingShortItem();

                listItem.BodyStyleId = car.BodyStyleId;
                listItem.BodyStyle = bodyStyles.FirstOrDefault(s => s.BodyStyleId == listItem.BodyStyleId).BodyStyle;
                listItem.CarModelId = car.CarModelId;
                listItem.CarModel = models.FirstOrDefault(m => m.CarModelId == listItem.CarModelId).CarModel;
                listItem.MakeId = models.FirstOrDefault(m => m.CarModelId == listItem.CarModelId).MakeId;
                listItem.Make = makes.FirstOrDefault(m => m.MakeId == listItem.MakeId).Make;
                listItem.ExteriorColorId = car.ExteriorColorId;
                listItem.ExteriorColor = exteriorColors.FirstOrDefault(c => c.ExteriorColorId == listItem.ExteriorColorId).ExteriorColor;
                listItem.InteriorColorId = car.InteriorColorId;
                listItem.InteriorColor = interiorColors.FirstOrDefault(c => c.InteriorColorId == listItem.InteriorColorId).InteriorColor;
                listItem.ImageFileName = car.ImageFileName;
                listItem.ListingPrice = car.ListingPrice;
                listItem.Mileage = car.Mileage;
                listItem.ModelYear = car.ModelYear;
                listItem.MSRP = car.MSRP;
                listItem.TransmissionId = car.TransmissionId;
                listItem.Transmission = transmissions.FirstOrDefault(t => t.TransmissionId == listItem.TransmissionId).Transmission;
                listItem.VehicleId = car.VehicleId;
                listItem.VIN = car.VIN;

                carList.Add(listItem);
            }

            return carList;

        }

        public IEnumerable<ListingShortItem> GetUsedVehicles()
        {
            var vehicles = new VehiclesMockRepo().GetAllVehicles().Where(c => c.ConditionId == 2);

            var carList = new List<ListingShortItem>();
            var bodyStyles = new BodyStylesMockRepo().GetAllBodyStyles();
            var conditions = new ConditionsMockRepo().GetAllConditions();
            var exteriorColors = new ExteriorColorsMockRepo().GetAllExteriorColors();
            var interiorColors = new InteriorColorsMockRepo().GetAllInteriorColors();
            var makes = new MakesMockRepo().GetAllMakes();
            var models = new CarModelMockRepo().GetAllCarModels();
            var transmissions = new TransmissionMockRepo().GetAllTransmissions();

            foreach (var car in vehicles)
            {
                var listItem = new ListingShortItem();

                listItem.BodyStyleId = car.BodyStyleId;
                listItem.BodyStyle = bodyStyles.FirstOrDefault(s => s.BodyStyleId == listItem.BodyStyleId).BodyStyle;
                listItem.CarModelId = car.CarModelId;
                listItem.CarModel = models.FirstOrDefault(m => m.CarModelId == listItem.CarModelId).CarModel;
                listItem.MakeId = models.FirstOrDefault(m => m.CarModelId == listItem.CarModelId).MakeId;
                listItem.Make = makes.FirstOrDefault(m => m.MakeId == listItem.MakeId).Make;
                listItem.ExteriorColorId = car.ExteriorColorId;
                listItem.ExteriorColor = exteriorColors.FirstOrDefault(c => c.ExteriorColorId == listItem.ExteriorColorId).ExteriorColor;
                listItem.InteriorColorId = car.InteriorColorId;
                listItem.InteriorColor = interiorColors.FirstOrDefault(c => c.InteriorColorId == listItem.InteriorColorId).InteriorColor;
                listItem.ImageFileName = car.ImageFileName;
                listItem.ListingPrice = car.ListingPrice;
                listItem.Mileage = car.Mileage;
                listItem.ModelYear = car.ModelYear;
                listItem.MSRP = car.MSRP;
                listItem.TransmissionId = car.TransmissionId;
                listItem.Transmission = transmissions.FirstOrDefault(t => t.TransmissionId == listItem.TransmissionId).Transmission;
                listItem.VehicleId = car.VehicleId;
                listItem.VIN = car.VIN;

                carList.Add(listItem);
            }

            return carList;
        }

        public ListingItem GetVehicleById(int vehicleId)
        {
            var vehicles = new VehiclesMockRepo().GetAllVehicles();
            var car = vehicles.FirstOrDefault(c => c.VehicleId == vehicleId);

            var bodyStyles = new BodyStylesMockRepo().GetAllBodyStyles();
            var conditions = new ConditionsMockRepo().GetAllConditions();
            var exteriorColors = new ExteriorColorsMockRepo().GetAllExteriorColors();
            var interiorColors = new InteriorColorsMockRepo().GetAllInteriorColors();
            var makes = new MakesMockRepo().GetAllMakes();
            var models = new CarModelMockRepo().GetAllCarModels();
            var transmissions = new TransmissionMockRepo().GetAllTransmissions();


            var listItem = new ListingItem();

            listItem.ConditionId = car.ConditionId;
            foreach (var condition in conditions)
            {
                if (condition.ConditionId == car.ConditionId)
                {
                    listItem.Condition = condition.Condition;
                }
            }

            listItem.Featured = car.Featured;
            listItem.CarDescription = car.CarDescription;
            listItem.Id = car.Id;
            listItem.CreationDate = car.AddDate;
            listItem.Sold = car.Sold;

            listItem.BodyStyleId = car.BodyStyleId;
            foreach (var style in bodyStyles)
            {
                if (style.BodyStyleId == listItem.BodyStyleId)
                {
                    listItem.BodyStyle = style.BodyStyle;
                    break;
                }
            }

            listItem.CarModelId = car.CarModelId;
            foreach (var model in models)
            {
                if (model.CarModelId == car.CarModelId)
                {
                    listItem.CarModel = model.CarModel;
                    listItem.MakeId = model.MakeId;
                    break;
                }
            }

            foreach (var make in makes)
            {
                if (listItem.MakeId == make.MakeId)
                {
                    listItem.Make = make.Make;
                    break;
                }
            }

            listItem.ExteriorColorId = car.ExteriorColorId;
            foreach (var color in exteriorColors)
            {
                if (color.ExteriorColorId == car.ExteriorColorId)
                {
                    listItem.ExteriorColor = color.ExteriorColor;
                    break;
                }
            }

            listItem.InteriorColorId = car.InteriorColorId;
            foreach (var color in interiorColors)
            {
                if (color.InteriorColorId == car.InteriorColorId)
                {
                    listItem.InteriorColor = color.InteriorColor;
                    break;
                }
            }

            listItem.ImageFileName = car.ImageFileName;
            listItem.ListingPrice = car.ListingPrice;
            listItem.Mileage = car.Mileage;
            listItem.ModelYear = car.ModelYear;
            listItem.MSRP = car.MSRP;
            listItem.TransmissionId = car.TransmissionId;

            foreach (var transmission in transmissions)
            {
                if (transmission.TransmissionId == car.TransmissionId)
                {
                    listItem.Transmission = transmission.Transmission;
                    break;
                }
            }

            listItem.VehicleId = car.VehicleId;
            listItem.VIN = car.VIN;

            return listItem;
        }

        public void Insert(Vehicle vehicle)
        {
            var repo = new VehiclesMockRepo();
            repo.Insert(vehicle);
        }

        public IEnumerable<ListingShortItem> NewSearch(ListingSearchParameters parameters)
        {
            var date = DateTime.Now.AddYears(-1).Year;

            var listings = GetAllVehicles().Where(v => v.ModelYear >= date);

            if (parameters.MaxPrice > 0 && parameters.MaxPrice != null)
            {
                listings = listings.Where(c => c.ListingPrice < parameters.MaxPrice);
            }
            if (parameters.MinPrice > 0 && parameters.MinPrice != null)
            {
                listings = listings.Where(c => c.ListingPrice > parameters.MinPrice);
            }
            if (parameters.MinYear > 0 && parameters.MinYear != null)
            {
                listings = listings.Where(c => c.ModelYear > parameters.MinYear);
            }
            if (parameters.MaxYear > 0 && parameters.MaxYear != null)
            {
                listings = listings.Where(c => c.ListingPrice < parameters.MaxYear);
            }
            //if (!string.IsNullOrEmpty(parameters.SearchBox))
            //{
            //    if(listings.Contains(parameters.SearchBox)
            //    listings = listings.Where(c =>c.Make .Contains(parameters.SearchBox));
            //}

            return listings;
        }

        public void Update(Vehicle vehicle)
        {
            var repo = new VehiclesMockRepo();
            repo.Delete(vehicle.VehicleId);
            repo.Insert(vehicle);
        }

        public IEnumerable<ListingShortItem> UsedSearch(ListingSearchParameters parameters)
        {
            var date = DateTime.Now.AddYears(-1).Year;

            var listings = GetAllVehicles().Where(v => v.ModelYear <= date);

            if (parameters.MaxPrice > 0 && parameters.MaxPrice != null)
            {
                listings = listings.Where(c => c.ListingPrice < parameters.MaxPrice);
            }
            if (parameters.MinPrice > 0 && parameters.MinPrice != null)
            {
                listings = listings.Where(c => c.ListingPrice > parameters.MinPrice);
            }
            if (parameters.MinYear > 0 && parameters.MinYear != null)
            {
                listings = listings.Where(c => c.ModelYear > parameters.MinYear);
            }
            if (parameters.MaxYear > 0 && parameters.MaxYear != null)
            {
                listings = listings.Where(c => c.ListingPrice < parameters.MaxYear);
            }

            return listings;
        }
    }
}
