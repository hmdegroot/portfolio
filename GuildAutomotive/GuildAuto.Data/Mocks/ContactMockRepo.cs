﻿using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Models.Tables;

namespace GuildAuto.Data.Mocks
{
    public class ContactMockRepo : IContactsRepository
    {
        private static List<Contacts> _contacts = new List<Contacts>()
        {
            new Contacts
            {
                ContactId=1,
                ContactMessage = "I am interested in purchasing a car.",
                ContactName = "George Jetson",
                Email = "George@TheJetsons.com",
                Phone = ""
            }

        };
        public void Insert(Contacts contact)
        {
            contact.ContactId = _contacts.Max(c => c.ContactId) + 1;
            _contacts.Add(contact);
        }
    }
}
