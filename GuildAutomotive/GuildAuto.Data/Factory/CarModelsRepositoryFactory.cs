﻿using GuildAuto.Data.Production;
using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Data.Mocks;

namespace GuildAuto.Data.Factory
{
    public class CarModelsRepositoryFactory
    {
        public static ICarModelsRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "PROD":
                    return new CarModelsRepositoryDapper();
                case "TEST":
                    return new CarModelMockRepo();
                default:
                    throw new Exception("Mode value in app config is not valid.");
            }
        }
    }
}
