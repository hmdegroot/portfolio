﻿using GuildAuto.Data.Production;
using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Data.Mocks;

namespace GuildAuto.Data.Factory
{
    public class ExteriorColorRepositoryFactory
    {
        public static IExteriorColorsRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "PROD":
                    return new ExteriorColorsRepositoryDapper();
                case "TEST":
                    return new ExteriorColorsMockRepo();
                default:
                    throw new Exception("Mode value in app config is not valid.");
            }
        }
    }
}
