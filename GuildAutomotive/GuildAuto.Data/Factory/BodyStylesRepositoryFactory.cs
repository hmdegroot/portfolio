﻿using GuildAuto.Data.Production;
using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Data.Mocks;

namespace GuildAuto.Data.Factory
{
    public class BodyStylesRepositoryFactory
    {
        public static IBodyStylesRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "PROD":
                    return new BodyStylesRepositoryDapper();
                case "TEST":
                    return new BodyStylesMockRepo();
                default:
                    throw new Exception("Mode value in app config is not valid.");
            }
        }
    }
}
