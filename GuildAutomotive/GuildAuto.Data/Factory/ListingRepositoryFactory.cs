﻿using GuildAuto.Data.Production;
using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Data.Mocks;

namespace GuildAuto.Data.Factory
{
    public static class ListingRepositoryFactory
    {
        public static IListingsRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "PROD":
                    return new ListingRepositoryDapper();
                case "TEST":
                    return new ListingMockRepo();
                default:
                    throw new Exception("Mode value in app config is not valid.");
            }
        }
    }
}
