﻿using GuildAuto.Data.Interfaces;
using GuildAuto.Data.Mocks;
using GuildAuto.Data.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Data.Factory
{
    public class RolesRepositoryFactory
    {
        public static IRolesRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "PROD":
                    return new RolesRepositoryDapper();
                case "TEST":
                    return new RolesMockRepo();
                default:
                    throw new Exception("Mode value in app config is not valid.");
            }
        }
    }
}

