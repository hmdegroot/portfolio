﻿using GuildAuto.Data.Production;
using GuildAuto.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildAuto.Data.Mocks;

namespace GuildAuto.Data.Factory
{
    public class AdminUserRepositoryFactory
    {
        public static IAdminUserRepository GetRepository()
        {
            switch (Settings.GetRepositoryType())
            {
                case "PROD":
                    return new AdminUserRepositoryDapper();
                case "TEST":
                    return new AdminUserMockRepo();
                default:
                    throw new Exception("Mode value in app config is not valid.");
            }
        }
    }
}
