﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Models
{
    public class EditVehicleViewModel : IValidatableObject
    {
        public IEnumerable<SelectListItem> Transmissions { get; set; }
        public IEnumerable<SelectListItem> Makes { get; set; }
        public IEnumerable<SelectListItem> CarModels { get; set; }
        public IEnumerable<SelectListItem> Conditions { get; set; }
        public IEnumerable<SelectListItem> BodyStyles { get; set; }
        public IEnumerable<SelectListItem> InteriorColors { get; set; }
        public IEnumerable<SelectListItem> ExteriorColors { get; set; }
        public ListingItem ListingItem { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }
        public Vehicle Vehicle { get; set; }
        public string DeleteImageFileName { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (ListingItem.BodyStyleId < 1)
            {
                errors.Add(new ValidationResult("Body Style is required"));
            }

            if (string.IsNullOrEmpty(ListingItem.CarDescription))
            {
                errors.Add(new ValidationResult("Description is required"));
            }

            if (ListingItem.CarModelId < 1)
            {
                errors.Add(new ValidationResult("Car Model is required"));
            }

            if (ImageUpload != null && ImageUpload.ContentLength > 0)
            {
                var extensions = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

                var extension = Path.GetExtension(ImageUpload.FileName.ToLower());

                if (!extensions.Contains(extension))
                {
                    errors.Add(new ValidationResult("Image file must be a jpg, png, gif, or jpeg."));
                }
            }
            if (ListingItem.ModelYear <= 1999)
            {
                errors.Add(new ValidationResult("Year is required. Vehicle must be no older than 2000."));
            }
            var dateNow = DateTime.Now;
            var nextYear = dateNow.AddYears(1);
            int year = nextYear.Year;

            if (ListingItem.ModelYear > year)
            {
                errors.Add(new ValidationResult($"Model year cannot be higher than {year}."));
            }
            if (ListingItem.ConditionId == 1)
            {
                if (ListingItem.Mileage > 1000)
                {
                    errors.Add(new ValidationResult("Only vehicles with mileage lower than 1,000 may be listed as 'New'"));
                }
            }
            if (ListingItem.ListingPrice < 1)
            {
                errors.Add(new ValidationResult("Listing price must be a positive number"));
            }
            if (ListingItem.MSRP < 1)
            {
                errors.Add(new ValidationResult("MSRP must be a positive number"));
            }
            return errors;
        }
    }
}