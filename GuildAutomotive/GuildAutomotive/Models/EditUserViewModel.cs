﻿using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Models
{
    public class EditUserViewModel : IValidatableObject
    {
        public AdminUser User { get; set; }
        public IEnumerable<SelectListItem> Roles{ get; set; }
        public string OldRole { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
     //   [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            
            if (ConfirmPassword != NewPassword)
            {
                errors.Add(new ValidationResult("Passwords do not match"));
            }

            if (string.IsNullOrEmpty(User.FirstName))
            {
                errors.Add(new ValidationResult("First name is required."));
            }

            if (string.IsNullOrEmpty(User.LastName))
            {
                errors.Add(new ValidationResult("Last name is required."));
            }

            if (string.IsNullOrEmpty(User.Email))
            {
                errors.Add(new ValidationResult("Email name is required."));
            }

            return errors;
        }
}
}