﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuildAutomotive.Models
{
    public class AddMakeViewModel 
    {
        [Required(ErrorMessage = "Please enter a Make")]
        public Makes Make{ get; set; }
        public IEnumerable<MakeDetails> Details{ get; set; }

        
    }
}