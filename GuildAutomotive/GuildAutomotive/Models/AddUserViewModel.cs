﻿using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Models
{
    public class AddUserViewModel : IValidatableObject
    {
        public IEnumerable<SelectListItem> Roles { get; set; }
        public RegisterViewModel RegisterViewModel { get; set; }
        public string Role { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (RegisterViewModel.Password != RegisterViewModel.ConfirmPassword)
            {
                errors.Add(new ValidationResult("Passwords do not match"));
            }

            if (string.IsNullOrEmpty(RegisterViewModel.FirstName))
            {
                errors.Add(new ValidationResult("First name is required."));
            }

            if (string.IsNullOrEmpty(RegisterViewModel.LastName))
            {
                errors.Add(new ValidationResult("Last name is required."));
            }

            if (string.IsNullOrEmpty(RegisterViewModel.Email))
            {
                errors.Add(new ValidationResult("Email name is required."));
            }

            return errors;
        }
    }
}