﻿using GuildAuto.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildAutomotive.Models
{
    public class InventoryReportViewModel
    {
        public IEnumerable<InventoryReport> NewInventory { get; set; }
        public IEnumerable<InventoryReport> UsedInventory { get; set; }
    }
}