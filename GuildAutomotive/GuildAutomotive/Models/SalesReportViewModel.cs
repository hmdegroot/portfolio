﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Models
{
    public class SalesReportViewModel
    {
        public SalesSearchParameters Parameters { get; set; }
        public IEnumerable<SalesReport> Sales { get; set; }
        public IEnumerable<SelectListItem> AdminUser { get; set; }
    }
}