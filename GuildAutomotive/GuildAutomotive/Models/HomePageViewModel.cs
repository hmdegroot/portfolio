﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildAutomotive.Models
{
    public class HomePageViewModel
    {
        public IEnumerable<FeaturedVehicles> FeaturedVehicles { get; set; }
        public IEnumerable<Specials> Specials { get; set; }
    }
}