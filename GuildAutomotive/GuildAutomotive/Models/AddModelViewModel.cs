﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Models
{
    public class AddModelViewModel : IValidatableObject
    {
        public IEnumerable<SelectListItem> Makes { get; set; }
        public List<CarModelDetails> Details { get; set; }
        public CarModels CarModel { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(CarModel.CarModel))
            {
                errors.Add(new ValidationResult("Model field is required"));
            }
            if (CarModel.MakeId < 1)
            {
                errors.Add(new ValidationResult("Please select a Make"));
            }

            return errors;
        }
    }
}