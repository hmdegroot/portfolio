﻿using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuildAutomotive.Models
{
    public class SpecialViewModel : IValidatableObject
    {
        public Specials Special { get; set; }
        public IEnumerable<Specials> SpecialList { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(Special.SpecialDescription))
            {
                errors.Add(new ValidationResult("Description is required"));
            }

            if (string.IsNullOrEmpty(Special.Title))
            {
                errors.Add(new ValidationResult("Title is required"));
            }

            return errors;
        }
    }


}