﻿using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Models
{
    public class PurchaseViewModel : IValidatableObject
    {
        public ListingItem ListingItem{ get; set; }
        public IEnumerable<SelectListItem> States{ get; set; }
        public IEnumerable<SelectListItem> PurchaseTypes { get; set; }
        public PurchaseInfo PurchaseInfo { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(PurchaseInfo.BuyerName))
            {
                errors.Add(new ValidationResult("Name is required."));
            }

            if (string.IsNullOrEmpty(PurchaseInfo.Email) && (string.IsNullOrEmpty(PurchaseInfo.Phone)))
            {
                errors.Add(new ValidationResult("Please enter an email address or phone number."));
            }

            if (string.IsNullOrEmpty(PurchaseInfo.Street1))
            {
                errors.Add(new ValidationResult("Address is required."));
            }

            if (string.IsNullOrEmpty(PurchaseInfo.City))
            {
                errors.Add(new ValidationResult("City is required."));
            }

            if (PurchaseInfo.ZipCode.ToString().Length > 5 || PurchaseInfo.ZipCode.ToString().Length < 5)
            {
                errors.Add(new ValidationResult("Please enter a valid zip code."));
            }
            if (string.IsNullOrEmpty(PurchaseInfo.StateId))
            {
                errors.Add(new ValidationResult("State is required."));
            }
            if (PurchaseInfo.SalePrice > ListingItem.MSRP)
            {
                errors.Add(new ValidationResult("Sale price cannot exceed MSRP."));
            }
            if (PurchaseInfo.SalePrice / ListingItem.MSRP < .95m)
            {
                errors.Add(new ValidationResult("Sale price can not be less than 95% of MSRP."));
            }
            if (!string.IsNullOrEmpty(PurchaseInfo.Email))
            {
                try
                {
                    MailAddress m = new MailAddress(PurchaseInfo.Email);
                }
                catch
                {
                    errors.Add(new ValidationResult("Email address is not valid"));
                }
            }
            return errors;
        }
    }
}