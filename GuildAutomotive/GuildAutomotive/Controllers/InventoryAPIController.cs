﻿using GuildAuto.Data.Factory;
using GuildAuto.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GuildAutomotive.Controllers
{
     public class InventoryAPIController : ApiController
    {
        [Route("api/inventory/used")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Used(decimal? minPrice, decimal? maxPrice, int? minYear, int? maxYear, string searchBox)
        {
            var repo = ListingRepositoryFactory.GetRepository();

            try
            {
                var parameters = new ListingSearchParameters()
                {
                    MaxPrice = maxPrice,
                    MinPrice = minPrice,
                    MaxYear = maxYear,
                    MinYear = minYear,
                    SearchBox = searchBox
                };

                var result = repo.UsedSearch(parameters);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/inventory/new")]
        [AcceptVerbs("GET")]
        public IHttpActionResult New(decimal? minPrice, decimal? maxPrice, int? minYear, int? maxYear, string searchBox)
        {
            var repo = ListingRepositoryFactory.GetRepository();

            try
            {
                var parameters = new ListingSearchParameters()
                {
                    MaxPrice = maxPrice,
                    MinPrice = minPrice,
                    MaxYear = maxYear,
                    MinYear = minYear,
                    SearchBox = searchBox
                };

                var result = repo.NewSearch(parameters);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
