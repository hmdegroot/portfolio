﻿using GuildAuto.Data.Factory;
using GuildAuto.Models.Tables;
using GuildAutomotive.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace GuildAutomotive.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            HomePageViewModel model = new HomePageViewModel();
            
            model.Specials = SpecialsRepositoryFactory.GetRepository().GetAllSpecials();
            model.FeaturedVehicles = ListingRepositoryFactory.GetRepository().GetFeaturedVehicles();
            
            return View(model);
        }

        public ActionResult Contact()
        {
            var model = new Contacts();
            if (!string.IsNullOrEmpty(Request.QueryString["Vin"]))
            {
                model.ContactMessage = "VIN: " + Request.QueryString["Vin"];
            }
            
            return View(model);
        }

        [HttpPost]
        public ActionResult Contact(Contacts model)
        {
            if (ModelState.IsValid)
            {
                var repo = ContactsRepositoryFactory.GetRepository();

                try
                {
                    repo.Insert(model);

                    return RedirectToAction("Index", "Home");
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }

            return View(model);
        }

        //GET: Specials
        public ActionResult Specials()
        {
            var model = SpecialsRepositoryFactory.GetRepository().GetAllSpecials();
            return View(model);
        }
    }
}