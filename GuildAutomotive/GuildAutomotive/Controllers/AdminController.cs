﻿using GuildAuto.Data.Factory;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using GuildAutomotive.Controllers;
using GuildAutomotive.Models;
using GuildAutomotive.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Controllers
{
    public class AdminController : Controller
    {

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Vehicles()
        {
            var model = ListingRepositoryFactory.GetRepository().GetAllVehicles();
            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult AddVehicle()
        {
            var model = new AddVehicleViewModel();

            var makesRepo = MakesRepositoryFactory.GetRepository();
            var transmissionRepo = TransmissionsRepositoryFactory.GetRepository();
            var carModelsRepo = CarModelsRepositoryFactory.GetRepository();
            var conditionsRepo = ConditionsRepositoryFactory.GetRepository();
            var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
            var exteriorColorsRepo = ExteriorColorRepositoryFactory.GetRepository();
            var interiorColorsRepo = InteriorColorRepositoryFactory.GetRepository();

            model.ListingItem = new ListingItem();
            model.Makes = new SelectList(makesRepo.GetAllMakes(), "MakeId", "Make");
            model.Transmissions = new SelectList(transmissionRepo.GetAllTransmissions(), "TransmissionId", "Transmission");
            model.CarModels = new SelectList(carModelsRepo.GetAllCarModels(), "CarModelId", "CarModel");
            model.Conditions = new SelectList(conditionsRepo.GetAllConditions(), "ConditionId", "Condition");
            model.BodyStyles = new SelectList(bodyStylesRepo.GetAllBodyStyles(), "BodyStyleId", "BodyStyle");
            model.ExteriorColors = new SelectList(exteriorColorsRepo.GetAllExteriorColors(), "ExteriorColorId", "ExteriorColor");
            model.InteriorColors = new SelectList(interiorColorsRepo.GetAllInteriorColors(), "InteriorColorId", "InteriorColor");

            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult AddVehicle(AddVehicleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repo = ListingRepositoryFactory.GetRepository();

                try
                {
                    if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0)
                    {
                        var savepath = Server.MapPath("~/Images");

                        string fileName = Path.GetFileNameWithoutExtension(model.ImageUpload.FileName);
                        string extension = Path.GetExtension(model.ImageUpload.FileName);

                        var filePath = Path.Combine(savepath, fileName + extension);

                        int counter = 1;
                        while (System.IO.File.Exists(filePath))
                        {
                            filePath = Path.Combine(savepath, fileName + counter.ToString() + extension);
                            counter++;
                        }

                        model.ImageUpload.SaveAs(filePath);
                        model.Vehicle.ImageFileName = Path.GetFileName(filePath);
                        model.Vehicle.Id = AuthorizeUtilities.GetUserId(this);
                        model.Vehicle.AddDate = DateTime.Now;
                        model.Vehicle.Sold = false;
                    }

                    repo.Insert(model.Vehicle);

                    return RedirectToAction("EditVehicle", new { id = model.Vehicle.VehicleId });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var makesRepo = MakesRepositoryFactory.GetRepository();
                var transmissionRepo = TransmissionsRepositoryFactory.GetRepository();
                var carModelsRepo = CarModelsRepositoryFactory.GetRepository();
                var conditionsRepo = ConditionsRepositoryFactory.GetRepository();
                var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
                var exteriorColorsRepo = ExteriorColorRepositoryFactory.GetRepository();
                var interiorColorsRepo = InteriorColorRepositoryFactory.GetRepository();

                model.ListingItem = new ListingItem();
                model.Makes = new SelectList(makesRepo.GetAllMakes(), "MakeId", "Make");
                model.Transmissions = new SelectList(transmissionRepo.GetAllTransmissions(), "TransmissionId", "Transmission");
                model.CarModels = new SelectList(carModelsRepo.GetAllCarModels(), "CarModelId", "CarModel");
                model.Conditions = new SelectList(conditionsRepo.GetAllConditions(), "ConditionId", "Condition");
                model.BodyStyles = new SelectList(bodyStylesRepo.GetAllBodyStyles(), "BodyStyleId", "BodyStyle");
                model.ExteriorColors = new SelectList(exteriorColorsRepo.GetAllExteriorColors(), "ExteriorColorId", "ExteriorColor");
                model.InteriorColors = new SelectList(interiorColorsRepo.GetAllInteriorColors(), "InteriorColorId", "InteriorColor");

                return View(model);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditVehicle(EditVehicleViewModel model, string action)
        {
            switch (action)
            {
                case "edit":
                    EditVehicleExt(model);
                    break;
                case "delete":
                    DeleteVehicle(model);
                    break;
            }

            if (action == "edit")
            {
                return RedirectToAction("EditVehicle", new { id = model.Vehicle.VehicleId });
            }
            else
            {
                return RedirectToAction("Vehicles");
            }
        }

        [Authorize(Roles = "admin")]
        private void DeleteVehicle(EditVehicleViewModel model)
        {
            var id = model.ListingItem.VehicleId;
            try
            {
                var repo = VehicleRepositoryFactory.GetRepository();

                if (model.DeleteImageFileName != null)
                {
                    var savepath = Server.MapPath("~/Images");

                    var deletePath = Path.Combine(savepath, model.DeleteImageFileName);
                    if (System.IO.File.Exists(deletePath))
                    {
                        System.IO.File.Delete(deletePath);
                    }
                }
                repo.Delete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Users()
        {
            var repo = AdminUserRepositoryFactory.GetRepository();
            var model = repo.GetAllUserDetails();
            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Makes()
        {
            var model = new AddMakeViewModel();

            model.Details = MakesRepositoryFactory.GetRepository().GetMakeDetails();
            model.Make = new Makes();

            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult Makes(Makes model)
        {
            if (ModelState.IsValid)
            {
                var repo = MakesRepositoryFactory.GetRepository();

                try
                {
                    model.Id = AuthorizeUtilities.GetUserId(this);
                    model.DateAdded = DateTime.Now;


                    repo.Insert(model);

                    return RedirectToAction("Makes");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var tryAgain = MakesRepositoryFactory.GetRepository().GetAllMakes();
                return View(tryAgain);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult Models()
        {
            var model = new AddModelViewModel();
            var makesRepo = MakesRepositoryFactory.GetRepository();

            model.Details = CarModelsRepositoryFactory.GetRepository().GetModelDetails();
            model.Makes = new SelectList(makesRepo.GetAllMakes(), "MakeId", "Make");
            model.CarModel = new CarModels();

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Specials()
        {
            var model = new SpecialViewModel();

            model.SpecialList = SpecialsRepositoryFactory.GetRepository().GetAllSpecials();
            model.Special = new Specials();

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeleteSpecial(int id)
        {
            try
            {
                var repo = SpecialsRepositoryFactory.GetRepository();

                repo.Delete(id);

                return RedirectToAction("Specials");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult Specials(SpecialViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repo = SpecialsRepositoryFactory.GetRepository();
                var specialToAdd = new Specials();

                specialToAdd.Title = model.Special.Title;
                specialToAdd.SpecialDescription = model.Special.SpecialDescription;

                try
                {
                    repo.Insert(specialToAdd);

                    return RedirectToAction("Specials");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var newModel = new SpecialViewModel();

                newModel.SpecialList = SpecialsRepositoryFactory.GetRepository().GetAllSpecials();
                newModel.Special = new Specials();

                return View(newModel);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult Models(AddModelViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repo = CarModelsRepositoryFactory.GetRepository();
                var modelToAdd = new CarModels();

                modelToAdd.CarModel = model.CarModel.CarModel;
                modelToAdd.MakeId = model.CarModel.MakeId;
                try
                {
                    modelToAdd.Id = AuthorizeUtilities.GetUserId(this);
                    modelToAdd.DateAdded = DateTime.Now;


                    repo.Insert(modelToAdd);

                    return RedirectToAction("Models");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {

                var makesRepo = MakesRepositoryFactory.GetRepository();

                model.Details = CarModelsRepositoryFactory.GetRepository().GetModelDetails();
                model.Makes = new SelectList(makesRepo.GetAllMakes(), "MakeId", "Make");

                return View(model);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditVehicle(int id)
        {
            var model = new EditVehicleViewModel();

            var makesRepo = MakesRepositoryFactory.GetRepository();
            var transmissionRepo = TransmissionsRepositoryFactory.GetRepository();
            var carModelsRepo = CarModelsRepositoryFactory.GetRepository();
            var conditionsRepo = ConditionsRepositoryFactory.GetRepository();
            var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
            var exteriorColorsRepo = ExteriorColorRepositoryFactory.GetRepository();
            var interiorColorsRepo = InteriorColorRepositoryFactory.GetRepository();

            model.Vehicle = new Vehicle();
            model.ListingItem = ListingRepositoryFactory.GetRepository().GetVehicleById(id);
            model.ListingItem.VehicleId = id;
            model.DeleteImageFileName = model.ListingItem.ImageFileName;
            model.Makes = new SelectList(makesRepo.GetAllMakes(), "MakeId", "Make");
            model.Transmissions = new SelectList(transmissionRepo.GetAllTransmissions(), "TransmissionId", "Transmission");
            model.CarModels = new SelectList(carModelsRepo.GetAllCarModels(), "CarModelId", "CarModel");
            model.Conditions = new SelectList(conditionsRepo.GetAllConditions(), "ConditionId", "Condition");
            model.BodyStyles = new SelectList(bodyStylesRepo.GetAllBodyStyles(), "BodyStyleId", "BodyStyle");
            model.ExteriorColors = new SelectList(exteriorColorsRepo.GetAllExteriorColors(), "ExteriorColorId", "ExteriorColor");
            model.InteriorColors = new SelectList(interiorColorsRepo.GetAllInteriorColors(), "InteriorColorId", "InteriorColor");

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public EditVehicleViewModel EditVehicleExt(EditVehicleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repo = ListingRepositoryFactory.GetRepository();
                var oldListing = repo.GetVehicleById(model.ListingItem.VehicleId);
                model.Vehicle = new Vehicle();

                model.Vehicle.VehicleId = model.ListingItem.VehicleId;
                model.Vehicle.AddDate = oldListing.CreationDate;
                model.Vehicle.Id = oldListing.Id;
                model.Vehicle.BodyStyleId = model.ListingItem.BodyStyleId;
                model.Vehicle.CarDescription = model.ListingItem.CarDescription;
                model.Vehicle.CarModelId = model.ListingItem.CarModelId;
                model.Vehicle.ConditionId = model.ListingItem.ConditionId;
                model.Vehicle.ExteriorColorId = model.ListingItem.ExteriorColorId;
                model.Vehicle.InteriorColorId = model.ListingItem.InteriorColorId;
                model.Vehicle.ListingPrice = model.ListingItem.ListingPrice;
                model.Vehicle.Mileage = model.ListingItem.Mileage;
                model.Vehicle.ModelYear = model.ListingItem.ModelYear;
                model.Vehicle.MSRP = model.ListingItem.MSRP;
                model.Vehicle.Sold = model.ListingItem.Sold;
                model.Vehicle.TransmissionId = model.ListingItem.TransmissionId;
                model.Vehicle.VIN = model.ListingItem.VIN;
                model.Vehicle.ImageFileName = model.ListingItem.ImageFileName;
                model.Vehicle.Featured = model.ListingItem.Featured;

                try
                {
                    if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0)
                    {
                        var savepath = Server.MapPath("~/Images");

                        string fileName = Path.GetFileNameWithoutExtension(model.ImageUpload.FileName);
                        string extension = Path.GetExtension(model.ImageUpload.FileName);

                        var filePath = Path.Combine(savepath, fileName + extension);

                        int counter = 1;
                        while (System.IO.File.Exists(filePath))
                        {
                            filePath = Path.Combine(savepath, fileName + counter.ToString() + extension);
                            counter++;
                        }

                        model.ImageUpload.SaveAs(filePath);
                        model.Vehicle.ImageFileName = Path.GetFileName(filePath);

                        var oldPath = Path.Combine(savepath, oldListing.ImageFileName);
                        if (System.IO.File.Exists(oldPath))
                        {
                            System.IO.File.Delete(oldPath);
                        }


                    }
                    else
                    {
                        model.Vehicle.ImageFileName = oldListing.ImageFileName;
                    }

                    repo.Update(model.Vehicle);


                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {

                var makesRepo = MakesRepositoryFactory.GetRepository();
                var transmissionRepo = TransmissionsRepositoryFactory.GetRepository();
                var carModelsRepo = CarModelsRepositoryFactory.GetRepository();
                var conditionsRepo = ConditionsRepositoryFactory.GetRepository();
                var bodyStylesRepo = BodyStylesRepositoryFactory.GetRepository();
                var exteriorColorsRepo = ExteriorColorRepositoryFactory.GetRepository();
                var interiorColorsRepo = InteriorColorRepositoryFactory.GetRepository();

                model.ListingItem = ListingRepositoryFactory.GetRepository().GetVehicleById(model.ListingItem.VehicleId);
                model.Makes = new SelectList(makesRepo.GetAllMakes(), "MakeId", "Make");
                model.Transmissions = new SelectList(transmissionRepo.GetAllTransmissions(), "TransmissionId", "Transmission");
                model.CarModels = new SelectList(carModelsRepo.GetAllCarModels(), "CarModelId", "CarModel");
                model.Conditions = new SelectList(conditionsRepo.GetAllConditions(), "ConditionId", "Condition");
                model.BodyStyles = new SelectList(bodyStylesRepo.GetAllBodyStyles(), "BodyStyleId", "BodyStyle");
                model.ExteriorColors = new SelectList(exteriorColorsRepo.GetAllExteriorColors(), "ExteriorColorId", "ExteriorColor");
                model.InteriorColors = new SelectList(interiorColorsRepo.GetAllInteriorColors(), "InteriorColorId", "InteriorColor");


            }
            return model;
        }

        [Authorize(Roles = "admin")]
        public ActionResult AddUser()
        {
            var model = new AddUserViewModel();
            model.Roles = new SelectList(RolesRepositoryFactory.GetRepository().GetAllRoles(), "Name", "Name");
            model.RegisterViewModel = new RegisterViewModel();

            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult AddUser(AddUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new ApplicationUser()
                    {
                        UserName = model.RegisterViewModel.Email,
                        FirstName = model.RegisterViewModel.FirstName,
                        LastName = model.RegisterViewModel.LastName,
                        Email = model.RegisterViewModel.Email
                    };

                    var result = UserManager.Create(user, model.RegisterViewModel.Password);

                    if (result.Succeeded)
                    {
                        UserManager.AddToRole(user.Id, model.Role);
                    }

                    return RedirectToAction("Users");
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }

            return View(model);

        }
        //fix edit user
        [Authorize(Roles = "admin")]
        public ActionResult EditUser(string id)
        {
            var model = new EditUserViewModel();
            var apUser = UserManager.FindById(id);
            model.User = new AdminUser();
            model.User.Email = apUser.Email;
            model.User.FirstName = apUser.FirstName;
            model.User.LastName = apUser.LastName;
            model.User.Id = apUser.Id;
            model.User.Role = UserManager.GetRoles(id).FirstOrDefault();
            model.OldRole = model.User.Role;
           
            model.Roles = new SelectList(RolesRepositoryFactory.GetRepository().GetAllRoles(), "Name", "Name");
            
            return View(model);
        }

        //fix edit user and roles
        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditUser(EditUserViewModel model)
        {
            var currentUser = UserManager.FindById(model.User.Id);

            if (ModelState.IsValidField(model.User.FirstName) && ModelState.IsValidField(model.User.LastName) && ModelState.IsValidField(model.User.Email))
            {
                try
                {
                    currentUser.Email = model.User.Email;
                    currentUser.FirstName = model.User.FirstName;
                    currentUser.LastName = model.User.LastName;

                    if(model.OldRole != model.User.Role)
                    {
                        UserManager.RemoveFromRole(model.User.Id, model.OldRole);
                        UserManager.AddToRole(model.User.Id, model.User.Role);
                    }
                             
                    UserManager.Update(currentUser);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            if (!string.IsNullOrEmpty(model.NewPassword))
            {
                if (ModelState.IsValidField(model.NewPassword) && ModelState.IsValidField(model.ConfirmPassword))
                    try
                    {
                        UserManager.RemovePassword(currentUser.Id);
                        UserManager.AddPassword(currentUser.Id, model.NewPassword);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
            }
            return RedirectToAction("Users");
        }



    }
}