﻿using GuildAuto.Data.Factory;
using GuildAuto.Models.Queries;
using GuildAutomotive.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        [Authorize(Roles ="admin")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Sales()
        {
            var model = new SalesReportViewModel();
            var repo = ReportsRepositoryFactory.GetRepository();
            var adminRepo = AdminUserRepositoryFactory.GetRepository();

            model.Sales = repo.GetAllSales();
            model.Parameters = new SalesSearchParameters();
            model.AdminUser = new SelectList((from s in adminRepo.GetAllUserDetails()
                                              select new
                                              {
                                                  Id = s.Id,
                                                  FullName = s.FirstName + ' ' + s.LastName
                                              }), "Id", "FullName");

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Inventory()
        {
            var model = new InventoryReportViewModel();
            var repo = ReportsRepositoryFactory.GetRepository();

            model.NewInventory = repo.GetNewInventory();
            model.UsedInventory = repo.GetUsedInvetory();

            return View(model);
        }
    }
}