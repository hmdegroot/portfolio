﻿using GuildAuto.Data.Factory;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using GuildAutomotive.Models;
using GuildAutomotive.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Controllers
{
    public class SalesController : Controller
    {
        // GET: Sales
        [Authorize(Roles = "sales, admin")]
        public ActionResult Index()
        {
            var model = ListingRepositoryFactory.GetRepository().GetAllVehicles();
            return View(model);
        }

        [Authorize(Roles = "sales, admin")]
        public ActionResult Purchase(int id)
        {
            var model = new PurchaseViewModel();

            var statesRepo = StatesRepositoryFactory.GetRepository();
            var listingRepo = ListingRepositoryFactory.GetRepository();
            var purchaseRepo = PurchaseTypesRepositoryFactory.GetRepository();



            model.States = new SelectList(statesRepo.GetAllStates(), "StateId", "StateName");
            model.ListingItem = listingRepo.GetVehicleById(id);
            model.PurchaseTypes = new SelectList(purchaseRepo.GetAllPurchaseTypes(), "PurchaseTypeId", "PurchaseType");
            model.PurchaseInfo = new PurchaseInfo();

            model.ListingItem.VehicleId = id;

            return View(model);
        }

        [Authorize(Roles = "sales, admin")]
        [HttpPost]
        public ActionResult Purchase(PurchaseViewModel model)
        {
            model.PurchaseInfo.PurchaseDate = DateTime.Now;
            model.PurchaseInfo.Id = "1";
            model.PurchaseInfo.VehicleId = model.ListingItem.VehicleId;

            var sold = new SoldVehicle();

            sold.VehicleId = model.ListingItem.VehicleId;
            sold.Sold = true;
            sold.ImageFileName = "sold.jpg";
            sold.Featured = false;

            if (ModelState.IsValid)
            {
                var repo = PurchaseInfoRepositoryFactory.GetRepository();

                try
                {
                    model.PurchaseInfo.Id = AuthorizeUtilities.GetUserId(this);

                    repo.InsertPurchase(model.PurchaseInfo);

                    repo.UpdateSold(sold);

                    return RedirectToAction("Index", "Sales");
                }

                catch (Exception ex)
                {
                    throw ex;
                }
            }

            else
            {
                var statesRepo = StatesRepositoryFactory.GetRepository();
                var listingRepo = ListingRepositoryFactory.GetRepository();
                var purchaseRepo = PurchaseTypesRepositoryFactory.GetRepository();

                model.States = new SelectList(statesRepo.GetAllStates(), "StateId", "StateName");
                model.ListingItem = listingRepo.GetVehicleById(model.PurchaseInfo.VehicleId);
                model.PurchaseTypes = new SelectList(purchaseRepo.GetAllPurchaseTypes(), "PurchaseTypeId", "PurchaseType");
                model.PurchaseInfo = new PurchaseInfo();

                return View(model);
            }
        }
    }
}