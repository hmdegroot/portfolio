﻿using GuildAuto.Data.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildAutomotive.Controllers
{
    public class InventoryController : Controller
    {
        // GET: NewInventory
        public ActionResult New()
        {
            return View();
        }

        //GET : UsedInventory
        public ActionResult Used()
        {
            return View();
        }

        //GET : Details
        public ActionResult Details(int id)
        {
            var model = ListingRepositoryFactory.GetRepository().GetVehicleById(id);
            return View(model);
        }

       
    }
}
