﻿using GuildAuto.Data.Factory;
using GuildAuto.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GuildAutomotive.Controllers
{
    public class ReportsAPIController : ApiController
    {
        [Authorize(Roles = "admin")]
        [Route("api/reports/sales")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Sales(string userId, DateTime? fromDate, DateTime? toDate)
        {
            var repo = ReportsRepositoryFactory.GetRepository();

            try
            {
                var parameters = new SalesSearchParameters()
                {
                    Id = userId,
                    FromDate = fromDate,
                    ToDate = toDate
                };

                var result = repo.SalesSearch(parameters);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
