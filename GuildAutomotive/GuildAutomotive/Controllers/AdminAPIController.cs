﻿using GuildAuto.Data.Factory;
using GuildAuto.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GuildAutomotive.Controllers
{
    public class AdminAPIController : ApiController
    {
        [Authorize(Roles = "admin")]
        [Route("api/admin/vehicles")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Vehicles(decimal? minPrice, decimal? maxPrice, int? minYear, int? maxYear, string searchBox)
        {
            var repo = ListingRepositoryFactory.GetRepository();

            try
            {
                var parameters = new ListingSearchParameters()
                {
                    MaxPrice = maxPrice,
                    MinPrice = minPrice,
                    MaxYear = maxYear,
                    MinYear = minYear,
                    SearchBox = searchBox
                };

                var result = repo.AllSearch(parameters);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "admin")]
        [Route("api/admin/models/{makeId}")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetModels(int makeId)
        {
            var repo = CarModelsRepositoryFactory.GetRepository();
            
            try
            {
                //  var result = new SelectList(repo.GetModel(makeId), "CarModelId", "CarModel");

                var result = repo.GetModel(makeId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
