namespace GuildAutomotive.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using GuildAutomotive.Models;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GuildAutomotive.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GuildAutomotive.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //


            // Load the user and role managers with our custom models
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleMgr = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            // have we loaded roles already?

            //var aUser = userMgr.FindById("");
            //if(aUser != null)
            //{
            //    userMgr.AddToRole(aUser.Id, "rolename");
            //}

            if (roleMgr.RoleExists("admin"))
                return;


            // create the admin role
            roleMgr.Create(new IdentityRole() { Name = "admin" });
            roleMgr.Create(new IdentityRole() { Name = "sales" });
            roleMgr.Create(new IdentityRole() { Name = "disabled" });

            // create the default user
            var user = new ApplicationUser()
            {
                UserName = "owner@test.com",
                Email = "owner@test.com"
            };
            var sales = new ApplicationUser()
            {
                UserName = "sales"
            };

            userMgr.Create(sales, "sales123");
            userMgr.AddToRole(sales.Id, "sales");
            // create the user with the manager class
            userMgr.Create(user, "testing123");
            // add the user to the admin role
            userMgr.AddToRole(user.Id, "admin");

            
        }
    }
}
