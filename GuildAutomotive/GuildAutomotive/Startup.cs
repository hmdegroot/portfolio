﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GuildAutomotive.Startup))]
namespace GuildAutomotive
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
