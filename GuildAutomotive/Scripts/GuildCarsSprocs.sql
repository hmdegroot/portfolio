USE GuildCars
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'StatesSelectAll')
      DROP PROCEDURE StatesSelectAll
GO

CREATE PROCEDURE StatesSelectAll AS
BEGIN
	SELECT StateId, StateName
	FROM States
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'BodyStylesSelectAll')
      DROP PROCEDURE BodyStylesSelectAll
GO

CREATE PROCEDURE BodyStylesSelectAll AS
BEGIN
	SELECT BodyStyleId, BodyStyle
	FROM BodyStyles
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'ConditionsSelectAll')
      DROP PROCEDURE ConditionsSelectAll
GO

CREATE PROCEDURE ConditionsSelectAll AS
BEGIN
	SELECT ConditionId, Condition
	FROM Conditions
END
GO


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'ExteriorColorsSelectAll')
      DROP PROCEDURE ExteriorColorsSelectAll
GO

CREATE PROCEDURE ExteriorColorsSelectAll AS
BEGIN
	SELECT ExteriorColorId, ExteriorColor
	FROM ExteriorColors
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'InteriorColorsSelectAll')
      DROP PROCEDURE InteriorColorsSelectAll
GO

CREATE PROCEDURE InteriorColorsSelectAll AS
BEGIN
	SELECT InteriorColorId, InteriorColor
	FROM InteriorColors
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'PurchaseTypesSelectAll')
      DROP PROCEDURE PurchaseTypesSelectAll
GO

CREATE PROCEDURE PurchaseTypesSelectAll AS
BEGIN
	SELECT PurchaseTypeId, PurchaseType
	FROM PurchaseTypes
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'SpecialsSelectAll')
      DROP PROCEDURE SpecialsSelectAll
GO

CREATE PROCEDURE SpecialsSelectAll AS
BEGIN
	SELECT SpecialId, Title, SpecialDescription
	FROM Specials
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'TransmissionsSelectAll')
      DROP PROCEDURE TransmissionsSelectAll
GO

CREATE PROCEDURE TransmissionsSelectAll AS
BEGIN
	SELECT TransmissionId, Transmission
	FROM Transmissions
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetAllAdminUsers')
      DROP PROCEDURE GetAllAdminUsers
GO

CREATE PROCEDURE GetAllAdminUsers AS
BEGIN
	SELECT a.Id, a.FirstName, a.LastName, a.Email, p.[Name] AS "Role"
	FROM AspNetUsers a
	INNER JOIN AspNetUserRoles r ON r.UserId = a.Id
	INNER JOIN AspNetRoles p ON p.Id = r.RoleId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetAllMakes')
      DROP PROCEDURE GetAllMakes
GO

CREATE PROCEDURE GetAllMakes AS
BEGIN
	SELECT MakeId, Make, DateAdded, Id
	FROM Makes
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetMakeDetails')
      DROP PROCEDURE GetMakeDetails
GO

CREATE PROCEDURE GetMakeDetails AS
BEGIN
	SELECT MakeId, Make, DateAdded, m.Id, a.Email
	FROM Makes m
		INNER JOIN AspNetUsers a on a.Id = m.Id
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetModelDetails')
      DROP PROCEDURE GetModelDetails
GO

CREATE PROCEDURE GetModelDetails AS
BEGIN
	SELECT CarModel, CarModelId, c.MakeId, Make, c.DateAdded, m.Id, a.Email
	FROM CarModels c
		INNER JOIN Makes m on m.MakeId = c.MakeId
		INNER JOIN AspNetUsers a on c.Id = a.Id
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetAllCarModels')
      DROP PROCEDURE GetAllCarModels
GO

CREATE PROCEDURE GetAllCarModels AS
BEGIN
	SELECT CarModelId, MakeId, CarModel, DateAdded, Id
	FROM CarModels
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehiclesSelectAll')
      DROP PROCEDURE VehiclesSelectAll
GO

CREATE PROCEDURE VehiclesSelectAll AS
BEGIN
	SELECT VehicleId, CarModelId, ExteriorColorId, InteriorColorId, TransmissionId, ConditionId, ModelYear, BodyStyleId, ListingPrice, MSRP, VIN, Mileage, CarDescription, Featured, AddDate, Id
	FROM Vehicles
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'PurchaseInfoSelectAll')
      DROP PROCEDURE PurchaseInfoSelectAll
GO

CREATE PROCEDURE PurchaseInfoSelectAll AS
BEGIN
	SELECT PurchaseInfoId, BuyerName, Phone, Street1, Street2, Email, StateId, ZipCode, SalePrice, PurchaseTypeId, VehicleId, PurchaseDate, Id
	FROM PurchaseInfo
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehiclesSelectFeatured')
      DROP PROCEDURE VehiclesSelectFeatured
GO

CREATE PROCEDURE VehiclesSelectFeatured AS 
BEGIN
	SELECT v.VehicleId, CarModel, Make, ModelYear, ListingPrice, ImageFileName
	FROM Vehicles v
		INNER JOIN CarModels c ON c.CarModelId = v.CarModelId
		INNER JOIN Makes m ON m.MakeId = c.MakeId
	WHERE v.Featured = 1
	AND v.Sold = 0
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehicleNewList')
      DROP PROCEDURE VehicleNewList
GO

CREATE PROCEDURE VehicleNewList AS 
BEGIN
	SELECT v.VehicleId, Mileage, CarModel, Make, ModelYear, ListingPrice, MSRP, ImageFileName, BodyStyle, InteriorColor, ExteriorColor, Transmission, VIN
	FROM Vehicles v
		INNER JOIN CarModels c ON c.CarModelId = v.CarModelId
		INNER JOIN Makes m ON m.MakeId = c.MakeId
		INNER JOIN BodyStyles b ON b.BodyStyleId = v.BodyStyleId
		INNER JOIN Transmissions t ON t.TransmissionId = v.TransmissionId
		INNER JOIN InteriorColors i ON i.InteriorColorId = v.InteriorColorId
		INNER JOIN ExteriorColors e ON e.ExteriorColorId = v.ExteriorColorId
	WHERE v.ConditionId = 1
	AND v.Sold = 0

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehicleUsedList')
      DROP PROCEDURE VehicleUsedList
GO

CREATE PROCEDURE VehicleUsedList AS 
BEGIN
	SELECT v.VehicleId, CarModel, Mileage, Make, ModelYear, ListingPrice, MSRP, ImageFileName, BodyStyle, InteriorColor, ExteriorColor, Transmission, VIN
	FROM Vehicles v
		INNER JOIN CarModels c ON c.CarModelId = v.CarModelId
		INNER JOIN Makes m ON m.MakeId = c.MakeId
		INNER JOIN BodyStyles b ON b.BodyStyleId = v.BodyStyleId
		INNER JOIN Transmissions t ON t.TransmissionId = v.TransmissionId
		INNER JOIN InteriorColors i ON i.InteriorColorId = v.InteriorColorId
		INNER JOIN ExteriorColors e ON e.ExteriorColorId = v.ExteriorColorId
	WHERE v.ConditionId = 2
	AND v.Sold = 0

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehicleListing')
      DROP PROCEDURE VehicleListing
GO

CREATE PROCEDURE VehicleListing (
	@VehicleId int
) AS 
BEGIN
	SELECT v.VehicleId, CarModel, v.Mileage, v.CarModelId, c.MakeId, Make, ModelYear, ListingPrice, MSRP, v.ImageFileName, v.BodyStyleId, BodyStyle, v.InteriorColorId, InteriorColor, v.ExteriorColorId, 
	ExteriorColor, v.TransmissionId, Transmission, VIN, CarDescription, v.ConditionId, Condition, AddDate, Featured, v.Sold, v.AddDate, v.Id, v.Featured
	FROM Vehicles v
		INNER JOIN CarModels c ON c.CarModelId = v.CarModelId
		INNER JOIN Makes m ON m.MakeId = c.MakeId
		INNER JOIN BodyStyles b ON b.BodyStyleId = v.BodyStyleId
		INNER JOIN Transmissions t ON t.TransmissionId = v.TransmissionId
		INNER JOIN InteriorColors i ON i.InteriorColorId = v.InteriorColorId
		INNER JOIN ExteriorColors e ON e.ExteriorColorId = v.ExteriorColorId
		INNER JOIN Conditions co ON v.ConditionId = co.ConditionId
	WHERE v.VehicleId = @VehicleId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehiclesAllUnsold')
      DROP PROCEDURE VehiclesAllUnsold
GO

CREATE PROCEDURE VehiclesAllUnsold AS 
BEGIN
	SELECT v.VehicleId, CarModel, Make, ModelYear, ListingPrice, MSRP, ImageFileName, BodyStyle, InteriorColor, ExteriorColor, Transmission, VIN, CarDescription, Mileage
	FROM Vehicles v
		INNER JOIN CarModels c ON c.CarModelId = v.CarModelId
		INNER JOIN Makes m ON m.MakeId = c.MakeId
		INNER JOIN BodyStyles b ON b.BodyStyleId = v.BodyStyleId
		INNER JOIN Transmissions t ON t.TransmissionId = v.TransmissionId
		INNER JOIN InteriorColors i ON i.InteriorColorId = v.InteriorColorId
		INNER JOIN ExteriorColors e ON e.ExteriorColorId = v.ExteriorColorId
	WHERE v.Sold = 0
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehicleInsert')
      DROP PROCEDURE VehicleInsert
GO

CREATE PROCEDURE VehicleInsert (
	@VehicleId int = NULL output,
	@CarModelId int,
	@ExteriorColorId int,
	@InteriorColorId int,
	@TransmissionId int,
	@ConditionId int,
	@ModelYear int,
	@BodyStyleId int,
	@ListingPrice decimal(8,2),
	@MSRP decimal(8,2),
	@VIN varchar(40),
	@Mileage int,
	@CarDescription varchar(2000),
	@ImageFileName varchar(50),
	@Featured bit,
	@AddDate datetime2(0),
	@Sold bit,
	@Id NVARCHAR(128)

) AS

BEGIN

	INSERT INTO Vehicles (CarModelId, ExteriorColorId, InteriorColorId, TransmissionId, ConditionId, ModelYear, BodyStyleId, ListingPrice, MSRP, VIN, 
	Mileage, CarDescription, ImageFileName, Featured, AddDate, Sold, Id)
	VALUES (@CarModelId, @ExteriorColorId, @InteriorColorId, @TransmissionId, @ConditionId, @ModelYear, @BodyStyleId, @ListingPrice, @MSRP, @VIN, @Mileage,
	@CarDescription, @ImageFileName, @Featured, @AddDate, @Sold, @Id)

	SET @VehicleId = SCOPE_IDENTITY();
END
GO


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehicleUpdate')
      DROP PROCEDURE VehicleUpdate
GO

CREATE PROCEDURE VehicleUpdate (
	@VehicleId int,
	@CarModelId int,
	@ExteriorColorId int,
	@InteriorColorId int,
	@TransmissionId int,
	@ConditionId int,
	@ModelYear int,
	@BodyStyleId int,
	@ListingPrice decimal(8,2),
	@MSRP decimal(8,2),
	@VIN varchar(40),
	@Mileage int,
	@CarDescription varchar(2000),
	@ImageFileName varchar(50),
	@Featured bit
) AS

BEGIN
	UPDATE Vehicles SET 
	CarModelId = @CarModelId,
	ExteriorColorId = @ExteriorColorId,
	InteriorColorId = @InteriorColorId,
	TransmissionId = @TransmissionId,
	ConditionId = @ConditionId,
	ModelYear = @ModelYear,
	BodyStyleId = @BodyStyleId,
	ListingPrice = @ListingPrice,
	MSRP = @MSRP, 
	VIN = @VIN,
	Mileage = @Mileage,
	CarDescription = @CarDescription, 
	ImageFileName = @ImageFileName,
	Featured = @Featured
	WHERE VehicleId = @VehicleId
END
GO






--IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
--   WHERE ROUTINE_NAME = 'UserInsert')
--      DROP PROCEDURE UserInsert
--GO

--CREATE PROCEDURE UserInsert (
--	@AdminUserId int = NULL output,
--	--@AspId nvarchar(128)
--	@FirstName varchar(30),
--	@LastName varchar(30)
--) AS

--BEGIN

--	INSERT INTO AdminUser(FirstName, LastName)
--	VALUES (@FirstName, @LastName)

--	SET @AdminUserId = SCOPE_IDENTITY();
--END
--GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'PurchaseInsert')
      DROP PROCEDURE PurchaseInsert
GO

CREATE PROCEDURE PurchaseInsert (
	@PurchaseInfoId INT = NULL OUTPUT,
	@BuyerName VARCHAR(100),
	@Phone VARCHAR(25),
	@Street1 VARCHAR(200),
	@Street2 VARCHAR(200),
	@Email VARCHAR(300),
	@StateId CHAR(2),
	@ZipCode INT,
	@SalePrice DECIMAL(7,2),
	@PurchaseTypeId INT,
	@VehicleId INT,
	@PurchaseDate DATETIME2,
	@Id NVARCHAR(128)
) AS

BEGIN

	INSERT INTO PurchaseInfo(BuyerName, Phone, Street1, Street2, Email, StateId, ZipCode, SalePrice, PurchaseTypeId, VehicleId, PurchaseDate, Id)
	VALUES (@BuyerName, @Phone, @Street1, @Street2, @Email, @StateId, @ZipCode, @SalePrice, @PurchaseTypeId, @VehicleId, @PurchaseDate, @Id)

	SET @PurchaseInfoId = SCOPE_IDENTITY();
END
GO


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'ContactInsert')
      DROP PROCEDURE ContactInsert
GO

CREATE PROCEDURE ContactInsert (
	@ContactId int = NULL output,
	@ContactName varchar(60),
	@Email varchar(100),
	@Phone varchar(20),
	@ContactMessage varchar(2000)
) AS

BEGIN 

	INSERT INTO Contacts (ContactName, Email, Phone, ContactMessage)
	VALUES (@ContactName, @Email,@Phone, @ContactMessage)

	SET @ContactId = SCOPE_IDENTITY();
END 
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'UserUpdate')
      DROP PROCEDURE UserUpdate
GO

CREATE PROCEDURE UserUpdate (
	@Id NVARCHAR(128) = NULL output,
	@FirstName varchar(30),
	@LastName varchar(30)
) AS

BEGIN
	UPDATE AspNetUser SET
	FirstName = @FirstName,
	LastName = @LastName
	WHERE Id = @Id	
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetCarModelForMake')
      DROP PROCEDURE GetCarModelForMake
GO

CREATE PROCEDURE GetCarModelForMake (
	@MakeId int
) AS
BEGIN
	SELECT CarModelId, CarModel, DateAdded, Id, MakeId
	FROM CarModels 
	WHERE MakeId = @MakeId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'MakeInsert')
      DROP PROCEDURE MakeInsert
GO

CREATE PROCEDURE MakeInsert (
	@MakeId INT = NULL output,
	@Make VARCHAR(100),
	@DateAdded DATETIME2,
	@Id NVARCHAR(128)
) AS

BEGIN

	INSERT INTO Makes(Make, DateAdded, Id)
	VALUES (@Make, @DateAdded, @Id)

	SET @MakeId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'ModelInsert')
      DROP PROCEDURE ModelInsert
GO

CREATE PROCEDURE ModelInsert (
	@CarModelId INT = NULL output,
	@MakeId INT,
	@CarModel VARCHAR(100),
	@DateAdded DATETIME2,
	@Id NVARCHAR(128)
) AS

BEGIN

	INSERT INTO CarModels(MakeId, CarModel, DateAdded, Id)
	VALUES (@MakeId, @CarModel, @DateAdded, @Id)

	SET @CarModelId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'SpecialInsert')
      DROP PROCEDURE SpecialInsert
GO

CREATE PROCEDURE SpecialInsert (
	@SpecialId INT = NULL output,
	@Title VARCHAR(100),
	@SpecialDescription VARCHAR(3000)
) AS

BEGIN

	INSERT INTO Specials(Title, SpecialDescription)
	VALUES (@Title, @SpecialDescription)
	 
	SET @SpecialId = SCOPE_IDENTITY();
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'SpecialDelete')
      DROP PROCEDURE SpecialDelete
GO

CREATE PROCEDURE SpecialDelete (
	@SpecialId int
) AS
BEGIN
	BEGIN TRANSACTION

	DELETE FROM Specials WHERE SpecialId = @SpecialId;

	COMMIT TRANSACTION
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'SalesReport')
      DROP PROCEDURE SalesReport
GO

CREATE PROCEDURE SalesReport AS
BEGIN

	SELECT FirstName + ' ' + LastName AS [User], 
	SUM(p.SalePrice) AS TotalSales, 			
	COUNT(PurchaseInfoId) AS NumberOfSales
	FROM AspNetUsers a
		INNER JOIN PurchaseInfo p ON  a.Id = p.Id
	GROUP BY FirstName, LastName
	ORDER BY TotalSales DESC
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'NewInventoryReport')
      DROP PROCEDURE NewInventoryReport
GO

CREATE PROCEDURE NewInventoryReport AS
BEGIN
	SELECT v.ModelYear, m.Make, cm.CarModel, c.Condition,
		COUNT(v.VehicleId) AS Stock,
		SUM(MSRP) AS StockValue
	FROM Vehicles v
		INNER JOIN CarModels cm ON v.CarModelId = cm.CarModelId
		INNER JOIN Makes m on cm.MakeId = m.MakeId
		INNER JOIN Conditions c on v.ConditionId = c.ConditionId
	WHERE Sold = 0 AND v.ConditionId = 1
	GROUP BY cm.CarModel, v.ModelYear, m.Make, c.condition
	ORDER BY StockValue DESC
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'UsedInventoryReport')
      DROP PROCEDURE UsedInventoryReport
GO

CREATE PROCEDURE UsedInventoryReport AS
BEGIN
	SELECT v.ModelYear, m.Make, cm.CarModel, c.Condition,
		COUNT(v.VehicleId) AS Stock,
		SUM(MSRP) AS StockValue
	FROM Vehicles v
		INNER JOIN CarModels cm ON v.CarModelId = cm.CarModelId
		INNER JOIN Makes m on cm.MakeId = m.MakeId
		INNER JOIN Conditions c on v.ConditionId = c.ConditionId
	WHERE Sold = 0 AND v.ConditionId = 2
	GROUP BY cm.CarModel, v.ModelYear, m.Make, c.condition
	ORDER BY StockValue DESC
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'SoldUpdate')
      DROP PROCEDURE SoldUpdate
GO

CREATE PROCEDURE SoldUpdate (
	@VehicleId int,
	@Sold bit,
	@Featured bit,
	@ImageFileName varchar(50)
) AS

BEGIN
	UPDATE Vehicles SET 
	Sold = @Sold, 
	ImageFileName = @ImageFileName,
	Featured = @Featured
	WHERE VehicleId = @VehicleId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'VehicleDelete')
      DROP PROCEDURE VehicleDelete
GO

CREATE PROCEDURE VehicleDelete (
	@VehicleId int
) AS
BEGIN
	BEGIN TRANSACTION

	DELETE FROM Vehicles WHERE VehicleId = @VehicleId;

	COMMIT TRANSACTION
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'RolesSelectAll')
      DROP PROCEDURE RolesSelectAll
GO

CREATE PROCEDURE RolesSelectAll AS
BEGIN
	SELECT [Name]
	FROM AspNetRoles
END
GO