USE GuildCars
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GuildCarsDBReset')
      DROP PROCEDURE GuildCarsDBReset
GO

CREATE PROCEDURE GuildCarsDBReset AS
BEGIN
	DELETE FROM PurchaseInfo;
	DELETE FROM States;
	DELETE FROM Vehicles;
	DELETE FROM BodyStyles;
	DELETE FROM Conditions;
	DELETE FROM ExteriorColors;
	DELETE FROM InteriorColors;
	DELETE FROM PurchaseTypes;
	DELETE FROM Specials;
	DELETE FROM Transmissions;
	DELETE FROM CarModels;
	DELETE FROM Makes;
	DELETE FROM AspNetUsers WHERE id IN ('00000000-0000-0000-0000-000000000000', '11111111-1111-1111-1111-111111111111');
	DELETE FROM AspNetUserRoles WHERE UserId IN ('00000000-0000-0000-0000-000000000000', '11111111-1111-1111-1111-111111111111');
	

		
	INSERT INTO States (StateId, StateName)
	VALUES
	('CA', 'California'),
	('CO', 'Colorado'),
	('NE', 'Nebraska'),
	('IA', 'Iowa')
	

	SET IDENTITY_INSERT BodyStyles ON;

	INSERT INTO BodyStyles (BodyStyleId, BodyStyle)
	VALUES (1, 'SUV'),
	(2, 'Truck'),
	(3, 'Van'),
	(4, 'Car')

	SET IDENTITY_INSERT BodyStyles OFF;

	SET IDENTITY_INSERT Conditions ON;
	
	INSERT INTO Conditions (ConditionId, Condition)
	VALUES (1, 'New'),
	(2, 'Used')
	
	SET IDENTITY_INSERT Conditions OFF;

	SET IDENTITY_INSERT ExteriorColors ON;

	INSERT INTO ExteriorColors (ExteriorColorId, ExteriorColor)
	Values (1, 'Red'), (2, 'Black'), (3, 'Silver'), (4, 'Blue'), (5, 'White'), (6, 'Green')

	SET IDENTITY_INSERT ExteriorColors OFF;


	SET IDENTITY_INSERT InteriorColors ON; 
	
	INSERT INTO InteriorColors (InteriorColorId, InteriorColor)
	VALUES (1, 'Black'), (2, 'Grey'), (3, 'Tan')

	SET IDENTITY_INSERT InteriorColors OFF;

	SET IDENTITY_INSERT PurchaseTypes ON;

	INSERT INTO PurchaseTypes (PurchaseTypeId, PurchaseType)
	VALUES (1, 'Bank Finance'), (2, 'Cash'), (3, 'Dealer Finance')

	SET IDENTITY_INSERT PurchaseTypes OFF;


	SET IDENTITY_INSERT Specials ON;

	INSERT INTO Specials (SpecialId, Title, SpecialDescription)
	VALUES (1, 'Financing Special', 'No interest financing for 3 years on new inventory. Does not include 2016 vehicles. Does not include scratch and dent inventory. Must pre-qaulify with extraordinary credit. $2000 down payment required, trade in value not considered. Cosigners need not apply.'), 
	(2, 'Military Discount', 'Discount %5 off new inventory for all active military. Must present valid military ID. Not valid with other offers.'), 
	(3, 'Trade In Bonanaza!', 'Trade in = Blue book + %10 on all vehicles 2000 and newer!! Must be in working condition and does not apply to push, pull or drag in vehicles. Must be purchasing a vehicle from Guild Cars.'),
	(4, 'Test Drive Our New Inventory', 'Test drive a new car and get a $25 gift card to Applebees.')

	SET IDENTITY_INSERT Specials OFF;

	SET IDENTITY_INSERT Transmissions ON;

	INSERT INTO Transmissions (TransmissionId, Transmission)
	VALUES (1, 'Automatic'), (2, 'Manual')

	SET IDENTITY_INSERT Transmissions OFF;

	INSERT INTO AspNetUsers(Id, FirstName, LastName, EmailConfirmed, PhoneNumberConfirmed, Email, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName)
	VALUES('00000000-0000-0000-0000-000000000000', 'Jack', 'Black', 0, 0, 'test@test.com', 0, 0, 0, 'test'),
	('11111111-1111-1111-1111-111111111111', 'Melania', 'Trump', 0, 0, 'test2@test.com', 0, 0, 0, 'test2');

	INSERT INTO AspNetUserRoles(UserId, RoleId)
	VALUES( '00000000-0000-0000-0000-000000000000', 'a6112e8a-aeed-4fe3-b5dc-41b2826f74e2'),
	('11111111-1111-1111-1111-111111111111', '2319e050-a2b4-4fe0-a5de-1b79a1e7d666')

	SET IDENTITY_INSERT Makes ON

	INSERT INTO Makes (MakeId, Make, DateAdded, Id)
	VALUES (1, 'Toyota', '2017-01-20 18:48:14','00000000-0000-0000-0000-000000000000'),
	(2, 'Subaru', CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000'), 
	(3, 'Volvo', CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000'),
	(4, 'Honda', CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000'),
	(5, 'Ford', CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000')

	SET IDENTITY_INSERT Makes OFF

	SET IDENTITY_INSERT CarModels ON

	INSERT INTO CarModels (CarModelId, MakeId, CarModel, Id, DateAdded)
	VALUES (1, 1, '4Runner', '00000000-0000-0000-0000-000000000000', '2017-01-20 18:48:14'),
	(2, 2, 'Forester', '00000000-0000-0000-0000-000000000000', CURRENT_TIMESTAMP),  
	(3, 3, 'S60', '00000000-0000-0000-0000-000000000000', CURRENT_TIMESTAMP), 
	(4, 4, 'Civic', '00000000-0000-0000-0000-000000000000', '2017-01-20 18:48:14'),
	(5, 5, 'F-150', '00000000-0000-0000-0000-000000000000', '2017-01-20 18:48:14'), 
	(6, 2, 'Impreza', '00000000-0000-0000-0000-000000000000', '2017-01-19 18:48:14'),
	(7, 4, 'Pilot', '00000000-0000-0000-0000-000000000000', CURRENT_TIMESTAMP)

	SET IDENTITY_INSERT CarModels OFF

	SET IDENTITY_INSERT Vehicles ON

	INSERT INTO Vehicles (VehicleId, CarModelId, ExteriorColorId, InteriorColorId, TransmissionId, ConditionId, ModelYear, BodyStyleId, ListingPrice, MSRP, VIN, Mileage, CarDescription, Featured, AddDate, Id, ImageFileName, Sold)
	VALUES(1, 1, 3, 2, 1, 2, 2010, 1, 35000.00, 36250.00, 'JH4TB2H26CC000000', 45000, 'Like new condition, ready to go off road', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', '20104runner.jpg', 0), 
	      (2, 2, 2, 3, 2, 1, 2017, 1, 22400.00, 22595.00, 'PPPP22223333344DAP', 12, 'Fully loaded, BOSE sound system, all leather interior. Brand new ready to haul whatever your adventures require all weather. One of the safest vehicles on the road today. The Forester will not dissapoint.', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', 'subaruForester2017.jpg', 0 ), 
		  (3, 3, 1, 3, 2, 1, 2017, 4, 32550.00, 33950.00, 'ASDD1234PPPP5546AA', 53, 'Go Fast Now.', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', 'volvos60.jpg', 0),
		  (4, 4, 4, 1, 1, 1, 2016, 4, 18600.00, 18640.00, 'ASDD1234PPPP5WWWWE', 13974, '1 year old = brand new with a discount!', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', '2016hondacivic.jpg', 0),
		  (5, 5, 5, 3, 1, 2, 2014, 2, 24950.00, 23000.00, 'PP333234PPPP5546AA', 38500, 'Fully loaded, 4wD XLT!', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', '2014FordF150.jpg', 0),
		  (6, 6, 6, 2, 2, 2, 2007, 4, 5000.00, 5500.00, 'ASDD1234PPPP33992I', 65333, 'Great car for your comute!', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', '2007SubaruImpreza.jpg', 0), 
		  (7, 7, 3, 1, 1, 2, 2008, 1, 7000.00, 7500.00, 'ASDD1234PPPP8888II', 78500, 'Looks like new!', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', '2008HondaPilot.jpg', 0),
		  (8, 1, 3, 3, 1, 2, 2000, 1, 5000.00, 5500.00, 'ASDD1234PPPP55222444', 120000, 'Ready to climb!', 1, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', '20004runner.jpg', 0),
		  (9, 7, 3, 1, 1, 2, 2007, 1, 6500.00, 6600.00, 'ASDD1234PPPP8888II', 78500, 'Looks like new!', 0, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', 'sold.jpg', 1),
		  (10, 1, 3, 3, 1, 1, 2005, 1, 9000.00, 9500.00, 'ASDD1234PPPP55222444', 120000, 'Ready to climb!', 0, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', 'sold.jpg', 1),
		  (11, 2, 2, 3, 2, 1, 2017, 1, 22700.00, 23500.00, 'YYYMNOPQURSSEEICOES', 25, 'Fully loaded, black interior. AWD all the time', 0, CURRENT_TIMESTAMP, '00000000-0000-0000-0000-000000000000', 'subaruForester2017.jpg', 0 )
		  
	SET IDENTITY_INSERT Vehicles OFF

	SET IDENTITY_INSERT PurchaseInfo ON

	INSERT INTO PurchaseInfo(PurchaseInfoId, BuyerName, Phone, Street1, Street2, Email, StateId, ZipCode, SalePrice, PurchaseTypeId, VehicleId, PurchaseDate, Id)
	VALUES(1, 'John Smith', '555-444-3333', '123 Main St', 'null' , 'johnSmith@google.com', 'CO', 80127,  26000, 2, 9, '2017-01-21 20:30:36','11111111-1111-1111-1111-111111111111'),
		  (2, 'Sally Field', '431-222-1234', '444 S 11th St', 'Lincoln', 'sallyField@gmail.com', 'NE', 68502, 15000, 1, 10, '2017-01-2 20:00:00', '00000000-0000-0000-0000-000000000000')

	SET IDENTITY_INSERT PurchaseInfo OFF

END