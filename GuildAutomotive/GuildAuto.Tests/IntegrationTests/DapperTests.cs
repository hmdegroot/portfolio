﻿using GuildAuto.Data.ADO;
using GuildAuto.Data.Production;
using GuildAuto.Models.Queries;
using GuildAuto.Models.Tables;
using GuildAutomotive.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Tests.IntegrationTests
{
    [TestFixture]
    public class DapperTests
    {
        [SetUp]
        public void Init()
        {
            using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["GuildCars"].ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "GuildCarsDBReset";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Connection = cn;
                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        [Test]
        public void DapperCanLoadStates()
        {
            var repo = new StatesRepositoryDapper();

            var states = repo.GetAllStates();

            Assert.AreEqual(4, states.Count());
            Assert.AreEqual("CA", states[0].StateId);
            Assert.AreEqual("Colorado", states[1].StateName);
        }

        [Test]
        public void DapperCanLoadBodyStyles()
        {
            var repo = new BodyStylesRepositoryDapper();

            var bodyStyles = repo.GetAllBodyStyles();

            Assert.AreEqual(4, bodyStyles.Count());
            Assert.AreEqual(1, bodyStyles[0].BodyStyleId);
            Assert.AreEqual("Car", bodyStyles[3].BodyStyle);
        }

        [Test]
        public void DapperCanLoadConditions()
        {
            var repo = new ConditionsRepositoryDapper();

            var conditions = repo.GetAllConditions();

            Assert.AreEqual(2, conditions.Count());
            Assert.AreEqual(2, conditions[1].ConditionId);
            Assert.AreEqual("New", conditions[0].Condition);
        }

        [Test]
        public void DapperCanLoadExteriorColors()
        {
            var repo = new ExteriorColorsRepositoryDapper();

            var colors = repo.GetAllExteriorColors();

            Assert.AreEqual(6, colors.Count());
            Assert.AreEqual(5, colors[4].ExteriorColorId);
            Assert.AreEqual("Blue", colors[3].ExteriorColor);
        }

        [Test]
        public void DapperCanLoadInteriorColors()
        {
            var repo = new InteriorColorRepositoryDapper();

            var color = repo.GetAllInteriorColors();

            Assert.AreEqual(3, color.Count());
            Assert.AreEqual("Grey", color[1].InteriorColor);
            Assert.AreEqual(3, color[2].InteriorColorId);
        }

        [Test]
        public void DapperCanLoadPurchaseTypes()
        {
            var repo = new PurchaseTypesRepositoryDapper();
            var types = repo.GetAllPurchaseTypes();

            Assert.AreEqual(3, types.Count());
            Assert.AreEqual("Cash", types[1].PurchaseType);
            Assert.AreEqual(1, types[0].PurchaseTypeId);

        }

        [Test]
        public void DapperCanLoadSpecials()
        {
            var repo = new SpecialsRepositoryDapper();
            var specials = repo.GetAllSpecials();

            Assert.AreEqual(4, specials.Count());
            Assert.AreEqual(2, specials[1].SpecialId);
            Assert.AreEqual("No interest financing for 3 years on new inventory. Does not include 2016 vehicles. Does not include scratch and dent inventory. Must pre-qaulify with extraordinary credit. $2000 down payment required, trade in value not considered. Cosigners need not apply.", specials[0].SpecialDescription);

        }

        [Test]
        public void DapperCanLoadTransmissions()
        {
            var repo = new TransmissionsRepositoryDapper();
            var trans = repo.GetAllTransmissions();

            Assert.AreEqual(2, trans.Count());
            Assert.AreEqual(1, trans[0].TransmissionId);
            Assert.AreEqual("Manual", trans[1].Transmission);
        }

        [Test]
        public void DapperCanLoadAdminUsers()
        {
            var repo = new AdminUserRepositoryDapper();
            var users = repo.GetAllUserDetails();

            Assert.AreEqual(5, users.Count());
            Assert.AreEqual("00000000-0000-0000-0000-000000000000", users[0].Id);
            Assert.AreEqual("Jack", users[0].FirstName);
            Assert.AreEqual("Black", users[0].LastName);
        }

        [Test]
        public void DapperCanLoadMakes()
        {
            var repo = new MakesRepositoryDapper();
            var makes = repo.GetAllMakes();
            Assert.AreEqual(5, makes.Count());
            Assert.AreEqual(1, makes[0].MakeId);
            Assert.AreEqual("Toyota", makes[0].Make);
            Assert.AreEqual("00000000-0000-0000-0000-000000000000", makes[1].Id);
        }

        [Test]
        public void DapperCanLoadCarModels()
        {
            var repo = new CarModelsRepositoryDapper();
            var cars = repo.GetAllCarModels();

            Assert.AreEqual(7, cars.Count());
            Assert.AreEqual("4Runner", cars[0].CarModel);
            Assert.AreEqual(2, cars[1].MakeId);
            Assert.AreEqual("00000000-0000-0000-0000-000000000000", cars[1].Id);
        }

        [Test]
        public void DapperCanLoadVehicles()
        {
            var repo = new VehiclesRepositoryDapper();
            var cars = repo.GetAllVehicles().ToList();

            Assert.AreEqual(11, cars.Count());

            Assert.AreEqual(2, cars[1].VehicleId);
            Assert.AreEqual(2, cars[1].CarModelId);
            Assert.AreEqual(2, cars[1].ExteriorColorId);
            Assert.AreEqual(3, cars[1].InteriorColorId);
            Assert.AreEqual(2, cars[1].TransmissionId);
            Assert.AreEqual(1, cars[1].ConditionId);
            Assert.AreEqual(2017, cars[1].ModelYear);
            Assert.AreEqual(1, cars[1].BodyStyleId);
            Assert.AreEqual(22400, cars[1].ListingPrice);
            Assert.AreEqual(22595, cars[1].MSRP);
            Assert.AreEqual("PPPP22223333344DAP", cars[1].VIN);
            Assert.AreEqual(12, cars[1].Mileage);
            Assert.AreEqual("Fully loaded, BOSE sound system, all leather interior. Brand new ready to haul whatever your adventures require all weather. One of the safest vehicles on the road today. The Forester will not dissapoint.", cars[1].CarDescription);
            Assert.AreEqual(true, cars[1].Featured);
            Assert.AreEqual("00000000-0000-0000-0000-000000000000", cars[1].Id);
        }

        [Test]
        public void DapperCanLoadPurchaseInfo()
        {
            var repo = new PurchaseInfoRepositoryDapper();
            var purchase = repo.GetAllPurchaseInfo().ToList();

            Assert.AreEqual(2, purchase.Count());
            Assert.AreEqual(1, purchase[0].PurchaseInfoId);
            Assert.AreEqual("John Smith", purchase[0].BuyerName);
            Assert.AreEqual("555-444-3333", purchase[0].Phone);
            Assert.AreEqual("123 Main St", purchase[0].Street1);
            Assert.AreEqual("null", purchase[0].Street2);
            Assert.AreEqual("johnSmith@google.com", purchase[0].Email);
            Assert.AreEqual("CO", purchase[0].StateId);
            Assert.AreEqual(80127, purchase[0].ZipCode);
            Assert.AreEqual(26000, purchase[0].SalePrice);
            Assert.AreEqual(2, purchase[0].PurchaseTypeId);
            Assert.AreEqual(9, purchase[0].VehicleId);
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", purchase[0].Id);
        }

        [Test]
        public void DapperCanLoadFeatured()
        {
            var repo = new ListingRepositoryDapper();
            var featured = repo.GetFeaturedVehicles().ToList();

            Assert.AreEqual(8, featured.Count());
            Assert.AreEqual(1, featured[0].VehicleId);
            Assert.AreEqual("4Runner", featured[0].CarModel);
            Assert.AreEqual("Toyota", featured[0].Make);
            Assert.AreEqual(2010, featured[0].ModelYear);
            Assert.AreEqual(35000.00, featured[0].ListingPrice);
            Assert.AreEqual("20104runner.jpg", featured[0].ImageFileName);
        }

        [Test]
        public void DapperCanLoadNewVehicles()
        {
            var repo = new ListingRepositoryDapper();

            var listings = repo.GetNewVehicles().ToList();

            Assert.AreEqual(4, listings.Count());
            Assert.AreEqual("Subaru", listings[0].Make);
            Assert.AreEqual("Forester", listings[0].CarModel);
            Assert.AreEqual("Tan", listings[0].InteriorColor);
            Assert.AreEqual("Black", listings[0].ExteriorColor);
            Assert.AreEqual(2017, listings[0].ModelYear);
            Assert.AreEqual("SUV", listings[0].BodyStyle);
            Assert.AreEqual("subaruForester2017.jpg", listings[0].ImageFileName);
            Assert.AreEqual(22400.00, listings[0].ListingPrice);
            Assert.AreEqual("Manual", listings[0].Transmission);
            Assert.AreEqual("PPPP22223333344DAP", listings[0].VIN);
            Assert.AreEqual(22595.00, listings[0].MSRP);
        }

        [Test]
        public void DapperCanLoadUsedVehicles()
        {
            var repo = new ListingRepositoryDapper();

            var listings = repo.GetUsedVehicles().ToList();

            Assert.AreEqual(5, listings.Count());
            Assert.AreEqual("Toyota", listings[0].Make);
            Assert.AreEqual("4Runner", listings[0].CarModel);
            Assert.AreEqual("Grey", listings[0].InteriorColor);
            Assert.AreEqual("Silver", listings[0].ExteriorColor);
            Assert.AreEqual(2010, listings[0].ModelYear);
            Assert.AreEqual("SUV", listings[0].BodyStyle);
            Assert.AreEqual("20104runner.jpg", listings[0].ImageFileName);
            Assert.AreEqual(35000, listings[0].ListingPrice);
            Assert.AreEqual("Automatic", listings[0].Transmission);
            Assert.AreEqual("JH4TB2H26CC000000", listings[0].VIN);
            Assert.AreEqual(36250, listings[0].MSRP);
        }

        [Test]
        public void DapperCanLoadSingleVehicle()
        {
            var repo = new ListingRepositoryDapper();
            var listing = repo.GetVehicleById(1);
            Assert.AreEqual(0, listing.VehicleId);
            Assert.AreEqual("Toyota", listing.Make);
            Assert.AreEqual(1, listing.MakeId);
            Assert.AreEqual("4Runner", listing.CarModel);
            Assert.AreEqual(1, listing.CarModelId);
            Assert.AreEqual("Grey", listing.InteriorColor);
            Assert.AreEqual(2, listing.InteriorColorId);
            Assert.AreEqual("Silver", listing.ExteriorColor);
            Assert.AreEqual(3, listing.ExteriorColorId);
            Assert.AreEqual(2010, listing.ModelYear);
            Assert.AreEqual("SUV", listing.BodyStyle);
            Assert.AreEqual(1, listing.BodyStyleId);
            Assert.AreEqual("20104runner.jpg", listing.ImageFileName);
            Assert.AreEqual(35000, listing.ListingPrice);
            Assert.AreEqual("Automatic", listing.Transmission);
            Assert.AreEqual(1, listing.TransmissionId);
            Assert.AreEqual("JH4TB2H26CC000000", listing.VIN);
            Assert.AreEqual(36250, listing.MSRP);
            Assert.AreEqual("Like new condition, ready to go off road", listing.CarDescription);
            Assert.AreEqual(false, listing.Sold);

            listing = repo.GetVehicleById(10);
            Assert.AreEqual(true, listing.Sold);




        }

        //[Test]
        //public void DapperCanLoadRoles()
        //{
        //    var roles = new RolesRepositoryDapper().GetAllRoles();
        //    Assert.AreEqual(2, roles.Count());
        //    Assert.AreEqual("Admin", roles[0].UserRole);
        //    Assert.AreEqual(1, roles[0].RoleId);
        //}

        [Test]
        public void DapperCanLoadAllUnsoldVehicles()
        {
            var repo = new ListingRepositoryDapper();
            var listings = repo.GetAllVehicles().ToList();

            Assert.AreEqual(9, listings.Count());
            Assert.AreEqual("Toyota", listings[0].Make);
            Assert.AreEqual("4Runner", listings[0].CarModel);
            Assert.AreEqual("Grey", listings[0].InteriorColor);
            Assert.AreEqual("Silver", listings[0].ExteriorColor);
            Assert.AreEqual(2010, listings[0].ModelYear);
            Assert.AreEqual("SUV", listings[0].BodyStyle);
            Assert.AreEqual("20104runner.jpg", listings[0].ImageFileName);
            Assert.AreEqual(35000, listings[0].ListingPrice);
            Assert.AreEqual("Automatic", listings[0].Transmission);
            Assert.AreEqual("JH4TB2H26CC000000", listings[0].VIN);
            Assert.AreEqual(36250, listings[0].MSRP);
        }

        [Test]
        public void DapperCanInsertVehicle()
        {
            var repo = new ListingRepositoryDapper();

            var listings = repo.GetAllVehicles();

            Vehicle car = new Vehicle();

            car.AddDate = DateTime.Now;
            car.BodyStyleId = 1;
            car.CarDescription = "Brand new SUV";
            car.CarModelId = 1;
            car.ConditionId = 1;
            car.ExteriorColorId = 4;
            car.InteriorColorId = 2;
            car.Featured = true;
            car.ImageFileName = "image20.jpg";
            car.ListingPrice = 20200;
            car.Mileage = 23;
            car.ModelYear = 2017;
            car.MSRP = 21200;
            car.Sold = false;
            car.TransmissionId = 1;
            car.VIN = "TTTYYYQQWE3355WER";
            car.Id = "00000000-0000-0000-0000-000000000000";

            repo.Insert(car);

            listings = repo.GetAllVehicles();

            Assert.AreEqual(10, listings.Count());
        }

        [Test]
        public void DapperCanUpdateVehicle()
        {
            var repo = new ListingRepositoryDapper();
            Vehicle car = new Vehicle();

            car.BodyStyleId = 2;
            car.CarDescription = "Mostly wrecked yet new SUV";
            car.CarModelId = 2;
            car.ConditionId = 2;
            car.ExteriorColorId = 3;
            car.InteriorColorId = 1;
            car.Featured = false;
            car.ImageFileName = "image22.jpg";
            car.ListingPrice = 5200;
            car.Mileage = 35;
            car.ModelYear = 2016;
            car.MSRP = 200;
            car.TransmissionId = 2;
            car.VIN = "TTTYYYQQWE33ppppp";
            car.VehicleId = 1;

            repo.Update(car);

            var updatedV = repo.GetVehicleById(1);

            Assert.AreEqual("Truck", updatedV.BodyStyle);
            Assert.AreEqual(2, updatedV.BodyStyleId);
            Assert.AreEqual("Mostly wrecked yet new SUV", updatedV.CarDescription);
            Assert.AreEqual(5200, updatedV.ListingPrice);
            Assert.AreEqual(2, updatedV.CarModelId);
            Assert.AreEqual(2, updatedV.ConditionId);
            Assert.AreEqual("Used", updatedV.Condition);
            Assert.AreEqual(3, updatedV.ExteriorColorId);
            Assert.AreEqual(1, updatedV.InteriorColorId);
            Assert.AreEqual("image22.jpg", updatedV.ImageFileName);
            Assert.AreEqual(5200, updatedV.ListingPrice);
            Assert.AreEqual(200, updatedV.MSRP);
            Assert.AreEqual(2, updatedV.TransmissionId);
            Assert.AreEqual("TTTYYYQQWE33ppppp", updatedV.VIN);
        }

        [Test]
        public void DapperCanSearchMaxPrice()
        {
            var parameters = new ListingSearchParameters();
            parameters.MaxPrice = 20000;

            var repo = new ListingRepositoryDapper().UsedSearch(parameters);
            Assert.AreEqual(3, repo.Count());
        }

        [Test]
        public void DapperCanSearchMinPrice()
        {
            var parameters = new ListingSearchParameters();
            parameters.MinPrice = 7000;

            var repo = new ListingRepositoryDapper().UsedSearch(parameters);
            Assert.AreEqual(3, repo.Count());
        }

        [Test]
        public void DapperCanSearchMaxYear()
        {
            var parameters = new ListingSearchParameters();
            parameters.MaxYear = 2008;

            var repo = new ListingRepositoryDapper().UsedSearch(parameters);
            Assert.AreEqual(3, repo.Count());
        }

        [Test]
        public void DapperCanSearchMinYear()
        {
            var parameters = new ListingSearchParameters();
            parameters.MinYear = 2008;

            var repo = new ListingRepositoryDapper().UsedSearch(parameters);
            Assert.AreEqual(3, repo.Count());
        }

        [Test]
        public void DapperCanSearchMake()
        {
            var parameters = new ListingSearchParameters();
            parameters.SearchBox = "Toy";

            var repo = new ListingRepositoryDapper().UsedSearch(parameters);
            Assert.AreEqual(2, repo.Count());
        }

        [Test]
        public void DapperCanSearchModel()
        {
            var parameters = new ListingSearchParameters();
            parameters.SearchBox = "4runner";

            var repo = new ListingRepositoryDapper().UsedSearch(parameters);
            Assert.AreEqual(2, repo.Count());
        }

        [Test]
        public void DapperCanAddPurchase()
        {
            var purchaseRepo = new PurchaseInfoRepositoryDapper().GetAllPurchaseInfo();

            Assert.AreEqual(2, purchaseRepo.Count());

            var purchase = new PurchaseInfo();

            purchase.BuyerName = "Ted";
            purchase.StateId = "CO";
            purchase.Street1 = "2817 S Kearney St";
            purchase.City = "Denver";
            purchase.Email = "ted@billnted.com";
            purchase.SalePrice = 20000m;
            purchase.PurchaseDate = DateTime.Now;
            purchase.Phone = "720-937-5005";
            purchase.VehicleId = 1;
            purchase.ZipCode = 80207;
            purchase.PurchaseTypeId = 1;
            purchase.Id = "00000000-0000-0000-0000-000000000000";

            var addRepo = new PurchaseInfoRepositoryDapper();
            addRepo.InsertPurchase(purchase);


            purchaseRepo = new PurchaseInfoRepositoryDapper().GetAllPurchaseInfo();

            Assert.AreEqual(3, purchaseRepo.Count());

        }

        [Test]
        public void DapperCanLoadModelDetails()
        {
            var repo = new CarModelsRepositoryDapper();

            var models = repo.GetModelDetails();

            Assert.AreEqual(7, models.Count());
        }

        [Test]
        public void DapperCanLoadMakeDetails()
        {
            var repo = new MakesRepositoryDapper();

            var makes = repo.GetMakeDetails();

            Assert.AreEqual(5, makes.Count());
            Assert.AreEqual("Honda", makes[3].Make);
            Assert.AreEqual("00000000-0000-0000-0000-000000000000", makes[4].Id);

        }

        [Test]
        public void DapperCanInsertMake()
        {
            var repo = new MakesRepositoryDapper();

            var makes = repo.GetAllMakes();

            Assert.AreEqual(5, makes.Count());

            Makes carMake = new Makes();

            carMake.Make = "Kia";
            carMake.DateAdded = DateTime.Now;
            carMake.Id = "00000000-0000-0000-0000-000000000000";

            repo.Insert(carMake);

            makes = repo.GetAllMakes();

            Assert.AreEqual(6, makes.Count());
        }

        [Test]
        public void DapperCanInsertModel()
        {
            var repo = new CarModelsRepositoryDapper();

            var models = repo.GetAllCarModels();

            Assert.AreEqual(7, models.Count());

            CarModels model = new CarModels();

            model.Id = "00000000-0000-0000-0000-000000000000";
            model.CarModel = "Outback";
            model.MakeId = 2;
            model.DateAdded = DateTime.Now;

            repo.Insert(model);

            models = repo.GetAllCarModels();

            Assert.AreEqual(8, models.Count());
        }

        [Test]
        public void DapperCanInsertSpecial()
        {
            var repo = new SpecialsRepositoryDapper();

            var specials = repo.GetAllSpecials();

            Assert.AreEqual(4, specials.Count());

            Specials newSpecial = new Specials();

            newSpecial.SpecialDescription = "This is a new special";
            newSpecial.Title = "new special title";

            repo.Insert(newSpecial);

            specials = repo.GetAllSpecials();

            Assert.AreEqual(5, specials.Count());
        }

        [Test]
        public void DapperCanDeleteSpecial()
        {
            var repo = new SpecialsRepositoryDapper();

            var specials = repo.GetAllSpecials();

            Assert.AreEqual(4, specials.Count());

            repo.Delete(1);

            specials = repo.GetAllSpecials();

            Assert.AreEqual(3, specials.Count());
        }

        [Test]
        public void DapperCanSearchSalesReportByUser()
        {
            var repo = new ReportsRepositoryDapper();

            var parameters = new SalesSearchParameters();

            parameters.Id = "00000000-0000-0000-0000-000000000000";

            var results = repo.SalesSearch(parameters);

            Assert.AreEqual(1, results.Count());
        }

        [Test]
        public void DapperCanSearchSalesReportByDate()
        {
            var repo = new ReportsRepositoryDapper();

            var parameters = new SalesSearchParameters();

            parameters.FromDate = new DateTime(2017, 01, 03, 01, 12, 00);

            var results = repo.SalesSearch(parameters);

            Assert.AreEqual(1, results.Count());
        }

        [Test]
        public void DapperCanSearchSalesReportEmpty()
        {
            var repo = new ReportsRepositoryDapper();

            var parameters = new SalesSearchParameters();

            var results = repo.SalesSearch(parameters);

            Assert.AreEqual(2, results.Count());
        }

        [Test]
        public void DapperCanLoadSalesReport()
        {
            var repo = new ReportsRepositoryDapper();
            var sales = repo.GetAllSales();

            Assert.AreEqual(2, sales.Count());
        }

        [Test]
        public void DapperCanLoadNewInvetoryReport()
        {
            var repo = new ReportsRepositoryDapper().GetNewInventory();

            Assert.AreEqual(3, repo.Count());
        }

        [Test]
        public void DapperCanLoadUsedInvetoryReport()
        {
            var repo = new ReportsRepositoryDapper().GetUsedInvetory();

            Assert.AreEqual(5, repo.Count());
        }

        [Test]
        public void DapperCanLoadRoles()
        {
            var roles = new RolesRepositoryDapper().GetAllRoles();

            Assert.AreEqual(2, roles.Count());
            Assert.AreEqual("admin", roles[0].Name);
        }
    }
}
