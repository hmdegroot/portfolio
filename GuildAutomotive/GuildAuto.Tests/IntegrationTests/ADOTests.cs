﻿using GuildAuto.Data.ADO;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Tests.IntegrationTests
{
    [TestFixture]
    public class ADOTests
    {
        [Test]
        public void ADOCanLoadStates()
        {
            var repo = new StatesRepositoryADO();

            var states = repo.GetAllStates();

            Assert.AreEqual(4, states.Count());
            Assert.AreEqual("CA", states[0].StateId);
            Assert.AreEqual("Colorado", states[1].StateName);
        }
    }
}
