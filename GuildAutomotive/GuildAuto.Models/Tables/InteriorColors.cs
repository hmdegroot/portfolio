﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class InteriorColors
    {
        public int InteriorColorId { get; set; }
        public string InteriorColor { get; set; }
    }
}
