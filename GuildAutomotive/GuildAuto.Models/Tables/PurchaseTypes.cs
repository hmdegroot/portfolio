﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class PurchaseTypes
    {
        public int PurchaseTypeId { get; set; }
        public string PurchaseType { get; set; }
    }
}
