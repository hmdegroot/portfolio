﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class PurchaseInfo
    {
        public int PurchaseInfoId { get; set; }
        public string BuyerName { get; set; }
        public string Phone { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Email { get; set; }
        public string StateId { get; set; }
        public decimal SalePrice { get; set; }
        public int ZipCode { get; set; }
        public int PurchaseTypeId { get; set; }
        public string ImageFileName { get; set; }
        public int VehicleId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string Id { get; set; }
        public string City { get; set; }
    }
}
