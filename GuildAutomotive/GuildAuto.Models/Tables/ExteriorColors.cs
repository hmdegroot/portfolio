﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class ExteriorColors
    {
        public int ExteriorColorId { get; set; }
        public string ExteriorColor { get; set; }
    }
}
