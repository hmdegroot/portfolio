﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class Contacts : IValidatableObject
    {
        public int ContactId { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ContactMessage { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(ContactName))
            {
                errors.Add(new ValidationResult("Please enter your name."));
            }
            if (string.IsNullOrEmpty(Email) && (string.IsNullOrEmpty(Phone)))
            {
                errors.Add(new ValidationResult("Please enter an email address or phone number."));
            }
            if (string.IsNullOrEmpty(ContactMessage))
            {
                errors.Add(new ValidationResult("Please tell us a bit about why you are contacting us."));
            }

            return errors;
        }
    }
}
