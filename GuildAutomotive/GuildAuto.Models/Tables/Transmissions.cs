﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class Transmissions
    {
        public int TransmissionId { get; set; }
        public string Transmission { get; set; }
    }
}
