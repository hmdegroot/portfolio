﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class CarModels
    {
        public int CarModelId { get; set; }
        public int MakeId { get; set; }
        public string CarModel { get; set; }
        public DateTime DateAdded { get; set; }
        public string Id { get; set; }
    }
}
