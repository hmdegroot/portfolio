﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class Conditions
    {
        public int ConditionId { get; set; }
        public string Condition { get; set; }
    }
}
