﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Tables
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        public int CarModelId { get; set; }
        public int ExteriorColorId { get; set; }
        public int InteriorColorId { get; set; }
        public int TransmissionId { get; set; }
        public int ConditionId { get; set; }
        public int ModelYear { get; set; }
        public int BodyStyleId { get; set; }
        public decimal ListingPrice { get; set; }
        public decimal MSRP { get; set; }
        public string VIN { get; set; }
        public int Mileage { get; set; }
        public string CarDescription { get; set; }
        public bool Featured { get; set; }
        public DateTime AddDate { get; set; }
        public string Id { get; set; }
        public string ImageFileName { get; set; }
        public bool Sold { get; set; }
    }
}
