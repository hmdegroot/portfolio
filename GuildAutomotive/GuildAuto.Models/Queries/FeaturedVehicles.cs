﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Queries
{
    public class FeaturedVehicles
    {
        public int VehicleId { get; set; }
        public string CarModel { get; set; }
        public int CarModelId { get; set; }
        public string Make { get; set; }
        public int MakeId { get; set; }
        public int ModelYear { get; set; }
        public decimal ListingPrice { get; set; }
        public string ImageFileName { get; set; }
    }
}
