﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Queries
{
    public class ListingItem
    {
        public int VehicleId { get; set; }
        public int CarModelId { get; set; }
        public string CarModel { get; set; }
        public string Make { get; set; }
        public string Condition { get; set; }
        public int ConditionId { get; set; }
        public int MakeId { get; set; }
        public int ModelYear { get; set; }
        public int Mileage { get; set; }
        public decimal ListingPrice { get; set; }
        public decimal MSRP { get; set; }
        public string ImageFileName { get; set; }
        public string BodyStyle { get; set; }
        public int BodyStyleId { get; set; }
        public string InteriorColor { get; set; }
        public int InteriorColorId { get; set; }
        public int ExteriorColorId { get; set; }
        public string ExteriorColor { get; set; }
        public string Transmission { get; set; }
        public int TransmissionId { get; set; }
        public string VIN { get; set; }
        public string CarDescription { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Sold { get; set; }
        public string Id { get; set; }
        public bool Featured { get; set; }
    }
}
