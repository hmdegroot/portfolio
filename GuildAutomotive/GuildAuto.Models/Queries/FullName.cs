﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Queries
{
    public class FullName
    {
        public string Name { get; set; }
    }
}
