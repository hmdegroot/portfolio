﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Queries
{
    public class SoldVehicle
    {
        public int VehicleId { get; set; }
        public string ImageFileName { get; set; }
        public bool Sold { get; set; }
        public bool Featured { get; set; }
    }
}
