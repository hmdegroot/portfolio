﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildAuto.Models.Queries
{
    public class InventoryReport
    {
        public int ModelYear { get; set; }
        public string Make { get; set; }
        public string CarModel { get; set; }
        public int Stock { get; set; }
        public decimal StockValue { get; set; }
    }
}
