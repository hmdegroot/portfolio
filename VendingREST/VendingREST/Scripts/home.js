﻿
$(document).ready(function () {
    var id;
    var moneyDepositSum = 0;
    var change;
    
    loadPage();

    $('.box').hover(
        function () {
            $(this).css('background-color', '#e6e6e6');
        },
        function () {
            $(this).css('background-color', '');
        });

    $('.box').click(
       function () {
           $('#item-selected').empty();
           $(id).empty();


           $(this).css('background-color', '#cccccc');
           var idString = (this.id);
           id = parseInt(idString);
           id += 1;
           var item = '<h4 class=text-center>' + id + '</h4>';
           $('#item-selected').append(item);
           $('#message-box').empty();
           
       });

    $('#purchase').click(
        function () {

            $.ajax({
                type: 'GET',
                url: 'http://localhost:8080/money/' + moneyDepositSum + '/item/' + id,

                success: function (value, key) {
                    $('#message-box').empty();
                    var message = 'Thank you!';
                    $('#message-box').replaceWith('<h4 id="message-box" class=text-center>' + message + '</h4>');

                    var quarters = value.quarters;
                    var dimes = value.dimes;
                    var nickels = value.nickels;
                    var pennies = value.pennies;

                    var change = (quarters * .25) + (dimes * .10) + (nickels * .05) + (pennies * .01);
                    $('#change').replaceWith('<h5 id="change" class=text-center>' + quarters + ' quarters ' + dimes + ' dimes ' + nickels + ' nickels ' + pennies + ' pennies' + '</h5>');

                    moneyDepositSum = 0;
                    $('#money-in-sum').empty();

                },
                error: function (xhr, error, errorThrown) {
                    console.log(xhr.status);
                    console.log(error);
                    console.log(errorThrown);
                    var err = JSON.parse(xhr.responseText);
                    var message = err.message;
                    $('#message-box').replaceWith('<h4 class=text-center id="message-box">' + message + '</h4>');
                }
            })
        });

    $('#add-dollar').click(
        function () {
            $('#money-in-sum').empty();
            moneyDepositSum += 1.00;
            $('#money-in-sum').append('<h4 class=text-center>$' + moneyDepositSum + '</h4>');
        });

    $('#add-quarter').click(
      function () {
          $('#money-in-sum').empty();
          moneyDepositSum += 0.25;
          $('#money-in-sum').append('<h4 class=text-center>$' + moneyDepositSum + '</h4>');
      });

    $('#add-dime').click(
      function () {
          $('#money-in-sum').empty();
          moneyDepositSum += 0.10;
          $('#money-in-sum').append('<h4 class=text-center>$' + moneyDepositSum + '</h4>');
      });

    $('#add-nickel').click(
      function () {
          $('#money-in-sum').empty();
          moneyDepositSum += 0.05;
          $('#money-in-sum').append('<h4 class=text-center>$' + moneyDepositSum + '</h4>');
      });

    $('#change-box').click(
        function () {
            id = 0;
            moneyDepositSum = 0;
            $('#change').replaceWith('<h4 class=text-center id="change">Cancel</h4>');
            $("#money-in-sum").empty();
            $('#item-selected').empty();
            $('#message-box').replaceWith('<h4 class=text-center id="message-box">Make a selection</h4>');
            clearAll();
            loadPage();
            
        })
});

function loadPage() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/items',
        success: function (value, key) {
            for (i = 0; i < 9; i++) {
                var id = '<strong>' + value[i].id + '</strong>';
                $('#id-' + i).append(id);

                var itemName = '<h4 class="text-center">' + value[i].name + '</h4>';
                $('#item-' + i).append(itemName);

                var itemCost = '<h4 class="text-center">$' + value[i].price + '</h4>';
                $('#cost-' + i).append(itemCost);

                var itemsRemaining = '<h4 class="col-sm-12 text-center">Quantity Left: ' + value[i].quantity + '</h4>';
                $('#remain-' + i).append(itemsRemaining);
            }

        },

        
        error: function () {
            alert('server problems. try again.');
        }
    });
}

function clearAll() {
    for (i = 0; i < 9; i++) {
        $('#id-' + i).empty();
        $('#item-' + i).empty();
        $('#cost-' + i).empty();      
        $('#remain-' + i).empty();
    }
}



