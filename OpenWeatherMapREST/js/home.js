
"use strict";
$(document).ready(function () {
	//// $('#fieldId').val().length 
	$("#currentWeather").hide();
	$('#fiveDayForecast').hide();

	$('#weather-button').click(function(event){

		$('#errorMessages').empty();
		var errorMessage;
		var zip = $('#zip-code').val();
		var reg = /^[0-9]+$/;
		if (zip == ''){
			errorMessage = "*Zipcode required!";
		}
		else if ((zip.length)< 5 || (zip.length)>5 ){
			errorMessage = "*zipcode should 5 digits";
		}
		else if (!reg.test(zip)){
			errorMessage = "*zipcode should be numbers only";
		}
		

		if(errorMessage){

			$('#errorMessages').append($('<li>').attr({class: 'list-group-item list-group-item-danger'}).text(errorMessage));
			return false;			
		}

		var units = $('#units').val();

		$.ajax({
			type: 'GET',
			url: 'http://api.openweathermap.org/data/2.5/weather?zip=' + zip + ',us&units=' + units + '&appid=e0e538d4848b02af4c52261129d72e52',
			success: function (value, key ){
				clearInfo();
				$('#currentWeather').show();

				var city = '<h3>Current Conditions in ' +value.name+ '</h3>';
				$('#city-name').append(city);

				var image = '<img src="http://openweathermap.org/img/w/' +value.weather[0].icon+ '.png" height="80" width="80" >';		
				$('#current-image').append(image);

				$('#current-description').append('<br><h5>' +value.weather[0].main+ '</h5');

				if(units=='imperial'){
					$('#current-temperature').append('<h5>Temperature: ' +value.main.temp+ ' F </h5>');
				}
				else{
					$('#current-temperature').append('<h5>Temperature: ' +value.main.temp+ ' C </h5>');
				}

				$('#current-humidity').append('<h5>Humidity: ' +value.main.humidity+ '%</h5>');

				if(units=='imperial'){
					$('#current-wind').append('<h5>Wind: ' +value.wind.speed+ ' miles/hour</h5>');
				}
				else{
					$('#current-wind').append('<h5>Wind: ' +value.wind.speed+ ' kilometer/hour</h5>');
				}		

				console.log("success callback finished");

				$.ajax({
					type: 'GET',
					url: 'http://api.openweathermap.org/data/2.5/forecast/daily?zip='+ zip + ',us&units=' +units + '&cnt={5}&appid=e0e538d4848b02af4c52261129d72e52',
					success: function (value, key){

						$('#fiveDayForecast').show();
						console.log('success');

						var image = '<img src="http://openweathermap.org/img/w/' +value.list[0].weather[0].icon+ '.png" >';		
						$('#image-0').append(image);

						if(units=='imperial'){
							$('#high-0').append('<h5>H : ' +value.list[0].temp.max+ ' F </h5>');
							$('#low-0').append('<h5>L ' +value.list[0].temp.min+ ' F </h5>');
						}
						else{
							$('#high-0').append('<h5>H : ' +value.list[0].temp.max+ ' C </h5>');
							$('#low-0').append('<h5>L ' +value.list[0].temp.min+ ' C </h5>');
						}

						$('#description-0').append('<br><h5>' +value.list[0].weather[0].main+ '</h5');

						var unixDate = value.list[0].dt;

						var day=defineDay(unixDate);
						var month= defineMonth(unixDate);


						$('#date-0').append('<br><h5>'  +day + ' ' + month +'</h5');

						////day 2

						var image = '<img src="http://openweathermap.org/img/w/' +value.list[1].weather[0].icon+ '.png" >';		
						$('#image-1').append(image);

						if(units=='imperial'){
							$('#high-1').append('<h5>H : ' +value.list[1].temp.max+ ' F </h5>');
							$('#low-1').append('<h5>L ' +value.list[1].temp.min+ ' F </h5>');
						}
						else{
							$('#high-1').append('<h5>H : ' +value.list[1].temp.max+ ' C </h5>');
							$('#low-1').append('<h5>L ' +value.list[1].temp.min+ ' C </h5>');
						}

						$('#description-1').append('<br><h5>' +value.list[1].weather[0].main+ '</h5');

						
						var unixDate1 = value.list[1].dt;
						
						var day1=defineDay(unixDate);
						var month1= defineMonth(unixDate);
						
						
						$('#date-1').append('<br><h5>'  +day1 + ' ' + month1 +'</h5');

						//// day 3

						var image = '<img src="http://openweathermap.org/img/w/' +value.list[2].weather[0].icon+ '.png" >';		
						$('#image-2').append(image);

						if(units=='imperial'){
							$('#high-2').append('<h5>H : ' +value.list[2].temp.max+ ' F </h5>');
							$('#low-2').append('<h5>L ' +value.list[2].temp.min+ ' F </h5>');
						}
						else{
							$('#high-2').append('<h5>H : ' +value.list[2].temp.max+ ' C </h5>');
							$('#low-2').append('<h5>L ' +value.list[2].temp.min+ ' C </h5>');
						}

						$('#description-2').append('<br><h5>' +value.list[2].weather[0].main+ '</h5');

						var unixDate = value.list[2].dt;
						
						var day=defineDay(unixDate);
						var month= defineMonth(unixDate);
						
						
						$('#date-2').append('<br><h5>'  +day + ' ' + month +'</h5');

						//// day 4

						var image = '<img src="http://openweathermap.org/img/w/' +value.list[3].weather[0].icon+ '.png" >';		
						$('#image-3').append(image);

						if(units=='imperial'){
							$('#high-3').append('<h5>H : ' +value.list[3].temp.max+ ' F </h5>');
							$('#low-3').append('<h5>L ' +value.list[3].temp.min+ ' F </h5>');
						}
						else{
							$('#high-3').append('<h5>H : ' +value.list[3].temp.max+ ' C </h5>');
							$('#low-3').append('<h5>L ' +value.list[3].temp.min+ ' C </h5>');
						}

						$('#description-3').append('<br><h5>' +value.list[3].weather[0].main+ '</h5');

						var unixDate = value.list[3].dt;
						
						var day=defineDay(unixDate);
						var month= defineMonth(unixDate);
						
						$('#date-3').append('<br><h5>'  +day + ' ' + month +'</h5');
						//// day 5

						var image = '<img src="http://openweathermap.org/img/w/' +value.list[4].weather[0].icon+ '.png" >';		
						$('#image-4').append(image);

						if(units=='imperial'){
							$('#high-4').append('<h5>H : ' +value.list[4].temp.max+ ' F </h5>');
							$('#low-4').append('<h5>L ' +value.list[4].temp.min+ ' F </h5>');
						}
						else{
							$('#high-4').append('<h5>H : ' +value.list[4].temp.max+ ' C </h5>');
							$('#low-4').append('<h5>L ' +value.list[4].temp.min+ ' C </h5>');
						}

						$('#description-4').append('<br><h5>' +value.list[4].weather[0].main+ '</h5');

						var unixDate = value.list[4].dt;
						
						var day=defineDay(unixDate);
						var month= defineMonth(unixDate);
						
						$('#date-4').append('<br><h5>'  +day + ' ' + month +'</h5');

					},
					error: function() {
						alert('failure');
					}

				})

},
error: function() {
	alert('failure');
}
})

console.log("finished with click method");

})
});


function clearInfo(){
	$('#zip-code').empty();
	$('#current-image').empty();
	$('#current-description').empty();
	$('#city-name').empty();
	$('#current-temperature').empty();
	$('#current-humidity').empty();
	$('#current-wind').empty();
	$('#high-0').empty();
	$('#low-0').empty();
	$('#image-0').empty();
	$('#description-0').empty();
	$('#date-0').empty();
	$('#high-1').empty();
	$('#low-1').empty();
	$('#image-1').empty();
	$('#description-1').empty();
	$('#date-1').empty();
	$('#high-2').empty();
	$('#low-2').empty();
	$('#image-2').empty();
	$('#description-2').empty();
	$('#date-2').empty();
	$('#high-3').empty();
	$('#low-3').empty();
	$('#image-3').empty();
	$('#description-3').empty();
	$('#date-3').empty();
	$('#high-4').empty();
	$('#low-4').empty();
	$('#image-4').empty();
	$('#description-4').empty();
	$('#date-4').empty();
}

function defineDay(unixDate){
	var date =new Date(unixDate*1000);
	var day = date.getDate();
	return day;
}

function defineMonth(unixDate){
	var date =new Date(unixDate*1000);
	var m = date.getMonth();
	

	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var n = month[date.getMonth()];

	return n;
}
