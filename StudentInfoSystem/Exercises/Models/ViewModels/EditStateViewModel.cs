﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exercises.Models.ViewModels
{
    public class EditStateViewModel
    {
        [Required(ErrorMessage = "Please enter a state abbreviation.")]
        public string StateAbbreviation { get; set; }
        [Required(ErrorMessage = "Please enter a state name.")]
        public string StateName { get; set; }
    }
}