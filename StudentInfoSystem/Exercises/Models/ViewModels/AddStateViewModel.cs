﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Exercises.Models.ViewModels
{
    public class AddStateViewModel
    {
        [Required(ErrorMessage = "Please enter a state abbreviation.")]
        public string StateAbbreviation { get; set; }
        [Required(ErrorMessage = "Please enter a state name.")]
        public string StateName { get; set; }
    }
}